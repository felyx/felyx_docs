================
Colocation query
================

Query format
============

The general format for a colocation query is as follow:

  .. sourcecode:: python

     {
       "colocation":
         {
          "maximum_time_difference": <a delta time, in minutes>,
          "input":
             {
                "selection": {<some selection criteria>}
             }
         }
     }

.. important::

   As for the `selection` block, it is only the first element of a workflow,
   that can be complex and involve multiple embedded steps. The most basic
   workflow uses the `colocation` block to retrieve colocated miniprods or
   metrics values, in which case the `colocation` block is embedded within
   an `extraction` block as follow:

  .. sourcecode:: python

     {
       "extraction":
         {
          "colocation":
            {
             "maximum_time_difference": <a delta time, in minutes>,
             "input":
               {
                 "selection": {<some selection criteria>}
			   }
            }
         }
     }


Examples
========

  .. sourcecode:: python

     {"extraction": {
        "colocation": {
            "maximum_time_difference": 720,
            "input": {
                "selection": [{
                    "stop_time": "2014-12-31T00:00:00",
                    "start_time": "2012-01-01T00:00:0",
                    "site_list": ["ghr060", "ghr014"],
                    "data_type": "metrics",
                    "dataset_list": ["ifr-l4-sstfnd-odyssea-med_002_v2.1"],
                    "metric_list": ["mean_sst", "day_or_night"]
                }, {
                    "stop_time": "2014-12-31T00:00:00",
                    "start_time": "2012-01-01T00:00:00",
                    "site_list": [ "ghr060", "ghr014"],
                    "data_type": "metrics",
                    "dataset_list": ["viirs_npp-navo-l2p-v1.0"],
                    "metric_list": ["mean_sst", "day_or_night"]
                }]
            }
        }
     }

