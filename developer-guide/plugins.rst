.. _felyxcontribs_metrics_base: https://gitlab.ifremer.fr/felyx/felyxcontribs_metrics_base

==================
Adding new plugins
==================

What are plugins?
=================

Any processing task in **felyx** backend can be described as a sequence of
steps to perform on an input file to obtain a result. These processing tasks
include the computation of metrics or the user queries. This sequence is
expressed in **felyx** as JobOrders.

Each of the steps that can be chained to each other is implemented by a
python module called **plugin**. New plugins can be added to the system backend
to extend the variety of available processing tasks that can be called by a
JobOrder.

.. digraph:: simple_sequence

	rankdir=LR;start -> "step 1";"step 1" -> "step 2";"step 2" -> "step 3";"step 3" -> end;start [shape=Mdiamond];end [shape=Msquare];

Felyx workers perform the operations described in a JobOrder by retrieving the
code associated with each step and chaining their execution (the plugin).

.. digraph:: simple_sequence_execution

    rankdir=LR;"input" -> "step 1" -> "output 1" -> "step 2" -> "output 2" ->"step 3" -> "output";"input" [shape=Mdiamond];"output" [shape=Msquare];"output 1" [shape=box];"output 2" [shape=box]


Each operation / step in the JobOrder is described by two properties:

**taskID**
   An identifier that is associated with the step implementation: the job
   processor will retrieve the code that realizes the operation and apply it on
   the input. We put here the name of the plugin class.

**args**
   A dictionary that contains the arguments to pass to the code that realizes
   the operation.

An example::

      "sequence": [ { "taskID": "list_directory"
                    , "args": {}
                    }
                  , { "taskID": "write_to_file"
                    , "args": {"filepath": "/tmp/result.list"}
                    }
                 ]


Create a plugin
===============

Felyx plugins are Python modules that expose some of their methods. These method
perform the operations described in the job order.

An example of package defining metrics operator plugins can be found in
`felyxcontribs_metrics_base`_ package.


Plugin skeleton
---------------

The following code is a magnificent Felyx plugin that does... nothing. This is
the bare minimum piece of code that Felyx will recognize as a plugin::

    # -*- coding: utf-8 -*-
    from felyx.processors.plugins import PluginMetadata, operator

    info = PluginMetadata( 'dummy', 'DummyyPlugin'
                         , ['has_task', 'perform', 'is_reducer', 'expects_uri'])


    class DummyPlugin(object):
        """Plugin skeleton for Felyx"""

        # Operators
        # -------------------------------------------------------------------------

        # Plugin interface implementation
        # -------------------------------------------------------------------------
        @staticmethod
        def has_task(task_id):
            return  False

        @staticmethod
        def is_reducer(task_id):
            return False

        @staticmethod
        def expects_uri(task_id):
            return False

        @staticmethod
        def perform(task_id, task_name, *args, **kwargs):
            return False, None, '...'


Create a directory named *dummy* and an *__init__.py* file inside it.

Put the code in the *__init__.py* file: you have created a Felyx plugin \\o/

Step by step explanation
~~~~~~~~~~~~~~~~~~~~~~~~
::

    from felyx.processors.plugins import PluginMetadata, operator

Import PluginMetadata (to describe the plugin), and operator (a Python decorator
that will be used on the methods to expose).

::

    info = PluginMetadata( 'empty', 'EmptyPlugin'
                         , ['has_task', 'perform', 'is_reducer', 'expects_uri'])

Create an *info* property that will be used to find the right plugin for a
specific operation.

The signature of the PluginMetadata constructor is:
   ``PluginMetadata( identifier, classname, capabilities )``

* *identifier* --  Use the name of the directory that contains the code as
  identifier (*dummy* in this example), this will make things easier to debug :).

* *classname* --  The name of the class that implements the capabilities.

* *capabilities* --  PluginMetadata can be used in several plugin systems, which may 
  rely on different mechanisms and require plugins to provide various information.
  Capabilities list the information that the plugin can provide, ie. it tells
  if the plugin is compatible with the plugin system.
  Unless you customized the core plugin system of Felyx, you should set the
  same capabilities as those described here.

::

    @staticmethod
    def has_task(task_id):
        return  False

Tell if the plugin has an implementation for the operation named *task_id*.

::

    @staticmethod
    def is_reducer(task_id):
        return False

Tell if the operation *task_id* takes several inputs to generate one ouput.

::

    @staticmethod
    def expects_uri(task_id):
        return False

Tell if input must be written on disk before performing the operation *task_id*.

::

    @staticmethod
    def perform(task_id, task_name, *args, **kwargs):
        return False, None, '...'

Perform the operation *task_id*.

* *task_id* -- identifier of the operation to perform

* *task_name*  -- Position of the operation in the sequence [TODO: rename it]

* *args* -- input can be found in args[0]

* *kwargs* -- additional parameters (the "args" property of the operation in the job order)

This method should return three values:

* a boolean that indicates the success of the operation

* the output of the operation

* An additional message (the log of the operation, an error message, etc...)


Exposing operations
===================

Right now our plugin does nothing, shall we make it work a little?

Add this code inside the EmptyPlugin class source code::

   @staticmethod
   @operator('misc', 1, str)
   def list_directory(path):
       import os
       return str(os.listdir(path))

By being annotated by the *operator* decorator the *list_directory* this method
will be available in Felyx.

The operator signature:
   ``operator( category, cost, *args)``

* *category* -- Operations can be categorized (this has no direct impact, it
  will just appear in the metadata).

* *cost* -- Estimation of the cost (arbitrary unit) of the operation, this will
  be used for load-balancing.

* *\*args* -- The remaning arguments are the detailed description of each operation
  argument. These descriptions are displayed in the administration operator forms, when
  defining new metrics and must be documented with care. Example::

   {'type': list, 'userDefined': False, 'doc': 'A list of Data objects for analysis'}
   , {'type': str, 'userDefined': True, 'doc': 'The name of the field to test for ice presence in (by default: sea_ice_fraction). The field must be 0 for no ice, and positive value otherwise'}
   , {'type': bool, 'userDefined': True, 'doc': 'Limit analysis to pixels strictly within the felyx site, False to analyse all pixels in the miniprod'}
   , {'type': list, 'userDefined': True, 'doc': 'A list of restriction dictionary elements'}

Each argument description consists of:

  * *type*: expected python type of the argument.
  * *userDefined*: boolean indicating if the field can be exposed and modified by
    the user. If false, it is hidden to the user.
  * *doc*: docstring for the argument.


.. warning::
   Operation MUST return a value, this is required to chain operations.


