.. _reading-data:

========================
Reading EO data in felyx
========================

The miniprods created by **felyx** are all in a single format (NetCDF), using a
common representation for similar acquisition patterns (grid, swath,
along-track data). However, these miniprods are extracted from input data files
that can come in a wide diversity of formats. This section explains how to
manage additional formats into felyx.

Format plugin
=============

The software layer supporting the access to input EO data is managed by a
specific python package, the **cerbere** library implemented by Ifremer.

**cerbere** is a python package with two layers of abstraction:

* the **Dataset** classes offer the same API to any data format: each format
  or groups of formats is managed by a dedicated python module. A Dataset
  object allows to inspect and read the data content of a EO data file.
* the **Feature** classes provide common observation data structures (swath,
  grid,   trajectories,...). It provides a generic typed template to store the
  content of a data file. A data file is the serialization of such a
  structure with its content. The data read from a mapper should be mapped and
  stored into such a template class, allowing for applying specialized
  operations per type of
  observation pattern. When saving the content of a datamodel object to a file,
  it will always be saved in the same format (same dimension and variable names
  for spatial and temporal information, same list of predefined attributes, etc
  ...) providing uniformity and homogenization to all the data saved using
  **cerbere**.

.. image:: ../_static/data_agnosticity.png


The **cerbere** package comes with a initial list of `Dataset` classes,
implemented for some common product formats.

New classes can be added to this library, through a mechanism of discoverable
plugins. To understand the concept of Dataset and learn how to implement a
new class, please refer to the **cerbere** documentation.


