.. _system-configuration:

====================
System configuration
====================

This section describes how to edit a **felyx** system configuration file.
A system configuration file is not required to use ``felyx-processor``
commands. However there are a number of cases where it is necessary:

* if you are using an Elasticsearch server (to index dynamic sites data
  instead of using parquet data files, or to index the extracted matchups
  instead of using the manifest files).
* if you are using the :ref:`felyx-report-details` extension in which case you
  need to provide the ``Elasticsearch`` and ``Grafana`` sections to define how
  felyx can connect to these services.

The system configuration file is in YAML format. It can be referenced to
through command line arguments when running a **felyx** command, or by
setting the ``FELYX_SYS_CFG`` environment variable (in which case **felyx**
commands will automatically load it).


General settings
================

.. warning::

   many of the settings below are not relevant anymore in **felyx** version 2.
   This list will be updated soon.

The system configuration should provide a minimum of general settings, as in
the example below:

.. code-block:: yaml

    #------------------------------------
    # Felyx system configuration file
    #------------------------------------
    # Access to local server cache
    shared_workspace_root: ./data

    # Plugins root directory
    plugins_root: conf/plugins

    # Plugins metadata directory
    metadata: conf/plugins/.metadata

    # Public subdirectory name for miniprods
    felyx_public_folder_name: public

    # The username for the REST connection
    default_felyx_user: felyx

    # The password for the REST connection
    default_felyx_password: felyx

    # Root directory for miniprods
    default_felyx_miniprod_store: data

    # Netcdf format of the miniprods
    nc_format: NETCDF4

    # Miniprods NetCDF global attributes values
    acknowledgement:
    miniprod_license:
    miniprod_format_version: "Felyx 2.0"
    miniprod_references:

    # Type of link of the miniprods from the public directory to the individual collection folders (values : soft or hard).
    link_mode: soft

    # Insitu collections mappings directory
    insitu_mappings_dir: conf/insitu_mappings




**shared_workspace_root**
   Directory where workspaces will be created.

   The directory must be writable by the scheduler **and** the workers.

**plugins_root**
   Directory where the plugins will be stored after registration.

   The directory must be writable by the scheduler **and** the workers.

**metadata**
   Directory where the metadata about plugins will be stored.

   The directory must be writable by the scheduler.
   
**felyx_public_folder_name**
   The name of the directory where all felyx public miniprods will be kept.
   
   This should either be 'public' or the name of the administrator.
   
**default_felyx_api**
   The root URL of the API for this felyx instance. If your felyx is a 
   'mothership' then it must be your instance. You may set this to another 
   felyx instance if your are not running a felyx frontend, but you MUST 
   know the credentials of a privileged user on that instance.
   
   E.g. "http://127.0.0.1:8000/api/v1/" or "http://felyx.esa.int/felyx/api/v2"

**default_felyx_miniprod_store**
   The root directory of the output location for all miniprods. Within this 
   directory all private miniprods will be stored under a directory named 
   for the owners username, and all public miniprods will be stored in the 
   folder with the name set by FELYX_PUBLIC_FOLDER_NAME.

**nc_format**
   The format of the output miniprods. Must be one of NETCDF3_CLASSIC, 
   NETCDF3_64BIT, NETCDF4_CLASSIC, and NETCDF4. This should be set to and 
   left as NETCDF4.
   
**default_felyx_user**
   The felyx administrator user name that this felyx instance will operate under. This 
   must be privileged (django staff) level user in order to process any
   miniprods from private collections. This user should be the owner of
   the public sites and public collections for this instance.
   
**default_felyx_password**  
   The password for default_felyx_user.
   
**acknowledgement**
   Instruction to the data users of what acknowledgment to include when 
   utilising or publishing your felyx data. E.g: Please acknowledge the 
   use of these data with the following statement : these data were obtained 
   from the Centre de Recherche et d Exploitation Satellitaire (CERSAT), at 
   IFREMER, Plouzane (France) on behalf of Felyx project.
   
**miniprod_license** 
   The name of the license, or the licenseing terms, under which you will 
   release your miniprods. E.g. Ifremer felyx data use is free and open.
    
**miniprod_format_version**
   The Felyx format version of your miniprods. The initial release of 
   felyx should use: Felyx 1.0.
   
**miniprod_references**
   A reference or hyperlink to an academic or technical document describing 
   your felyx implementation. Check the felyx project website for the latest 
   version of this. If you have significantly modified the felyx code, you 
   should augment this value with a reference of your own

**link_mode**
   Where a miniprod exists in several collections, the original copy shall 
   be placed in FELYX_PUBLIC_FOLDER_NAME and all other copies shall be links 
   to that copy. Set this value to "soft" to produce symlinks, or "hard" to 
   produce hard links. Hard links are more robust, and safer UNLESS you are 
   using a network storage solution such as NFS, in which case they WILL 
   FAIL. Only use hard links on physical file systems.


Elasticsearch
=============

This section of the system configuration file defines the Elasticsearch server
access and operations settings. Elasticsearch is required only if dynamic
sites, extracted child products or metrics are indexed. It is also required
if using the :ref:`felyx-report-details` extension for monitoring a routine
production.

.. code-block:: yaml

    # Elastic search configuration
    elasticsearch:
      # URL of the ES server, credentials and port
      url: "https://isi_cersat_felyx_rw:****************@elasticsearch7-val.ifremer.fr:9200"
      # Prefix for the ES indexes
      prefix: "isi_cersat_felyx_report_"

      # Default configuration for ES operations
      # Size of data returned by search request
      chunksize: 5000
      # Timeout in seconds for a search request
      timeout: 10s
      # Delay of retention in minutes after a scroll request
      scroll_retention_time: 5m

      operations:
        # Configuration for search operation (used for reading insitu data)
        insitu_search:
          chunksize: 8000
          timeout: 20s
          scroll_retention_time: 6m

        # Configuration for index operation (used for registering miniprods)
        miniprod_index:
          chunksize: 5000
          timeout: 10s


**elasticsearch:**
    **url:** URL of the ES server, credentials and port. Example: https://user:password@host:9200

    **prefix:**  Prefix for the Elasticsearch indices. An index stores the reports of a given MMDB configuration. Example: `felyx_report_`

    **Default configuration for ES operations**

    The following entries are defined for default Elasticsearch operations.

    **chunksize:** Size of data returned by requests (in bytes). Example : 5000.

    **timeout:** Timeout in seconds for requests (in seconds). Example : 10s.

    **scroll_retention_time:** Delay of retention in minutes after a scroll request. Example : 5m.

    **operations:**
        Configuration of Elasticsearch insitu data search operation (optional).

        The following entries can be added to define specific settings for insitu data reading requests.

        **insitu_search:**
            **chunksize:** Size of data returned by insitu search requests (in bytes).

            **timeout:** Timeout in seconds for insitu search requests (in seconds).

            **scroll_retention_time:** Delay of retention in minutes after a insitu scroll request.


Grafana
=======

This section of the system configuration file defines the Grafana server
access, dashboards and alert settings. Elasticsearch is required only if using
the :ref:`felyx-report-details` extension for monitoring a routine production.

.. code-block:: yaml

    # Grafana configuration
    grafana:
      host: 'grafana.swarm-test.ifremer.fr'
      api_key:  '***************************'
      ssl_verify: False
      search:
        api_endpoint: '/api/search?query=%'
        dashboard_type: 'dash-db'
      dashboards:
        plugin_version: '8.4.2'
        time_range_from_days: 1370
        time_range_to_days: 1339
      alert_rules:
        api_endpoint: '/api/ruler/grafana/api/v1/rules/'
        alert_evaluation_period: '1m'
        alert_firing_delay: '5m'
        alert_relative_timerange_start: 864000
        alert_relative_timerange_end: 0
      notifications:
        api_endpoint: '/api/alertmanager/grafana/config/api/v1/alerts'
      datasources:
        type: 'elasticsearch'
        access: 'proxy'
        url: 'https://elasticsearch7-val.ifremer.fr:9200' # TODO : merge common keys with elasticsearch credentials ?
        password: '**************************'
        user: 'isi_cersat_felyx_rw'
        basic_auth: True
        time_field_name: 'report_date'
        json_data:
          es_version: '7.0.0'
          max_concurrent_shard_requests: 5
      root_folder: 'felyx_report_test'



**grafana:**
  **host:** URL of the Grafana server host. Example:  'grafana.domain.country'.

  **api_key:** Grafana API key string (This key must be previously generated with the Grafana UI).

  **ssl_verify:** Enables the SSL verify option for HTTPS protocol. Protocol HTTPS is used if set to True, protocol HTTP is used otherwise.

  **search:**
    The following entries define the Grafana search configuration.

    **api_endpoint:** The Grafana search API endpoint. Example : /api/search?query=%.

    **dashboard_type:** The Grafana dashboard type. Set to : 'dash-db'.

  **dashboards:**
    The following entries define the dashboards configuration.

    **plugin_version:** The Grafana plugin version. Example: '8.4.2'.

    **time_range_from_days:** Specify the number of days from today for the time range start of the dashboards. Example : 10.

    **time_range_to_days:** Specify the number of days from today for the time range end of the dashboards. Example : 1.

    A value of 0 means the time range end is set to today.

  **alert_rules:**
    The following entries define the alert rules configuration.

    **api_endpoint:** The Grafana alert API endpoint. Example : /api/ruler/grafana/api/v1/rules/

    **alert_evaluation_period:**  Specify how often Grafana should evaluate the alert rules.

    Example : 1m means the evaluation period is set to 1 minute.

    **alert_firing_delay:** Once condition is breached, alert will go into pending state. If it is pending for longer than the "for" value, it will become a firing alert. Example: 5m means a pending alert will turn to firing state after 5 minutes

    **alert_relative_timerange_start:** Specify the time range start for alert evaluation. The value is expressed in seconds relative from now.

    Example : 86400 means the time range start is set to one day before now.

    **alert_relative_timerange_end:** Specify the time range end for alert evaluation. The value is expressed in seconds relative from now.

    Example : 0 means the time range end is set to now.

  **notifications:**
    The following entries define the alert notification configuration.

    **api_endpoint:** The Grafana notification API endpoint. Example /api/alertmanager/grafana/config/api/v1/alerts

  **datasources:**
    The following entries define the Grafana datasources configuration.

    **type:** The type of the datasource. Set to : elasticsearch.

    **access:** The Grafana datasources access mode, proxy or direct (Server or Browser in the UI). Set to : proxy.

    **url:** The URL of the Elasticsearch server. Example:  https://host:9200.

    **user:** The Elasticsearch user.

    **password:** The Elasticsearch password

    **basic_auth:** Enable or disable authentication using Basic Auth, with the given user and password above. Set to : True.

    **time_field_name:** The name of Elasticsearch time field used by Grafana. Set to : field report_date.

    **json_data:**
      **es_version:** The Elasticsearch version. Example: '7.0.0'.

      **max_concurrent_shard_requests:** Maximum number of concurrent shard requests that each sub-search request executes per node. Set to: 5

  **root_folder:** The name of the folder which will contain the Grafana dashboards. Example : felyx_report.

