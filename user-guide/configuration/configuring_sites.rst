.. _configuring-sites:

=========================================
Configuring and managing site collections
=========================================

Overview
========

The sites are the locations over which felyx extracts subsets of the input
data.

.. Important::

   To familiarize yourself with this concept before going further into the
   configuration aspect, check first this :ref:`introduction to sites and site
   collections <sites_and_collections>`. 


These sites and site collections can be defined manually through the
administration web services, as seen in the section on data configuration, but
can also be imported or exported through a dedicated web services.

This is particularly useful for some specific operations:

  * adding to the system a predefined list of sites
  * adding at once a large (more than 5) collections of sites in a automatic
    manner from their description layed out into a simple ascii file
  * exporting a collection of sites to another **felyx** server
  * updating the list of time and location of dynamic sites

The export & import mechanism of **felyx** sites makes indeed possible to 
duplicate the definition of a collection and its sites from one felyx instance
to another.

  .. image:: /_static/collection_sharing.png

.. Note::
     This is the fastest way to create a collection. However it works with some
     limitations. Static sites can not be defined twice (the service raises an
     error) : it is therefore not possible to import a collection containing sites
     already defined (with the same code) in your instance.



Static sites
============

Individual static sites can be defined easily through the administration
frontend. However, to exchange full collections of sites with another **felyx**
server, the **export** and **import web services** should be used instead.

Site export
-----------

Calling the ``csv`` service in GET mode will return the list of sites stored in
a "master" server, in CSV format. This list can then, for example, be provided to
the administrator of another **felyx** server to ingest the very same
collections of sites in his own server and later allow cross queries over the
same sites between the two servers.

.. Important::

   You need to set the ``type`` keyword to ``static`` to export static sites
   or collections.  


For instance to get the full list of static sites of an instance, use the
``type`` argument::

  curl -XGET 'http://www.ifremer.fr/cersat1/exp/felyx/services/csv/?type=static'


It is also possible to retrieve the sites of a set of collections only, using
the ``collections`` argument with a comma separated list of collection
identifiers:: 

  curl -XGET 'http://www.ifremer.fr/cersat1/exp/felyx/services/csv/?type=static&collections=GlobColour,GHRSST'


Site import
-----------

Calling the ``csv`` service in POST mode will push a list of sites in CSV
format to a **felyx** instance.

.. Important::

   You need to set the ``type`` keyword to ``static`` to import static sites
   or collections.  

For instance to ingest and create a complete list of static sites::

  curl -XPOST -F type=static -F file=@static_sites.csv 'http://www.ifremer.fr/cersat1/exp/felyx/services/csv/'


Format of static sites
----------------------

Sites and site collections can be imported or exported as csv files, using the
RESTful services described above. This section describes how these csv files
are formatted. When importing sites or site collections in **felyx**, your csv
file, posted in your REST URL call, must comply to this format or the ingestion
will fail.

Static sites are listed in the CSV file. Multiple sites can be provided in a
single CSV file (one line per site). Each site is defined by the following
sequence of fields, separated by ``;``:

  1. a **unique site identifier** (code without any space character, max 64 char.):
     it can be a project code (‘ghr066’) or the identifier of a moored buoy (‘65035’).
  2. a **full name** for user friendly identification of the site .
  3. a textual **description**.
  4. a **polygonal shape**, expressed as a wkt string:
     ``POLYGON((lon lat, lon lat, ...))``.
  5. a **list of site collection identifiers**: the identifiers of the groups
     to which this site is associated (usually defined a as a group of similar
     interest such as ‘ghrsst’, ‘ioccg’, ‘ndbc’,...). If the collection is not
     yet existing, it is created and added to the current list of collections.
  6. a **list of tags** characterizing the site (and used later as search
     filters), expressed as : ``{tag1,tag2,tag3,...}``
  7. an **owner name** (must be one of the users registered into the felyx
     instance)

Optional fields can be left empty (``;;``).

Example of a csv site file::

    # one site definition per line
    # site identifier ; site full name ; description ; shape (expressed as POLYGON( lon lat, lon lat, ...)); collections (expressed as {collection 1, collection2,...}) ; tags (expressed as {tag1,tag2,tag3,...}) ; owner
    ndb051 ; Bering Sea ; site centered on NDBC buoy 46035 ; POLYGON((178.58 56.05, 178.58 58.05,­ 176.58 58.05,­ 176.58 56.05,­ 178.58 56.05)) ; {ghrsst,ndbc}  ; {sea_ice_area,cold_sst_area,arctic} ; ifremer
    ...



Dynamic sites
=============

Storage
-------

Though the trajectories are updated through the **import** service, they are
physically stored in a sqlite database. The stored trajectories can therefore
be directly inspected by the administrator with SQL commands.

The database for these dynamic sites is stored in ``$shared_workspace_root/trajectories.db``
sqlite file with ``shared_workspace_root`` defined in your felyx configuration (pointed to by
``FELYX_CFG`` environment variable).


Site export
-----------

Calling the ``csv`` service in GET mode will return the list of sites stored in
an instance, in CSV format.

To get the full list of dynamic sites of an instance, use the ``type=dynamic`` argument::

  curl -XGET 'http://www.ifremer.fr/cersat1/exp/felyx/services/csv/?type=dynamic'


It is also possible to retrieve the sites of a set of collections only, using
the ``collections=<your collection>`` argument with a comma separated list of collection
identifiers::

  curl -XGET 'http://www.ifremer.fr/cersat1/exp/felyx/services/csv/?type=dynamic&collections=drifters'


Site import
-----------

Calling the ``csv`` service in POST mode will push a list of sites in CSV format
to a **felyx** instance. This is the way you update your base of trajectories in
**felyx**.

To create or update a list of dynamic sites::

  curl -XPOST -F type=dynamic -F file=@dynamic_sites.csv 'http://www.ifremer.fr/cersat1/exp/felyx/services/csv/'

This sends to the front-end the list of trajectories defining the dynamic sites.
These trajectories are sent by the front-end to the back-end where they are ingested
and stored. This process may take some time if you have many trajectories.

The above command returns a json message as follow:

.. code-block:: bash

   {"status": "ok", "msg": "Upload complete.\nTrajectories update task sent to backend, you can check status at http://felyx.cersat.fr/services/csv/status/?update_id=f6833f5f-35de-4e06-9647-6b5eeb818676\nDon't forget to use felyx-cache to update backend database"} 

By checking the returned URL, you can check if the ingestion is completed in the back-end: 

.. code-block:: bash

   {"result": [{"status": "ONPROGRESS", "completion_time": "", "upload_time": "2016-01-20T11:20:06.585821+00:00", "update_id": "f6833f5f-35de-4e06-9647-6b5eeb818676"}]}

The ingestion is completed when the status is COMPLETED instead of PENDING:

.. code-block:: bash

   {"result": [{"status": "COMPLETE", "completion_time": "2016-01-20T11:20:51.251273+00:00", "upload_time": "2016-01-20T11:20:06.585821+00:00", "update_id": "f6833f5f-35de-4e06-9647-6b5eeb818676"}]}

.. note::

   Note that when updating a dynamic site, the new trajectory locations are
   appended to the locations already stored in felyx database. If they overlap in
   time previously existing locations for the same time frame, they will replace
   the previous ones.


Format of dynamic sites
-----------------------

parquet format
**************

Dynamic sites are provided in parqiet



Dynamic sites are listed in the CSV file. Multiple sites can be provided in a
single CSV file. Each list of trajectories is defined by the following
sequence of lines:

  * a first line providing the static description of the site, including the
    following fields, separated by semicolon:

    1. a keyword **target** indicating we enter the description of a new site.
       **Mandatory**.
    2. a **unique site identifier** (code without any space character, max 64 char.):
       it can be a project code (‘ghr066’) or the identifier of a moored buoy (‘65035’).
       **Mandatory**.
    3. a **full name** for user friendly identification of the site. **Optional**.
    4. a textual **description**. **Optional**.
    5. a **list of site collection identifiers**: the identifiers of the groups
       to which this site is associated (usually defined a as a group of similar
       interest such as ‘isar’, ‘argo’, ‘drifting-buoys’,...). If the collection
       is not yet existing, it is created and added to the current list of collections.
       **Optional**.
    6. a **list of tags** characterizing the site (and used later as search
       filters), expressed as : {tag1,tag2,tag3,...}. **Optional**.
    7. an **owner name** (must be one of the users registered into the **felyx**
       server). **Optional**.
  * a second line with only the keyword **positions**.
  * a list of lines, one for each time and location of the trajectory with
    fields separated by semicolons, in the following order:

    1. longitude
    2. latitude
    3. time expressed as YYYYMMDD**T**hh:mm:ss

Optional fields can be left empty (``;;``).

.. note::

   it is recommended to fill in all fields the first time a site is created. They can
   be left blank (semicolon separated). If they are provided for a site already
   existing, they will be ignored.


Example of a csv site file::

  target;CpF;Cap Finistere;ISAR in-situ skin SST data, collected for the validation of AATSR and SLSTR SST products;{isar};{};felyx;
  position;
  -1.09743;50.81148;20111102T10:51:24;
  -1.09748;50.81145;20111102T10:55:00;
  -1.09752;50.81150;20111102T10:58:37;
  -1.09743;50.81150;20111102T11:02:14;
  -1.09682;50.81195;20111102T11:05:51;
  -1.09730;50.81161;20111102T11:09:29;
  -1.09733;50.81158;20111102T11:13:06;
  -1.09708;50.81175;20111102T11:16:43;
  -1.09603;50.81247;20111102T11:20:21;
  -1.09618;50.81248;20111102T11:23:59;


Querying dynamic sites
----------------------

A dedicated service is also provide to returns a JSON object containing the positions [lon, lat, time] for a list of dynamic sites between two dates::

  curl -XGET 'http://www.ifremer.fr/cersat1/exp/felyx/services/positions/?sites=tst001,tst002&start=20120510153000&stop=20130101021500'

This service is used in particular by the miniprod processing system in the system backend.



Data
====

Real data (such as in situ measurements collected along some moving platform -
buoy, ship, etc...) can be ingested into felyx.

The properties of these data must be first registered::

  felyx-register-insitu-mappings cmems_wave ./test/cmems_wave.json



felyx-import-csv insitu test/202001_cmems_wave_data.csv --dataset cmems_wave

