.. _sibling:

================
Sibling datasets
================

Some datasets are very similar to each other, and their respective files can be
matched one to one. They are referred to as **sibling datasets**. For one file
of dataset A, there is a matching file in dataset B spanning over the same
acquisition time frame, covering the same footprint on the same grid (or a
grid that can be match applying some transform factor).

Such datasets include:

* datasets corresponding to a different processing algorithm or version of
  the same input data.
* datasets corresponding to a different processing level (L1, L2) of the same
  instrument data.

In such a scenario, we can take advantage of the fact that the match-up
search was already performed on one of the sibling datasets, and skip this
processing step for the other related datasets, then optimizing the usage of
processing resources and time.

Configuration
=============
Relating a dataset to a parent sibling from which the match-ups will be
inferred is done by adding a ``sibling`` configuration block in ``MMDatasets
/ <MMDB identifer> / eo_datasets / <dataset identifier>`` block.

This block is defined as a YAML dictionary with the following keys:

* ``identifier`` : the identifier of the parent sibling dataset, as defined
  in the ``EODatasets`` configuration block
* ``scaling``: a dictionary where keys are dimension names within the source
  file from which the matchups are sliced, and values are the multiplying
  factor to apply to the indices in the slices extracted from the parent
  sibling.

.. code-block:: yaml

    MMDatasets:
      S3A_SL__OPE_NRT__cmems_drifter:
        site_collection: cmems_drifter
        eo_datasets:
          S3A_SL_2_WCT__OPE_NRT:
            subset_size: 21
            matching_criteria:
              time_window: 360
              search_radius: 5.
            sibling:
              identifier: S3A_SL_2_WST__OPE_NRT
              scaling:
                row: 1
                cell: 1
          S3A_SL_2_WST__OPE_NRT:
            reference: True
            subset_size: 21
            matching_criteria:
              time_window: 360
              search_radius: 5.

In above example, `S3A_SL_2_WCT__OPE_NRT` is a sibling of
`S3A_SL_2_WST__OPE_NRT`. There is no need to run ``felyx-extraction``
command on `S3A_SL_2_WCT__OPE_NRT`, only `S3A_SL_2_WST__OPE_NRT` should be
processed. When assembling multi-match-up dataset files (``felyx-assemble``
command), **felyx** will infer the match-ups for `S3A_SL_2_WCT__OPE_NRT` from
the list of match-ups extracted from `S3A_SL_2_WST__OPE_NRT`; it will extract
the corresponding data subsets from  `S3A_SL_2_WCT__OPE_NRT` source files,
using the slices provided with `S3A_SL_2_WST__OPE_NRT` match-ups. It will
apply the scaling factors defined for each slicing dimension in ``scaling`` to
the `S3A_SL_2_WST__OPE_NRT` slices before extracting from
`S3A_SL_2_WCT__OPE_NRT`. Here the scaling factors are all equal to 1, meaning
both datasets are on the same grid. If `S3A_SL_2_WCT__OPE_NRT` had twice the
resolution of `S3A_SL_2_WST__OPE_NRT` dataset, its data grid would be twice
as large in each dimension and a factor of 2 would be used there.

Locating the sibling's source data files
========================================
In order to properly locate the source files of `S3A_SL_2_WCT__OPE_NRT`,
**felyx** uses the path pattern defined in ``filenaming`` in the dataset
description block (``EODatasets / <dataset identifier>``) of
`S3A_SL_2_WCT__OPE_NRT` and the source file's acquisition start time (an
information stored with each match-up) from which the path pattern should be
dependent. For example:

.. code-block:: yaml

    filenaming: 'tests/data/eo/slstr-a/wct_granule/S3A_SL_2_WCT____%Y%m%dT%H%M%S_*.SEN3'

This requires the source files:

* to include the start time in their full path and file name, coded in
  ``filenaming`` using python date formatting convention.
* the subdirectories in the data tree to based also wrt to the acquisition
  start time of each file.

If this is not possible (for instance if the filenames do not contain the
acquisition start time), a dictionary can be passed to **felyx** that
contains the match between the parent sibling filenames and the filenames of
the dataset to process.

Execution
=========
``felyx-assemble`` will automatically process a dataset from its parent's
sibling pre-processed matchups if it finds the ``sibling`` block in its
configuration (even if match-ups were also pre-extracted for the dataset).
