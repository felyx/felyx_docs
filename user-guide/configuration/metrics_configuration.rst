===================
Configuring metrics
===================

.. highlights::

   Metrics in **felyx** are calculated values from the content of a miniprod, such
   as the mean or standard deviation of a given parameters or more complex
   operators. Metrics are defined per dataset and will be calculated for each site
   of a specified set of collections : a metrics definition is therefore
   associated with a dataset and a list of site collection.
   
   The metrics can be defined in a more user-friendly way through the **felyx**
   web interface. They can also be directly entered in their native description
   format, following the syntax described in this section.


General syntax
==============

Simple metrics
--------------

Simple metrics are defined by one operator (coded in a plugin) and a list of
arguments. The syntax is as follow:

.. sourcecode:: json

   [ // A list with only one element
  	  { // A dictionary with one key and one value
    		"mean": { // The TaskID of the plugin
      		"field": "sea_surface_temperature" // a dictionary with any arguments to be passed to the plugin.
    		}
  	  }
   ]


Complex metrics
---------------

several operators can also be chained sequentially to compute a more complex
metric. A metric as a sequence of operators is defined as follow.:

.. sourcecode:: json

   [ // A list of each element in the sequence
  	  { // A dictionary with one key (the task id of the plugin) and one value (the arguments)
         "task1": { // The TaskID of the plugin
      		"field": "sea_surface_temperature" // any arguments to be passed to the plugin.
    	 }
  	  }
  	  { // A dictionary with one key (the task id of the plugin) and one value (the arguments)
    	"task2": {
      		"field": "sea_surface_temperature" //  a dictionary with any arguments to be passed to the plugin.
    	}
  	  }
   ]


Common operator arguments
=========================

Common arguments offered by operator plugins to condition the processing of the
metrics include:

limit_to_site
-------------

The miniprod's bounding box usually includes fully the static site coverage or
the dynamic site neighbourhood but also pixels that are out of these
boundaries. The mask provided in the miniprods specifies for each pixel if it
belongs to the site boundaries (=1) or not (=0). 

  .. image:: /_static/mask_static.png

Most metrics operators provide an optional argument **limit_to_site** (by 
default set to `true`) restricting the computation of the metrics value over
the pixels within the site limits (red area in the image above). 
If **limit_to_site** is set to `false`, the metrics is computed over all pixels
of the extracted miniprod (blue and red areas).

must_have
---------

Conditions can be applied to data before calculation (field vs threshold) by
using the ``must_have`` keyword. This provides a list of dictionaries, where
each dictionary is a a criteria that each pixel must pass in order to be
included in the metric calculation. The format of the dictionary (in JSON) is:

.. sourcecode:: json

   {
    "field": "SSES_bias_error",
    "operator": "less",
    "value": 0.25
   }

In this case, pixels ‘must_have’ a ‘SSES_bias_error’ of ‘less’ than ‘0.25’ to be
included in the calculation. You can provide as many of these as you like, for
as many fields. The operator can be any of: ``eq``, ``ge``, ``gt``, ``mod``,
``le``, ``lte``, ``equal``, ``not_equal``, ``less``, ``less_equal``, ``greater``,
``greater_equal``, ``flag_bit_set`` and ``flag_bit_not_set``.

The ``flag_bit_set`` and ``flag_bit_not_set`` keywords take the bit number of a
flag as input. So, to filter out pixels which would have the bit 1 of a field
named ``rejection_flag`` (cosmetic flag of AATSR SST product), the format would
be:

.. sourcecode:: json

   {
    "field": "rejection_flag",
    "operator": "flag_bit_not_set",
    "value": 1
   }


This means that each pixel ‘must_have’ bit ‘1’ ‘not_set’ to to be included in the metric.

To expand on the earlier example, lets say we want the metric to include all
pixels (including those outside the site), but only it the SSES_bias_error is
less than (or equal too) 1K, AND the cosmetic flag is not set:

.. sourcecode:: json

   {
    "inputs": [{
        "datatype": "miniprod",
        "uri": "_miniprod_"
    }],
    "sequence": [{
        "args": {
            "field": "sea_surface_temperature",
            "limit_to_site": false,
            "must_have": [
                {
                    "field": "SSES_bias_error",
                    "operator": "less_equal",
                    "value": 1
                },
                {
                    "field": "rejection_flag",
                    "operator": "bit_flag_not_set",
                    "value": 1
                }
            ]
        },
        "taskID": "mean"
    }],
    "granularity": 1
   }



Testing
=======

Testing that the metrics have been correctly configured and run without error
on a dataset can be easily tested by using the command line metrics generation.

Use the ``--mode=local`` option to run the metrics processing locally, the
``--storage=file`` option to save the metrics in ascii files (instead of
registering them in the search engine) and the ``--output`` option to store 
these files in a test folder::  

  felyx-metric --output /tmp/ --no-clean --mode local --verbose --storage file <miniprod file> 


