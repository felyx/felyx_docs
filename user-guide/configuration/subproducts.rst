============================
Configuring MMDB subproducts
============================

It is also possible to create several MMDB subproducts instead of creating one
single mega output file with all the source variables inside. To do so, add the
``products`` block to the configuration of ``output`` block, replacing the
``content`` section, as shown in the following example:

.. code-block:: yaml

    EODatasets:
      S3A_SL_2_WCT__OPE_NRT:
        dataset_class: SAFESLWCTDataset
        feature_class: Swath
        # pattern of the full path to the EO data files wrt their time coverage
        # start. The organization of the data files into year, month, day etc...
        # subfolders must therefore be based on the time coverage start of the
        # data files. Time dependent formatting in the pattern uses python style for
        # date format.
        filenaming: /tcenas/s3optcal/data/satellite/slstr/level2/sl_2_wct____o_nr/data/s3a/%Y/%j/S3A_SL_2_WCT____%Y%m%dT%H%M%S_*.SEN3
        description: S3A SLSTR WCT OPE NRT L2 product
        level: L2
        name: S3A SLSTR WCT OPE NRT Level 2
        supplier: EUMETSAT

      S3A_SL_1_RBT_A__OPE_NRT:
        dataset_class: SAFESL500ADataset
        feature_class: Swath
        filenaming: /tcenas/s3optcal/data/satellite/slstr/level1/sl_1_rbt____o_nr/data/s3a/level1/sl_1_rbt____o_nr/data/s3a/%Y/%j/S3A_SL_1_RBT____%Y%m%dT%H%M%S_*.SEN3
        description: S3A SLSTR RBT A-Stripe OPE NRT L1 product
        level: L1
        name: S3A SLSTR RBT A-Stripe OPE NRT Level 1
        supplier: EUMETSAT

      S3A_SL_1_RBT_I__OPE_NRT:
        dataset_class: SAFESLIRDataset
        feature_class: Swath
        filenaming: /tcenas/s3optcal/data/satellite/slstr/level1/sl_1_rbt____o_nr/data/s3a/%Y/%j/S3A_SL_1_RBT____%Y%m%dT%H%M%S_*.SEN3
        description: S3A SLSTR RBT IR OPE NRT L1 product
        level: L1
        name: S3A SLSTR RBT IR OPE NRT Level 1
        supplier: EUMETSAT

      S3A_SL_2_WST__OPE_NRT:
        dataset_class: SAFESLWSTDataset
        feature_class: Swath
        # pattern of the full path to the EO data files wrt their time coverage
        # start. The organization of the data files into year, month, day etc...
        # subfolders must therefore be based on the time coverage start of the
        # data files. Time dependent formatting in the pattern uses python style for
        # date format.
        filenaming: /tcenas/s3optcal/data/satellite/slstr/level2/sl_2_wst____o_nr/data/s3a/%Y/%j/S3A_SL_2_WST____%Y%m%dT%H%M%S_*.SEN3
        description: S3A SLSTR WST OPE NRT L2 product
        level: L2P
        name: S3A SLSTR WST OPE NRT Level 2
        supplier: EUMETSAT

    Metrics: []

    SiteCollections:
      cmems_drifter:
        description: CMEMS In Situ TAC selection of SST drifter data
        name: CMEMS In Situ TAC drifters
        datasource_access: parquet
        parquet_config:
          filepath_pattern: /tcenas/s3optcal/scratch/felyx/data/insitu/cmems/cer_sst_insitu_db/parquet/v02/%Y/%j/%Y%m%d%H%M%S_cmems_drifter.parquet
          frequency: 6H


    MMDatasets:
      S3A_SL__OPE_NRT__cmems_drifter:
        site_collection: cmems_drifter
        eo_datasets:
          S3A_SL_2_WCT__OPE_NRT:
            subset_size: 21
            matching_criteria:
              time_window: 120
              search_radius: 5.
            sibling:
              identifier: S3A_SL_2_WST__OPE_NRT
              scaling:
                row: 1
                cell: 1

          S3A_SL_1_RBT_A__OPE_NRT:
            subset_size: 42
            matching_criteria:
              time_window: 120
              search_radius: 5.
            sibling:
              identifier: S3A_SL_2_WST__OPE_NRT
              scaling:
                row: 2
                cell: 2

          S3A_SL_1_RBT_I__OPE_NRT:
            subset_size: 21
            matching_criteria:
              time_window: 120
              search_radius: 5.
            sibling:
              identifier: S3A_SL_2_WST__OPE_NRT
              scaling:
                row: 1
                cell: 1

          S3A_SL_2_WST__OPE_NRT:
            reference: True
            subset_size: 21
            matching_criteria:
              time_window: 120
              search_radius: 5.


        common_options:
          create_miniprod: false

        output: # (optionnel, utile à la commande assemble matchup)
          # the temporal extent of each created match-up file
          granularity:
            # time units (any pandas units for date range: H for hour, m for
            # minute, ...)
            unit: D
            # duration in number of time units
            count: 1
          # history of in situ data to include with the matched measurement, as a
          # duration in minutes before and after the matching time
          history: 360
          # output file pattern (ignored if subproducts are defined)
          # default time units default time units in CF convention style (default is
          # 'seconds since 1950-01-01')
          default_time_units: seconds since 1980-01-01

          products:
            S3A_SL__OPE_NRT__cmems_drifter:
              # output file pattern for the subproduct
              filenaming: '%Y/%j/%Y%m%d%H%M%S_S3A_SL__OPE_NRT__cmems_drifter.nc'
              # tailor the content of the assembled files
              content:
                S3A_SL_2_WST__OPE_NRT:

                  # [Optional] list of variables to include in the assembled files (all
                  # of them by default)
                  variables: [.*]

                  # [Optional] variables NOT TO include in extracted child products
                  # (none of them by default)
                  #except_variables:

                  # [Optional] list of global attributes to include in the assembled
                  # files (all of them by default)
                  #attributes:

                  # [Optional] list of global attributes NOT TO include in the assembled
                  # files (none of them by default)
                  except_attributes:
                    - time_coverage_end
                    - time_coverage_start
                    - westernmost_longitude
                    - easternmost_longitude
                    - northernmost_latitude
                    - southernmost_latitude
                    - file_quality_level
                    - start_time
                    - stop_time
                    - history

                  attributes_as_variables:
                    # for traceability
                    - uuid
                    - date_created

                  # prefix by which to rename all variables and global attributes
                  # coming from this dataset (by default the dataset id is used)
                  prefix: wst

            S3A_SL__OPE_NRT__cmems_drifter__aux_met:
              # output file pattern for the subproduct
              filenaming: '%Y/%j/%Y%m%d%H%M%S_S3A_SL__OPE_NRT__cmems_drifter_aux_met.nc'
              # tailor the content of the assembled files
              content:
                S3A_SL_2_WCT__OPE_NRT:

                  # [Optional] list of variables to include in the assembled files (all
                  # of them by default)
                  variables:
                    - cloud_fraction_tx
                    - dew_point_tx
                    - p_atmos
                    - sea_ice_fraction_tx
                    - sea_surface_temperature_tx
                    - skin_temperature_tx
                    - specific_humidity_tx
                    - surface_pressure_tx
                    - temperature_profile_tx
                    - temperature_tx
                    - total_column_water_vapour_tx
                    - u_wind_tx
                    - v_wind_tx

                  # [Optional] variables NOT TO include in extracted child products
                  # (none of them by default)
                  #except_variables:

                  # [Optional] list of global attributes to include in the assembled
                  # files (all of them by default)
                  #attributes:

                  # [Optional] list of global attributes NOT TO include in the assembled
                  # files (none of them by default)
                  except_attributes:
                    - time_coverage_end
                    - time_coverage_start
                    - westernmost_longitude
                    - easternmost_longitude
                    - northernmost_latitude
                    - southernmost_latitude
                    - file_quality_level
                    - start_time
                    - stop_time
                    - history
                    - track_offset
                    - start_offset
                    - start_time
                    - stop_time
                    - product_name
                    - creation_time
                    - absolute_orbit_number

                  attributes_as_variables:
                    # for traceability
                    - uuid
                    - creation_time

                  # prefix by which to rename all variables and global attributes
                  # coming from this dataset (by default the dataset id is used)
                  prefix: met



            S3A_SL__OPE_NRT__cmems_drifter__aux_wct:
              # output file pattern for the subproduct
              filenaming: '%Y/%j/%Y%m%d%H%M%S_S3A_SL__OPE_NRT__cmems_drifter_aux_wct.nc'
              # tailor the content of the assembled files
              content:
                S3A_SL_2_WCT__OPE_NRT:
                  # [Optional] list of variables to include in the assembled files (all
                  # of them by default)
                  variables:
                    - D2_SST
                    - D2_SST_uncertainty
                    - D2_exception
                    - D3_SST
                    - D3_SST_uncertainty
                    - D3_exception
                    - N2_SST
                    - N2_SST_uncertainty
                    - N2_exception
                    - N3_SST
                    - N3_SST_uncertainty
                    - N3_exception
                    - cloud_in
                    - confidence_in
                    - cloud_io
                    - confidence_io
                    - sat_azimuth_tn
                    - sat_zenith_tn
                    - solar_azimuth_tn
                    - solar_zenith_tn
                    - sat_azimuth_to
                    - sat_zenith_to
                    - solar_azimuth_to
                    - solar_zenith_to

                  # [Optional] variables NOT TO include in extracted child products
                  # (none of them by default)
                  #except_variables:

                  # [Optional] list of global attributes to include in the assembled
                  # files (all of them by default)
                  #attributes:

                  # [Optional] list of global attributes NOT TO include in the assembled
                  # files (none of them by default)
                  # except_attributes:

                  attributes_as_variables:
                    # for traceability: filename
                    - source

                  # prefix by which to rename all variables and global attributes
                  # coming from this dataset (by default the dataset id is used)
                  prefix: wct

            S3A_SL__OPE_NRT__cmems_drifter__aux_rbt_i:
              # output file pattern for the subproduct
              filenaming: '%Y/%j/%Y%m%d%H%M%S_S3A_SL__OPE_NRT__cmems_drifter_aux_rbt_i.nc'
              # tailor the content of the assembled files
              content:
                S3A_SL_1_RBT_I__OPE_NRT:
                  # [Optional] list of variables to include in the assembled files (all
                  # of them by default)
                  variables:
                    - S7_BT_in
                    - S7_exception_in
                    - S7_BT_io
                    - S7_exception_io
                    - S7_T_detector_in
                    - S7_T_detector_io
                    - S8_BT_in
                    - S8_exception_in
                    - S8_BT_io
                    - S8_exception_io
                    - S8_T_detector_in
                    - S8_T_detector_io
                    - S9_BT_in
                    - S9_exception_in
                    - S9_BT_io
                    - S9_exception_io
                    - S9_T_detector_in
                    - S9_T_detector_io
                    - x_in
                    - y_in
                    - x_io
                    - y_io
                    - pointing_in
                    - pointing_io
                    - elevation_in
                    - elevation_io
                    - detector_in
                    - scan_in
                    - pixel_in
                    - detector_io
                    - scan_io
                    - pixel_io

                  # [Optional] variables NOT TO include in extracted child products
                  # (none of them by default)
                  #except_variables:

                  # [Optional] list of global attributes to include in the assembled
                  # files (all of them by default)
                  #attributes:

                  # [Optional] list of global attributes NOT TO include in the assembled
                  # files (none of them by default)
                  # except_attributes:

                  attributes_as_variables:
                    # for tracability: filename
                    - source

                  # prefix by which to rename all variables and global attributes
                  # coming from this dataset (by default the dataset id is used)
                  prefix: rbti


            S3A_SL__OPE_NRT__cmems_drifter__aux_rbt_a:
              # output file pattern for the subproduct
              filenaming: '%Y/%j/%Y%m%d%H%M%S_S3A_SL__OPE_NRT__cmems_drifter_aux_rbt_a.nc'
              # tailor the content of the assembled files
              content:
                S3A_SL_1_RBT_A__OPE_NRT:
                  # [Optional] list of variables to include in the assembled files (all
                  # of them by default)
                  variables:
                    - S1_solar_irradiance_an
                    - S1_solar_irradiance_ao
                    - S1_radiance_an
                    - S1_exception_an
                    - S1_radiance_ao
                    - S1_exception_ao
                    - S2_solar_irradiance_an
                    - S2_solar_irradiance_ao
                    - S2_radiance_an
                    - S2_exception_an
                    - S2_radiance_ao
                    - S2_exception_ao
                    - S3_solar_irradiance_an
                    - S3_solar_irradiance_ao
                    - S3_radiance_an
                    - S3_exception_an
                    - S3_radiance_ao
                    - S3_exception_ao
                    - S4_solar_irradiance_an
                    - S4_solar_irradiance_ao
                    - S4_radiance_an
                    - S4_exception_an
                    - S4_radiance_ao
                    - S4_exception_ao
                    - S5_solar_irradiance_an
                    - S5_solar_irradiance_ao
                    - S5_radiance_an
                    - S5_exception_an
                    - S5_radiance_ao
                    - S5_exception_ao
                    - S6_solar_irradiance_an
                    - S6_solar_irradiance_ao
                    - S6_radiance_an
                    - S6_exception_an
                    - S6_radiance_ao
                    - S6_exception_ao
                    - x_an
                    - y_an
                    - x_ao
                    - y_ao
                    - cloud_an
                    - confidence_an
                    - pointing_an
                    - cloud_ao
                    - confidence_ao
                    - pointing_ao
                    - elevation_an
                    - latitude_an
                    - longitude_an
                    - elevation_ao
                    - latitude_ao
                    - longitude_ao
                    - detector_an
                    - scan_an
                    - pixel_an
                    - detector_ao
                    - scan_ao
                    - pixel_ao

                  # [Optional] variables NOT TO include in extracted child products
                  # (none of them by default)
                  #except_variables:

                  # [Optional] list of global attributes to include in the assembled
                  # files (all of them by default)
                  #attributes:

                  # [Optional] list of global attributes NOT TO include in the assembled
                  # files (none of them by default)
                  # except_attributes:

                  attributes_as_variables:
                    # for tracability: filename
                    - source

                  # prefix by which to rename all variables and global attributes
                  # coming from this dataset (by default the dataset id is used)
                  prefix: rbta

