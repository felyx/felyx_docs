.. _processing-configuration:

========================
Processing configuration
========================
.. _Cerbere: https://cerbere.readthedocs.io/en/latest/

Each processing command in **felyx** requires a configuration file in yAML
format providing the processing details. This configuration is unique for all
processing steps for a given usage, though only some sections of this
configuration may be used by a given command.

A configuration file typically includes the following main sections:

* ``EODatasets``: definition of the Earth Observation datasets to be processed
  by felyx
* ``SiteCollections``: definition of the collections of sites over which
  the Earth Observation data are extracted
* ``Metrics``: an optional section for calculating metrics from the
  extracted Earth Observation subsets
* ``MMDatasets``: definition of the extraction criteria, as well as optional
  configuration information for assembling the extracted Earth Observation
  subsets into multi-matchup dataset files

This section leads you through the different sections of a configuration file.
The full example is provided :download:`here </examples/s3a_mmdb.yaml>`.

Input EO data (``EODatasets``)
==============================

The first section of the YAML configuration file describes the input Earth
Observation dataset(s) to be processed, in ``EODatasets``:

.. code-block:: yaml

    EODatasets:
      S3A_SL_2_WST__OPE_NRT:
        # dataset_class: the reader class name in cerbere library to read this
        # dataset ('NCDataset' for CF compliant data, 'GHRSSTNCDataset' for
        # GHRSST data, ...)
        # Check cerbere at: https://cerbere.gitlab-pages.ifremer.fr/cerbere/
        dataset_class: SAFESLWSTDataset

        # feature_class: the feature class name in cerbere library for the
        # observation pattern of this dataset. Possible values are: Swath,
        # CylindricalGrid, Trajectory
        # Check cerbere at: https://cerbere.gitlab-pages.ifremer.fr/cerbere/
        feature_class: Swath

        # pattern of the full path to the EO data files wrt their time coverage
        # start. The organization of the data files into year, month, day etc...
        # subfolders must therefore be based on the time coverage start of the
        # data files. Time dependent formatting in the pattern uses python style for
        # date format.
        filenaming: 'tests/data/eo/slstr-a/wst_granule/S3A_SL_2_WST____%Y%m%dT%H%M%S_*.SEN3'

      S3A_SL_2_WCT__OPE_NRT:
        dataset_class: SAFESLWCTDataset
        feature_class: Swath
        filenaming: 'tests/data/eo/slstr-a/wct_granule/S3A_SL_2_WCT____%Y%m%dT%H%M%S_*.SEN3'

        # defines a sibling dataset: a dataset with the same time, locations and
        # geometry. A sibling dataset can be used to speed up match-ups
        # extraction from this dataset, using the match-up properties already
        # processed for this sibling dataset.
        sibling:
          # the identifier of the sibling dataset
          dataset_id: S3A_SL_2_WST__OPE_NRT
          # the factor by which the slices corresponding to the extracted
          # match-ups from the sibling dataset have to be multiplied to extract the
          # same match-up from this dataset.
          slice_scale_factor: 2


``EODatasets`` is described as a dictionary where keys are the identifiers of
each EO datasets to be processed (here: ``S3A_SL_2_WST__OPE_NRT`` or
``S3A_SL_2_WCT__OPE_NRT``, which correspond to EUMETSAT Sentinel-3 Sea
Surface Temperature data from SLSTR instrument).

EO Data in **felyx** are read with the Cerbere_ API, which provides a unified
API to read any geolocated observation data. It can be extended through
reader plugins to decode new datasets, though it is able to read many
datasets in NetCDF format compliant to the Climate and Forecast convention
with its base reader ``NCDataset``. This API also provides a typology of
observation patterns (**features**) for which **felyx** implements
internally different extraction methods. The Cerbere reader and feature
classes to be used by felyx for a given EO dataset must be provided
respectively in ``dataset_class`` and ``feature_class``.

The feature classes handled by felyx currently include:

* ``CylindricalGrid`` : gridded data with independent lat, lon axes (like a
  numerical model output or Level 3/4 satellite datasets)
* ``Swath`` : satellite data in satellite projection with curvilinear lat, lon
  and time coordinates
* ``Trajectory`` : time dependent lat, lon and time coordinates, such as
  satellite along-track data (altimetry), ship cruises, ...

For more details in how to verify your data can be read with Cerbere and how
to extend Cerbere, check also the :ref:`reading-data` section.


Site collections (``SiteCollections``)
======================================
This section of the configuration defines the target sites over which the EO
data are extracted, grouped into collections of consistent platforms or
objects (e.g. for which the extraction criteria are similar, as well as the
data possibly attached to these sites, such as in situ measurements).

The ``SiteCollections`` section of the configuration corresponds to a
dictionary of site collections, where keys are the identifiers of the
collections.

As seen in previous sections, site collections may contain (exclusively)
**static** (stationary) or **dynamic** (moving location with time) sites. The
configuration will be different in each case.


Dynamic sites
-------------
This type of collection typically corresponds to in situ measurements collected
along a measuring platform (ship, drifting buoy, ...).

.. code-block:: yaml

    SiteCollections:
      trusted:
        description: CORIOLIS GDAC selection of SST TRUSTED data
        name: TRUSTED

        # source for the data associated with the sites (parquet or
        # elasticsearch)
        datasource_access: parquet

        # location and organisation of the parquet files
        parquet_config:
          # the path and date/time depended file pattern of the parquet file (in
          # python datetime formatting style)
          filepath_pattern: tests/data/insitu/parquet/trusted/%Y%m%d_trusted.parquet

          # the periodicity of the parquet files (panda style frequency)
          frequency: 6H

In this example (e.g. ``trusted`` collection), we define where the times and
locations (and possibly observation data) of the sites are stored.

the storage source provided in ``datasource_access`` can be Parquet files
(``parquet``) or Elasticsearch indexing engine (``elasticsearch``). Refer to
_configuring-sites section for more details on the site data storage.

In the case of Parquet files, the site data are stored in a single file or
split into several files covering a constant time period. The file pattern
(or path in the case of a single file) is set in ``filepath_pattern`` of
``parquet_config`` subsection.

For multiple files, the pattern must include the time formatting string of the
individual files in python datetime formatting style (the date string
corresponding to the starting time of the data in a file). The duration and
periodicity of each site data file is set in ``frequency`` using python panda
style.


Static sites
------------

This type of collection corresponds to predefined areas where extration must
be performed systematically for all input files (cal/val site, area of
interest for an application,...). It is the most basic usage of **felyx**, as
a simple extraction tool.

The defined areas can be of any polygonal shape.

.. code-block:: yaml

    SiteCollections:
      test4static:
        description: 'Static collection'
        datasource_access: inline
        sites:
          dummy1:
            name: test1_ok
            shape: POLYGON((50 -73,50 -72,57 -72,57 -73,50 -73))
          dummy2:
            name: test2_ok
            shape: POLYGON((16 -76,16 -80,37 -80,37 -76,16 -76))
          dummy3:
            name: test3_nok
            shape: POLYGON((0 0, 0 20, 20 20, 20 0, 0 0))


Compared to dynamic site configuration, here ``datasource_access`` must be
set to ``inline``. A additional configuration item, ``sites``, allows to
define the list of static sites as a dictionary where keys are the site
identifiers. In this definition, ``name`` is the full label of the site, and
``shape`` its geometry as a WKT string, which must be a polygon.



Extraction (MMDatasets)
=======================

The extraction configuration, in ``MMDatasets``  section of the YAML
configuration file, pairs EO datasets with the site collection(s) where
extraction should be performed. For dynamic site, which are moving in space
and time, match-up criteria are required to define under which proximity
conditions to a given site an extraction should be performed or not.

.. code-block:: yaml

    MMDatasets:
      S3A_SL__OPE_NRT__trusted:
        site_collection_id: trusted
        eo_datasets:
          S3A_SL_2_WCT__OPE_NRT:
            subset_size: 21
            matching_criteria:
              time_window: 360
              search_radius: 5.
            sibling:
              identifier: S3A_SL_2_WST__OPE_NRT
              scaling:
                row: 1
                cell: 1
          S3A_SL_2_WST__OPE_NRT:
            reference: True
            subset_size: 21
            matching_criteria:
              time_window: 360
              search_radius: 5.


The extraction pairs (site collection vs EO datasets) are defined as a
dictionary where keys are the identifiers (``S3A_SL__OPE_NRT__trusted`` in
above example).

An extraction must define the site collection in ``site_collection_id``, which
must be unique.

The EO dataset to be matched against this site collection is defined in
``eo_datasets`` as a dictionary where identifiers are the EO dataset
collection as defined in the ``EODatasets`` section.

It is possible to have more than one EO dataset in this dictionary if the
resulting child products (matchups) are to be grouped together in the
same output multi-matchup dataset files later (see :ref:`felyx-assemble`
section). If they are grouped in different files, they should be paired
in a different extraction item. When grouping two or more datasets in the
same multi-matchup file, ``reference``, set to True, indicates which one the
others should be joined to: all the matchups of the reference dataset will be
assembled into the output files, but only the matchups of the other dataset
which are also matchups of the reference (therefore anchored to the same
site location and time) will be joined into the multi-matchup dataset (as a
*join left** would do in a pandas DataFrame). When joined with the dynamic
site data, this creates *triple colocation* (or more) matchup datasets.


In the case of extraction over **dynamic sites**, the colocation criteria must
be defined (they can be skipped for static sites):

``search_radius`` defines the spatial distance, in km, between an EO Dataset
measurement (pixel, grid point) and the nearest dynamic site data.

``time_window`` defines the maximum absolute time difference between a EO
dataset measurement and the nearest dynamic site data, in minutes.

Extraction can be performed only if both above criteria are met. The matching
EO Dataset measurement is the one which minimizes the spatial distance then
time difference.

In the extraction process, one can extract more than the matching EO Dataset
measurement: a segment (for one dimensional data) or box (for
multi-dimensional data) can be extracted. The size of this subset is set in
``subset_size`` and must be an odd number. The extracted subset is centered
on the matching measurement.

Some EO datasets are very similar to each other, and their respective files
can be matched one to one. They are referred to as **sibling datasets**. For
one file of dataset A, there is a matching file in dataset B spanning over
the same acquisition time frame, covering the same satellite swath on the same
grid (or a swath or grid that can be matched applying some multiplying
transform factor). Processing can be greatly optimized in such a case by
taking advantage of this relationship. This relationship is described through
optional ``sibling`` configuration item. Refer to the
dedicated :ref:`sibling` section of this documentation.


Matchup assembling (MMDatasets)
===============================

When grouping matchups into multi-matchup files with ``felyx-assemble``
command, additional configuration must be provided in the configuration file
in the ``MMDatasets`` section, adding the ``output`` subsection.

We define, in the following, daily multi-matchup dataset files:

.. code-block:: yaml

    output:
      # the temporal extent of each created match-up file
      # duration in any pandas units for date range: H for hour, m for
      # minute, ...)
      frequency: 24H

      # history of in situ data to include with the matched measurement, as a
      # duration in minutes before and after the matching time
      history: 360

      # output file pattern (ignored if subproducts are defined)
      # filenaming:

      # default time units default time units in CF convention style (default is
      # 'seconds since 1950-01-01')
      default_time_units: seconds since 1980-01-01

      products:
        S3A_SL__OPE_NRT__trusted:
          # output file pattern for the subproduct
          filenaming: '%Y/%Y%m%d%H%M%S_S3A_SL__OPE_NRT__trusted.nc'
          # tailor the content of the assembled files
          content:
            S3A_SL_2_WCT__OPE_NRT:
              # [Optional] list of variables to include in the assembled files (all
              # of them by default)
              variables: [.*]

              # [Optional] variables NOT TO include in extracted child products
              # (none of them by default)
              #except_variables:

              # [Optional] list of global attributes to include in the assembled
              # files (all of them by default)
              #attributes:

              # [Optional] list of global attributes NOT TO include in the assembled
              # files (none of them by default)
              # except_attributes:

              attributes_as_variables:
                # for tracability: filename
                - source

              # prefix by which to rename all variables and global attributes
              # coming from this dataset (by default the dataset id is used)
              prefix: wct

            S3A_SL_2_WST__OPE_NRT:
              # [Optional] list of variables to include in the assembled files (all
              # of them by default)
              variables: [.*]

              # [Optional] variables NOT TO include in extracted child products
              # (none of them by default)
              #except_variables:

              # [Optional] list of global attributes to include in the assembled
              # files (all of them by default)
              #attributes:

              # [Optional] list of global attributes NOT TO include in the assembled
              # files (none of them by default)
              except_attributes:
                - time_coverage_end
                - time_coverage_start
                - westernmost_longitude
                - easternmost_longitude
                - northernmost_latitude
                - southernmost_latitude
                - file_quality_level
                - start_time
                - stop_time

              attributes_as_variables:
                # for traceability
                - uuid
                - date_created

              # prefix by which to rename all variables and global attributes
              # coming from this dataset (by default the dataset id is used)
              prefix: wst


The temporal extent of each output multi-matchup dataset is defined in
``granularity``, as a number (``count``) of time units (``unit``) in pandas
DateTimeIndex convention.

The dynamic site data colocated with the EO dataset will be added in the
assembling step to the output multi-matchup dataset, by setting ``history``
to a value higher than 0. The data ranging from minus to plus the provided
value, in minutes, around the matchup colocation time will be selected,
resulting possibly in adding a time series of multiple and variable
length measurements to each matchup depending on the frequency of the dynamic
site sampling.

The content of the output multi-matchup dataset files is defined in the
``products`` section. For a given elementary time range (defined in
``granularity``), one or multiple output files (products) can be generated with
different content: each of them is defined in ``products`` section, where
they keys are the identifiers of each product and the values their definition
consisting of a time dependent filename pattern in ``filenaming`` (the
date/time field in the filename, following python datetime formatting
convention, being filled with the start time of the covered time range) and
the variables and attributes to be dumped into the product in ``content``.

``content`` describes, for each EO source dataset to be included in a
product, the variables and attributes to be selected and/or excluded in the
ouput multi-matchup product, respectively in ``variables``,
``except_variables``, ``attributes``, ``except_attributes``.
``attributes_as_variables`` provides the list of global attributes from the
source EO data files to be converted into a variable (it is intended for
global attributes that have a different value in different source EO files
and therefore can not be preserved as a global attributes when stacking the
child products into the assembled file. Regular expressions using wild cards
etc can be used to select/exclude multiple variables or attributes at once.

Last, a prefix can be prepended to all the selected variables and global
attributes from a source EO dataset, using the ``prefix`` configuration item
(by default the dataset id is used). When merging different EO datasets in a
multi-matchup dataset, this prevents conflicts occurring from identical
variable or attributes in these datasets.
