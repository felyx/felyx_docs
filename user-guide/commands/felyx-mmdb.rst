==========================================
felyx-mmdb: multi-matchup dataset building
==========================================


Commandline
===========

.. program:: felyx-mmdb

End to end command to build multi-matchup datasets. It combines
``felyx-extraction`` and ``felyx-assemble`` commands while interacting with
jobard framework to distribute the processing over multiple processing nodes.


.. code-block:: bash

    usage: felyx-mmdb [-h] [--mmdb-id MMDB_ID [MMDB_ID ...]] [--no-jobard] [--ignore-failures] [--start START] [--end END] [--skip-sibling-extraction]
                      [--skip-extraction] [--mmdb-dir MMDB_DIR] [--manifest-dir MANIFEST_DIR] [--child-product_dir CHILD_PRODUCT_DIR] [-c SYSTEM_CONFIG]
                      [--felyx-extraction-bin FELYX_EXTRACTION_BIN] [--felyx-assemble-bin FELYX_ASSEMBLE_BIN] [--resume] [--cluster CLUSTER] [--split SPLIT]
                      [--workers WORKERS] [--cluster-ssh SSH] [-u URL] [--extraction-memory EXTRACTION_MEMORY] [--extraction-walltime EXTRACTION_WALLTIME]
                      [--assemble-memory ASSEMBLE_MEMORY] [--assemble-walltime ASSEMBLE_WALLTIME] [--logfile LOGFILE] [--logfile-level LOGFILE_LEVEL]
                      [-v | -q | -s]
                      mmdb_config

    positional arguments:
      mmdb_config           A configuration file with the settings of the match-up dataset to generate

    optional arguments:
      -h, --help            show this help message and exit
      --mmdb-id MMDB_ID [MMDB_ID ...]
                            the list of identifiers, within the MMDB configuration file of the matchup dataset to generate
      --no-jobard           skip usage of jobard orchestrator (runs the MDB in sequential mode)
      --ignore-failures     proceed with the assembly anyway, even if the matchup extraction step failed for some input files. By default the MMDB assembly
                            step is cancelled in case of any failure.
      --start START         start date of the data interval to include in the produced MMDB
      --end END             end date of the data interval to include in the produced MMDB
      --skip-sibling-extraction
                            skip extraction of sibling datasets (extraction will happen at assembling time only)
      --skip-extraction     skip usage of jobard orchestrator (runs the MDB in sequential mode)
      --mmdb-dir MMDB_DIR   Path of the directory that will be used as root to store the assembled MMDB files. Default is current dir.
      --manifest-dir MANIFEST_DIR
                            Path of the directory that will be used as root to store manifest. Default is current dir.
      --child-product_dir CHILD_PRODUCT_DIR
                            Path of the directory that will be used as root when building the path of the files containing the extractions
      -c SYSTEM_CONFIG, --system-config SYSTEM_CONFIG
                            A configuration file with the settings of processing environment
      --felyx-extraction-bin FELYX_EXTRACTION_BIN
                            Path to `felyx-extraction` command if running in a non conda/docker environment
      --felyx-assemble-bin FELYX_ASSEMBLE_BIN
                            Path to `felyx-assemble` command if running in a non conda/docker environment
      --resume              resume a previously launched processing (in case of interruption)
      --cluster CLUSTER     cluster type (among PBS, SWARM, HTCONDOR, LOCAL)
      --split SPLIT         split the list of input files in several jobarrays, e.g. "/10" to create 10 jobarrays or "10" to create N(files)/10 jobarrays
      --workers WORKERS     number of workers used to process each jobarray
      --cluster-ssh SSH     ssh connection point to the cluster
      -u URL, --url URL     URL of jobard scheduler service (if different from the one provided in system configuration)
      --extraction-memory EXTRACTION_MEMORY
                            maximum memory for felyx-extraction (can be overriden per dataset in MMDB configuration file). Default is `2G`.
      --extraction-walltime EXTRACTION_WALLTIME
                            maximum walltime for felyx-extraction (can be overriden per dataset in MMDB configuration file). Default is `02:00:00`.
      --assemble-memory ASSEMBLE_MEMORY
                            maximum memory for felyx-assemble (can be overriden per dataset in MMDB configuration file). Default is `2G`.
      --assemble-walltime ASSEMBLE_WALLTIME
                            maximum walltime for felyx-assemble (can be overriden per dataset in MMDB configuration file). Default is `02:00:00`.
      --logfile LOGFILE     Path of the file where logs will be written
      --logfile-level LOGFILE_LEVEL
                            Minimal level that log messages must reach to be written in the log file
      -v, --verbose         Activate debug level logging - for extra feedback.
      -q, --quiet           Disable information logging - for reduced feedback.
      -s, --silence         Log ONLY the most critical or fatal errors.