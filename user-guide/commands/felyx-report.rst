.. _felyx-report-command:

============
felyx-report
============

Command-line for report and dashboards generation
-------------------------------------------------

Produce monitoring reports from MMDBs produced by **felyx** processor and
generate dashboards and alerts. For more details refer to
:ref:`felyx-report-details`.

.. code-block:: bash

    usage: felyx-report [-h] -c CONFIGURATION_FILE
                        --start START
                        --stop STOP
                        --manifest_dir MANIFEST_DIR
                        [--metric_dir METRIC_DIR]
                        --mdb_dir MDB_DIR
                        [--logfile LOGFILE]
                        [--logfile_level LOGFILE_LEVEL]
                        [-v | -q | -s]


    positional arguments:
      -c CONFIGURATION_FILE, --configuration_file CONFIGURATION_FILE
                            The user felyx configuration file path
      --start START         Time frame start date (YYYY-mm-ddTHH:MM:SS format)
      --stop STOP           Time frame end date (YYYY-mm-ddTHH:MM:SS format)
      --manifest_dir MANIFEST_DIR
                            Manifest files root dir path
      --mdb_dir MDB_DIR     Matchup files root dir path

    optional arguments:
      -h, --help            show this help message and exit
      --metric_dir METRIC_DIR
                            Metric files root dir path
      --logfile LOGFILE     Path of the file where logs will be written
      --logfile_level LOGFILE_LEVEL
                            Minimal level that log messages must reach to be written in the log file
      -v, --verbose         Activate debug level logging - for extra feedback.
      -q, --quiet           Disable information logging - for reduced feedback.
      -s, --silence         Log ONLY the most critical or fatal errors.


Producing reports data :
^^^^^^^^^^^^^^^^^^^^^^^^

These report data are calculated for each MMDB defined in the felyx user configuration file and for a given time range specified by the command arguments.

The input data for report processing are :

- In situ data which paths are defined in *SiteCollections* section of the user configuration file.
- EO source data which paths are defined in *EODatasets* section of the user configuration file.
- Manifest files produced by the extraction process which path is given by the command argument.
- Metric files produced by the extraction process which path is given by the command argument. This input is optional.
- MMDB files produced by the assembling process which path is given by the command argument.

For each MMDB and for each day of the time range, statistics are calculated and stored into an Elasticsearch index.

Each index cumulates the whole report data of a given MMDB.

MMDB Statistics are organized in the following sections :

- Processed files :

  For each EO source:

  *  Number of processed EO files taken from EO dataset repository
  *  Number of manifests taken from manifest repository

- Metrics (if any) :

  For each EO source:

  *  Number of created metrics for each type of metrics taken from metrics repository

- Matchups :

  *  Number of in situ measurements taken from in situ data files

  For each EO source and MMDB product:

  -  Number of created MMDB files taken from MMDB repository
  -  Number of created matchups taken from manifest repository
  -  Number of assembled matchups taken from MMDB repository


Generating Grafana dashboards :
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

After computing reports data, the report processing creates dashboards presenting the statistics described in the previous section.
They can be viewed with the Grafana UI.

The dashboards are stored in Grafana in a global repository which name is specified in the report system configuration file (*root_folder* entry).

A folder is created for each MMDB in this repository and contains all the dashboards of the MMDB.

The following dashboards are defined for each MMDB :

- Input data processing :

  For each EO source:  Number of processed EO files vs Number of produced manifests

- Metrics :

  For each EO source and type of metrics: Number of created metrics

- Matchups :

  *  Number of in situ measurements

  *  For each EO source and MMDB product: Number of created MMDB files

  *  For each EO source and MMDB product: Number of created matchups from manifest vs Number of assembled matchups


**Nota for Number of created matchups from manifest vs Number of assembled matchups dashboards :**

As it is possible that for some products, the manifest files have not been generated, this type of dashboard can be empty.

To avoid this, the user can configure the generation of these dashboards for each EO dataset and for each product
in MMDB product sections in the felyx user configuration file.
If not specified, the dashboard is generated.

.. code-block:: yaml

     products:
       ...
              monitoring:
                ...
                dashboard_create_nb_of_created_matchups: True

Generating Grafana alerting rules :
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Some alerting rules are created by the report processing to produce alerts if rules are not followed.
These alerts can be viewed with the Grafana UI in the Grafana alerting page.

The following alerting rules are created for each dashboard type :

- Number of processed EO files and Number of produced manifests should be equal
- Number of created metrics and Number of created matchups from manifest for each type of metric
- Number of in situ measurements should be above a given count
- Number of created MMDB files should be equal to expected value
- Number of created matchups from manifest and Number of assembled matchups should be equal

Grafana evaluates periodically these rules on the dashboards data and generates alerts if they are not followed.

The alerting rules can be configured in the felyx processor user configuration file with the following entries :

- In EO dataset sections :
  For each EO dataset, the *alert_check_nb_of_processed_eo_files* specifies if alerts on number of processed EO files should be generated.

  If not specified, the alert rule is not created.

.. code-block:: yaml

  EODatasets:

  ...
    monitoring:
      alert_check_nb_of_processed_eo_files: True

- In site collections sections :
  For each site collection, the *alert_threshold_nb_of_in_situ_measurements* specifies the threshold for generating alerts.

  If not specified, the alert rule is not created.

.. code-block:: yaml

  SiteCollections:

    collection_id:
      ...
      monitoring:
        alert_threshold_nb_of_in_situ_measurements: 19500

- In MMDB metrics sections:
  For each type of metric of each EO dataset, the *alert_check_nb_metrics* specifies if alert on created metrics should be generated.

  If not specified, the alert rule is not created.

.. code-block:: yaml

  metrics:
      ...
      monitoring:
              alert_check_nb_metrics: True

- In MMDB product sections :
  For each EO dataset and for each product:

  - the *alert_threshold_nb_of_created_mmdb_files* specifies the value for generating alerts
  - the *alert_check_nb_of_created_matchups* specifies if alerts on created matchups should be generated.

  If not specified, the alert rules are not created.

.. code-block:: yaml

     products:
       ...
              monitoring:
                alert_threshold_nb_of_created_mmdb_files: 1
                alert_check_nb_of_created_matchups: True
                dashboard_create_nb_of_created_matchups: True


Grafana alert notification :
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

If alerts has been generated, Grafana sends periodically alert notification emails to users.
This notifications can be configured in the user felyx configuration file in the *contact_points* section.

.. code-block:: yaml

  contact_points:
    - name: 'name1'
      addresses:
        - 'your.address@domain.com'
        - 'your.other.address@domain.com'
      disable_resolve_message: False
      repeat_interval:
        duration: 12
        unit: 'h'
  ...

For each contact to notify, a list of email addresses can be specified in the *addresses* list.

The *disable_resolve_message* option allows to disable the resolve message that is sent when the alerting state returns to false.
The *repeat_interval* entry specifies the waiting time to resend an alert after they have successfully been sent.

