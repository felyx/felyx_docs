==========================================
felyx-assemble: building match-up datasets
==========================================


Commandline matchup assembling
==============================

.. program:: felyx-assemble

Assemble child products extracted with ``felyx-extraction`` into
multi-matchup files.

.. code-block:: bash

    usage: felyx-assemble [-h] [-c CONFIGURATION_FILE] [--matchup-dataset MATCHUP_DATASET] [-o OUTPUT_DIR] [--child-product-dir CHILD_PRODUCT_DIR]
                          [--manifest-dir MANIFEST_DIR] [--from-manifests] [--extract-from-source] [--start START] [--end END] [--dry-run] [--logfile LOGFILE]
                          [--logfile-level LOGFILE_LEVEL] [-v | -q | -s] [-p PARAMETERS_FILE] [-P [key=value [key=value ...]]]
                          [-C [key=value [key=value ...]]]

    assemble full match-up dataset files from the EO child products and measurements associated with the dynamic extraction sites

    optional arguments:
      -h, --help            show this help message and exit
      -c CONFIGURATION_FILE, --configuration-file CONFIGURATION_FILE
                            A configuration file with the settings of the match-up dataset to assemble
      --matchup-dataset MATCHUP_DATASET
                            The identifier of the of the match-up dataset to assemble, as defined in the settings configuration file
      -o OUTPUT_DIR, --output-dir OUTPUT_DIR
                            The directory to write the output match-up files
      --child-product-dir CHILD_PRODUCT_DIR
                            The directory where child products are stored (uses the value in felyx system configuration file by default). Only used if assembling the match-ups from pre-extracted child products.
      --manifest-dir MANIFEST_DIR
                            The directory where manifests are stored (uses the value in felyx system configuration file by default). Only used if assembling the match-ups from manifest
      --from-manifests      collect the child products to assemble from the manifest files created by `felyx-extraction`. Avoids querying an index engine such as Elasticsearch.
      --extract-from-source
                            Extract the child products to assemble directly from the EO source data files. Prevents from saving the child products to disk in `felyx-extraction`.
      --start START         start date (YYYY-mm-ddTHH:MM:SS format)
      --end END             end date (YYYY-mm-ddTHH:MM:SS format)
      --dry-run
      --logfile LOGFILE     Path of the file where logs will be written
      --logfile-level LOGFILE_LEVEL
                            Minimal level that log messages must reach to be written in the log file
      -v, --verbose         Activate debug level logging - for extra feedback.
      -q, --quiet           Disable information logging - for reduced feedback.
      -s, --silence         Log ONLY the most critical or fatal errors.



Details
=======
The following sections give further examples and details on how to use this
command and its different options. These examples can be executed from the
root folder of the ``felyx-processor`` directory if the corresponding gitlab
repository was cloned to your local environment.

Basic options
-------------

The simplest call assembles previously extracted child products into output
files, using the assembling settings defined in a configuration file
(see :ref:`processing-configuration` on how to write a configuration file):

.. code-block:: bash

  felyx-assemble --configuration <path to configuration file> \
    --matchup-dataset <matchup dataset identifier>

where `<matchup dataset identifier>` is the identifier of the multi-matchup
dataset to produce, as defined in the configuration file.

If ``--output-dir`` is not set, the output multi-matchup files are created in
the current directory.


