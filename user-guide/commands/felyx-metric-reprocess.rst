felyx-metric-reprocess
======================

Command-line metrics reprocessing
---------------------------------

.. program:: felyx-metric-reprocess

::

   felyx-metric-reprocess [-h] [--no-metadata | --no-metrics]
                              [-g GRANULARITY] [--dataset-name DATASET_NAME]
                              [--metric-name METRIC_NAME]
                              [--user-name USER_NAME]
                              [--site-collection SITE_COLLECTION]
                              [--storage {file,elastic_search}] [--no-clean]


Reproduce all metrics and (optionally) metadata for the miniprods in the felyx
archive, limited by a set of criteria.

optional arguments:

.. option:: -h, --help

   show this help message and exit

.. option::  --no-metadata

   Do NOT produce miniprod file metadata (times,
   locations etc.). Only produce statistical or numerical
   metrics (default: False).

.. option::  --no-metrics

   Do NOT produce statistical or numerical metrics, only
   produce file level metadata (default: False).

.. option::  -g <GRANULARITY>, --granularity <GRANULARITY>
                        
   The maximum number of miniprods to be reprocessed by
   each process. Some systems have a limit on the total
   number of open files, and this flag is intended to
   accommodate them. Default: 1000.

.. option::  --dataset-name <DATASET_NAME>

   The name of the dataset for which metrics are to be
   reprocessed. By default, all datasets will be
   reprocessed.

.. option::  --metric-name <METRIC_NAME>

   The name of the metric to be reprocessed. By default,
   all metrics will be reevaluated.

.. option::  --user-name <USER_NAME>

   The name of the user for whom to reprocess. Defaults
   to the main user as configured for this felyx
   instance.

.. option::  --site-collection <SITE_COLLECTION>

   The name of the site collection to reprocess. By
   default all site collections for the stated user will
   be reprocessed, and if no user is provided than all
   public miniprods will be reprocess.

.. option:: --storage

   {file,elastic_search,file_and_elastic_search}
   Store the metrics and metadata in JSON files in
   output-dir (file), or publish the metrics and metadata
   to ElasticSearch without creating any files
   (elastic_search), or do both. Default: file.

.. option::  --no-clean

   Do NOT remove manifest file. Default value is False)
   which will cause the manifest file to be removed if it
   exists.


