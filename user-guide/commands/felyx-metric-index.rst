felyx-metric-index
==================

Command-line metrics generation
-------------------------------

.. program:: felyx-metric-index

:: 

   felyx-metric-index [-h] [--no-metadata | --no-metrics] [-g GRANULARITY]
                          [--dataset-name DATASET_NAME]
                          [--metric-name METRIC_NAME] [--user-name USER_NAME]
                          [--site-collection SITE_COLLECTION]
                          [--only-sites ONLY_SITES [ONLY_SITES ...]]
                          [--json-files JSON_FILES [JSON_FILES ...] | --stdin]
                          [--mode {local,remote}]

Reindex metrics and metadata for the miniprods in the felyx archive, or from a
list of input files, limited by a set of optional criteria. No command line
options are required, but indexing will take a *long* time of none are
specified. When provided without the optional --json-files or --stdin flags,
then the entire felyx archive for the default felyx user will be (re)indexed.
All arguments are optional.


.. option:: -h, --help 

   show this help message and exit

.. option:: --no-metadata

   Do NOT index miniprod file metadata (times, locations
   etc.). Only index statistical or numerical metrics
   (default: False).

.. option:: --no-metrics

   Do NOT index statistical or numerical metrics, only
   produce file level metadata (default: False).

.. option:: -g GRANULARITY, --granularity GRANULARITY

   The ElasticSearch bulk upload value. Default: 1000.

.. option:: --dataset-name DATASET_NAME
   
   The name of the dataset for which metrics are to be
   indexed. By default, all datasets will be indexed.

.. option:: --metric-name METRIC_NAME

   The name of the metric to be indexed. By default, all
   metrics will be indexed.

.. option:: --user-name USER_NAME

   The name of the user for whom to index. Defaults to
   the main user as configured for this felyx instance.

.. option:: --site-collection SITE_COLLECTION
   
   The name of the site collection to reprocess. By
   default all site collections for the stated user will
   be indexed, and if no user is provided than all public
   metrics will be indexed.

.. option:: --only-sites ONLY_SITES [ONLY_SITES ...]

   A space separated list of site codes to be indexed. Do
   NOT provide a equals sign. For example: use --only-
   sites ghr001 ghr002. By default all sites matching the
   --dataset-name, --user-name and --site-collection
   options will be indexed (these need not be provided as
   they all have default values).

.. option:: --json-files JSON_FILES [JSON_FILES ...]
            
   An optional space separated string list of full paths
   to JSON metric or metadata files. Do NOT provide a
   equals sign. For example: --json-files /tmp/data1.json
   /tmp/data2.json. This option will not ignore --no-
   metrics or --no-metadata, so if you specify --no-
   metrics and then provide with this option a list of
   only metric JSON files, no data will be indexed.
   However, it will ignore the --site-collection, --user-
   names, --only-sites and --dataset-name options. Cannot
   be used with --stdin.

.. option:: --stdin

   Read a list of line separated JSON files for input
   from STDIN. Cannot be used with --json-files.
   (default: False).

.. option:: --mode {local,remote}
   
    Perform the indexing of metrics and / or metadata
    locally or remotely. Note that no parallel execution
    occurs in remote mode. Default: local.


Examples
--------

Let's first index one single JSON file::

  felyx-metric-index --json-file /home/cercache/project/felyx/data/public/ghr048/ifr-l4-sstfnd-odyssea-med_002_v2.1/2010/242/20100830000000_ghr048_ifr-l4-sstfnd-odyssea-med_002_v2.1_20100830-IFR-L4_GHRSST-SSTfnd-ODYSSEA-MED_002-v2.nc___mean_sst.json

You can index several files alltogether with the same option but you may be limited at some point by the maximum length of a command::

  felyx-metric-index --json-file /home/cercache/project/felyx/data/public/ghr048/ifr-l4-sstfnd-odyssea-med_002_v2.1/2010/242/20100830000000_ghr048_ifr-l4-sstfnd-odyssea-med_002_v2.1_20100830-IFR-L4_GHRSST-SSTfnd-ODYSSEA-MED_002-v2.nc___mean_sst.json

In any case, you can feed the ingestion with a list of any number of files from stdin::

  find /home/cercache/project/felyx/data/public/ghr048/ifr-l4-sstfnd-odyssea-med_002_v2.1/2010/242/ -name "*.json" | felyx-metric-index --stdin


Alternatively, the following method can be used, providing the list of input
JSON files directly in the command line with `--json-files` option but the number
of input files will be limited by the maximum length of an unix command on your
system :
 

