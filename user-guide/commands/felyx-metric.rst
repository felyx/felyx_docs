Extracting metrics
==================

Command-line metrics generation
-------------------------------

.. program:: felyx-metric

:: 

   felyx-metric [-h] [--no-metadata | --no-metrics]
                    [--metric-name METRIC_NAME] [-o OUTPUT_DIR]
                    [--mode {local,remote}] [--storage {file,elastic_search}]
                    [--no-clean]
                    [infile]


Create metrics from a group of miniprods. By default miniprod metadata and
calculated metrics will be produced in JSON format files and will be stored in
the same directory as the source miniprod. Note that this is the
user/site_collection directory and not the public miniprod directory.


.. option:: infile

   The full path to a list of miniprods, for example the
   manifest file produced by ``felyx-extraction``. If no path
   is provided then a list of miniprod files names will
   be read from STDIN (which can also be provided as the
   STDOUT of ``felyx-extraction`` when running in local mode).

.. option:: -h, --help

   show this help message and exit

.. option:: --no-metadata

   Do NOT produce miniprod file metadata (times,
   locations etc.). Only produce statistical or numerical
   metrics (default: False).

.. option:: --no-metrics

   Do NOT produce statistical or numerical metrics, only
   produce file level metadata (default: False).

.. option:: --metric-name <METRIC_NAME>

   The name of the metric to be produced. This is
   intended to aid reprocessing efforts. Omit to produce
   all applicable metrics. Ignored if --no-metrics is
   set, forces --no-metadata to be set. It is not
   intended for users to apply this flag, and it is
   documented only for completeness. DO NOT SET THIS
   FLAG.

.. option:: -o <OUTPUT_DIR>, --output-dir <OUTPUT_DIR>

   The directory to write output JSON files to. The
   default action is to store the JSON files with the
   miniprods. Ignored if execution of felyx-metric is in
   remote mode (where output is always stored with the
   miniprods) or if mode is set to elastic_search.

.. option:: --mode {local,remote}

   Perform evaluation of metrics and / or metadata
   locally or in remotely across distributed workers.
   Default: remote.

.. option:: --storage {file,elastic_search,file_and_elastic_search}

   Store the metrics and metadata in JSON files in
   output-dir (file), or publish the metrics and metadata
   to ElasticSearch without creating any files
   (elastic_search), or do both. Default: file.

.. option:: --no-clean

   Do NOT remove manifest file. Default value is False)
   which will cause the manifest file to be removed if it
   exists. Intended to aid testing only.


