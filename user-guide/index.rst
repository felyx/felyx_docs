==========
User Guide
==========

In this user guide, you will find detailed descriptions and examples that
describe many common tasks that you can accomplish with **felyx**.

.. toctree::
   :maxdepth: 2
   :hidden:

   concepts
   Configuring your system <configuration/system-configuration>
   Preparing the dynamic sites <dynamic_sites>
   Configuring your processing <configuration/processing-configuration>
   felyx-extraction: Extracting matchups <felyx_extraction>
   felyx-assemble: Building multi-matchup datasets <mmdatasets>
   felyx-mmdb: Complete matchup dataset production <felyx_mmdb>
   felyx-metric: Computing metrics over extracted matchups <metrics>
   Product output format <output_format>
   Traceability of the extracted data <traceability>
   Local vs distributed processing <processing>
   Automating with Airflow <airflow.rst>
   Monitoring and alerting <monitoring_and_alerting>

