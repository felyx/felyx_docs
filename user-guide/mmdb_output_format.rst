===================
MMDB product format
===================
The output content and format for the assembled MMDB files is very flexible,
as described in the :ref:`processing configuration section <mmdatasets>`_. The
data files are in netCDF4 format and follow the Climate and Forecast
Convention recommendations.

All matchups within a MMDB file are stacked along a common ``obs`` dimension.

By default all variables and attributes from the assembled match-ups (child
products) are copied to the output MMDB file, including the dynamic site data
(in situ measurements in most cases), unless specified otherwise in the
configuration file. They can be prefixed with a specific label defined for
each dataset combined into the MMDB.

Additional variables found in all MMDB files include the site properties at
the match-up time, as follow:

.. code-block:: bash

    string __site_id(obs) ;
        __site_id:long_name = "identifier of the site matched with the observation" ;
        __site_id:coordinates = "lon lat time" ;
    string __site_name(obs) ;
        __site_name:long_name = "full name of the site matched with the observation" ;
        __site_name:coordinates = "lon lat time" ;
    double time(obs) ;
        time:_FillValue = 0. ;
        time:standard_name = "time" ;
        time:long_name = "time of the nearest site data" ;
        time:units = "seconds since 1980-01-01" ;
        time:calendar = "proleptic_gregorian" ;
    double lon(obs) ;
        lon:_FillValue = 1.e+20 ;
        lon:standard_name = "longitude" ;
        lon:long_name = "longitude of the nearest site data" ;
    double lat(obs) ;
        lat:_FillValue = 1.e+20 ;
        lat:standard_name = "latitude" ;
        lat:long_name = "latitude of the nearest site data" ;


Additional attributes, prefixed with ``__`` string, include the identifier
'``__id``) of the MMDB (sub)product, as defined in the configuration file, the
start and end time (``__time_coverage_start`` and ``__time_coverage_end``) of
the period over which the match-ups were collected and stacked. For example:

.. code-block:: bash

		:__id = "S3A_SL__OPE_NRT__cmems_drifter" ;
		:__time_coverage_end = "2021-09-16T00:00:00" ;
		:__time_coverage_start = "2021-09-15T00:00:00" ;
