***********************
Hardware considerations
***********************

Number of machines
==================

The Felyx system can be installed on a single computer for test purposes, but a
minimal production installation should count at least 6 machines:

* 1 for RabbitMQ
* 1 for Celery worker (miniprod & metrics processing) and ElasticSearch
* 1 for Celery worker (web requests) and ElasticSearch
* 1 for Celery worker (logs requests) and ElasticSearch
* 1 for the downloader
* 1 for the frontend

Note that this setup can only be used to process a limited volume of data and
should be scaled up (with more Celery "processing" workers) if the workload
grows too much.

RabbitMQ machine
================

The RabbitMQ server will relay job orders from client applications to Celery
workers.

If the server is too busy or does not have enough memory, these orders will be
delayed and might be ignored (there is a timeout).

Since its role is critical, the RabbitMQ server must have its own machine, with
enough memory to store the potential thousands of messages it will receive and
good network capabilities to handle the communication with workers. This
depends on the number of files you intend to process at the same time or in a
reprocessing and the number of sites where you extract miniprods, and the number
of metrics you intend to process for each miniprod. There is one entry in the
RabbitMQ queue per miniprod extracted or metrics processed.

ElasticSearch node
==================

ElasticSearch nodes require a lot of RAM and some disk space to store the
indexed data (miniproducts information, metrics and logs). A good CPU will speed
up indexation and search, but is not essential.

.. note::
  Celery workers and ElasticSearch nodes can share the same hardware, but it may
  have a negative impact on performance under heavy load.

Elasticsearch web site provides some good resources on the hardware requirements,
depending if you are running it on a cluster, on virtual machines, etc...

Also, using at least two different machines will improve the speed and reliability,
as Elasticsearch provides both load-balancing and replication capabilities.

Celery workers
==============

Celery workers will need both a good CPU and a good amount of RAM to perform 
the operations requested by Felyx users in optimal delays.

.. note::
  Celery workers and ElasticSearch nodes can share the same hardware, but it may
  have a negative impact on performance under heavy load.

.. note::
  Different workers (e.g for processing, logs requests, web requests) should
  run on different cores or servers to avoid concurrent access to CPU
  resources. The highest number of workers should be affected to the most 
  frequently accessed process, usually the miniprod/metrics processing 
  (especially when reprocessing data) or the web requests. During web requests,
  distributed processing is performed whenever possible : a large number of 
  workers will therefore result in faster processing of a single request (in 
  addition to the fact that multiple requests can be processed at the same
  time).

Downloader server
=================

The downloader should be installed on its own machine and have read/write
permission on the shared disk space.

.. note::
  The Downloader is not mandatory in your installation. It is provided to help
  the routine ingestion of EO data into felyx. If you only process data offline,
  or using your own orchestrator, you don't need it.

Shared disk
===========

The shared disk space must be readable and writable by Celery workers. It will store
the miniprods and, optionally, the metrics. But the big part are the miniprods and
it grows fast if you have a lot of extraction sites, process a lot of products or
products with a high resolution. This is difficult to give a figure here as it will
depend on your own usage. Try to assess this respect to the total covered area of your 
products versus the total spatial coverage of your extraction sites.

.. note::
  The shared disk should also be readable by the machines that will host the
  frontend.

.. warning::
  felyx creates a huge number of files (miniprods and metrics). This must be taken
  into account when configuring your file system for felyx usage as the number of
  inodes may be an issue on some servers.

Operating system
================

Felyx back-end is fully implemented in Python 2.7 and should therefore be able to run
on any operating system. It has been succesfully tested on Linux (Ubuntu, CentOS)
and MacOS.


