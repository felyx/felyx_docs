.. _Parquet: https://parquet.apache.org/
.. _Elasticsearch: https://www.elastic.co/elastic-stack/
.. _pandas: https://pandas.pydata.org/
.. _felyx_processor: https://gitlab.ifremer.fr/felyx/felyx_processor

============================================
Dynamic extraction sites and associated data
============================================

This section addresses the extraction of EO data over moving targets. If you
only extract data over fix locations (static sites), you can skip this section.

Introduction
============
Child products can be extracted over **dynamic sites**, a suite of locations in
space and time, defining trajectories, attached to objects which can be
artificial (a measuring device like a buoy, a ship,...) or physical (a
hurricane). Properties or measurement values can be associated with each of
the trajectory points. A typical case is *in situ* observations attached to
drifting buoys or cruise ships.

Subsetting EO data over the nearest locations of these trajectories with respect
to a colocation radius and a time window, and joining these subsets with the
corresponding properties or observation of the trajectories, is how **felyx**
helps to build **match-up datasets**.

In order to process efficiently these dynamic sites data in the extraction
process, they must be provided to felyx in `Parquet`_ format or registered
into a fast indexing and search engine such `Elasticsearch`_.

`felyx_processor`_ package provides helper functions to build parquet files
from arrays of data and save them in the correct format specifications for
**felyx**.


Parquet file format
===================
`Parquet`_ is a columnar format; the dynamic sites can be defined as a list
of times and locations and associated properties or measurements of any quantity
attached to an object identified by a unique string (the identification code
of a buoy or ship, the name or code of a hurricane,...). **felyx** requires
as minimal set of columns the object identifier, time, latitude, and
longitude, to be named respectively `id`, `time`, `lat`, `lon`. Properties or
measurements can be added in extra columns, with a free but unique column
name. `time` must be of ``datetime64`` data type.

The Parquet files must be indexed with `('id', 'time')` multi-index.

`obs` is a reserved name and must never be used as a index or column name in
parquet files.

`Parquet`_ files can be easily created in different languages, for instance in
python with `pandas`_ using ``DataFrame`` objects:

.. code-block:: python

    >>> # assume we have a DataFrame with the required list of identifier,
    >>> # locations and times. They are indexed with the site id and times.

    >>> df
                                         lat         lon  water_temperature_0  solar_zenith_angle
    id        time
    cm5102725 2021-09-15 12:00:00  25.212200  142.556000            29.860001          134.751404
              2021-09-15 13:00:00  25.209000  142.555405            29.810001          144.816086
              2021-09-15 14:00:00  25.205999  142.551193            29.790001           150.78125
              2021-09-15 15:00:00  25.205799  142.548798            29.790001          150.215744
              2021-09-15 16:00:00  25.208200  142.547607            29.790001          143.299011
    ...                                  ...         ...                  ...                 ...
    cm3201822 2021-09-15 13:00:00  12.486600 -139.326797            28.080002          121.303093
              2021-09-15 14:00:00  12.490600 -139.338394            28.070002          107.115273
              2021-09-15 15:00:00  12.493800 -139.350601            28.060001           92.313026
              2021-09-15 16:00:00  12.498800 -139.360199            28.050001           77.691429
              2021-09-15 17:00:00  12.507000 -139.368805            28.050001           63.303493

    [6830 rows x 4 columns]

    >>> # save to parquet
    >>> df.to_parquet('my_dynamic_sites.parquet')



Last, a `z` index level is authorized for profile data having a depth or height
dimension. The depth value itself should be placed into another variable
(``meas_depth`` in the example below).

.. code-block:: python

                                   meas_depth        lat  climatology_water_temperature  ...        lon  solar_zenith_angle  closest_to_surface
    id      time                z                                                        ...
    6202652 2021-09-15 00:00:00 0         0.0 -33.358601                      14.380005  ...  92.511803           88.707100                   0
                                1         NaN -33.358601                      14.380005  ...  92.511803           88.707100                   0
                                2         NaN -33.358601                      14.380005  ...  92.511803           88.707100                   0
                                3         NaN -33.358601                      14.380005  ...  92.511803           88.707100                   0
                                4         NaN -33.358601                      14.380005  ...  92.511803           88.707100                   0
    ...                                   ...        ...                            ...  ...        ...                 ...                 ...
    6204570 2021-09-15 05:59:00 0         0.0  -9.198100                      24.630005  ... -20.984800          110.261177                   0
                                1         NaN  -9.198100                      24.630005  ... -20.984800          110.261177                   0
                                2         NaN  -9.198100                      24.630005  ... -20.984800          110.261177                   0
                                3         NaN  -9.198100                      24.630005  ... -20.984800          110.261177                   0
                                4         NaN  -9.198100                      24.630005  ... -20.984800          110.261177                   0


file metadata
-------------
`Parquet`_ allows to add some metadata to the created Parquet file. This is
particularly useful for instance to provide some formatting information for
the dynamic site data. When creating a match-up file, **felyx** will use
these metadata to properly fill in each variable's attributes (``units``,
``dtype``, ``long_name``, ``_FillValue``, etc...) of the created netcdf file,
creating a richer and self-described output. It is strongly recommended to
set at least the ``dtype`` attribute (as in numpy) for each column in the
Parquet data frame, so that the data are saved to NetCDF with the proper type
(as Parquet data types are more limited than the numpy ones).

If these metadata are not provided, the variables corresponding to each Parquet
column will be created without any attribute.

The metadata can be set in Parquet using the built-in field metadata
and *schema* structure that defines both column attributes and global
metadata, as shown below in python using pandas DataFrame objects:


.. code-block:: python

    # suppose we have a pandas dataframe with all our dynamic site properties
    # and measurements
    df = (...)

    # set first the variable attributes for each column in the dataframe
    fields = []
    metadata = df.field('column1').metadata or {}
    metadata['long_name'] = json.dumps('description of column1')
    metadata['units'] = json.dumps('kelvin')
    metadata['_FillValue'] = json.dumps('1e20')
    metadata['dtype'] = json.dumps('float64')
    # Update field with updated metadata
    fields.append(df.field('column1').with_metadata(metadata))

    # repeat for each colmun
    ...

    # set global attributes
    attrs = df.schema.metadata or {}
    attrs['title'] = 'example in situ datase'
    attrs['source'] = 'the provider of these data'

    # Create new schema with updated field metadata and updated table
    # metadata
    schema = pa.schema(fields, metadata=attrs)

    # With updated schema build new table (shouldn't copy data)
    df = df.cast(schema)



file granularity
----------------
In order to limit the amount of dynamic site data to be read when processing
a particular EO file, the data shall be broken down into different
periodic files (6-hourly, daily, monthly, etc...). It also makes easier the
update process of these dynamic site data in the context of a regular
processing with new incoming in situ measurements for instance.

The size and periodicity of the Parquet file depends on the application.
There shall not be too large in order to avoid loading into memory too many
irrelevant data when processing a single EO file. There shall not be too
small neither in order to avoid opening and loading multiple files.
As a rule of thumb, if dealing with a large amount of dynamic sites (several
hundreds a day), the temporal coverage of each parquet file shall be larger
than the duration of the EO data granules to process, including the
colocation time window.

For instance if dealing with EO orbit files from polar orbiting satellite
(approximately 100 minutes of acquisition), with a time window of two hours,
the minimum periodicity of the Parquet files shall be more than 6 hours, to
avoid opening more than two files, and not greater than a day to avoid
loading too many data out of the colocation window for a given orbit file.

using felyx API
---------------
Helper functions are provided in `felyx_processor`_ package to create in
python language the parquet files with their metadata at the desired
periodicity.

The normal sequence of tasks to create such data files will usually be as:

* read the source data
* filter out irrelevant data that should not be used by **felyx**
* transform the data to a two-dimensional `pandas`_ DataFrame object with
  proper column naming and type for the site's identifier, latitude, longitude
  and time
* set the metadata dictionary
* instantiate a ``DynamicSiteData`` object with the DataFrame and metadata
  dictionary
* save the ``DynamicSiteData`` object to parquet, at the desired frequency

Here is an example:

.. code-block:: python

    # import helper functions
    from felyx_processor.sites.dynamic_site_data import DynamicSiteData

    # suppose we have a pandas dataframe with all our dynamic site properties
    # and measurements
    df = (...)

    # set the metadata
    metadata = {'felyx_globals': {
        'vars': {
            'column1': {'long_name': 'description of column1',
                        'units': 'kelvin',
                        '_FillValue': 1e20,
                        'dtype': 'float64'},
            ...
        },
        'globals': {
            'title': 'example in situ dataset',
            'source': 'the provider of these data',
            ...
        }}}

    # instantiate DynamicSiteData object
    insitudata = DynamicSiteData(collection_id='my_collection')
    insitudata.add_data(df)
    insitudata.add_metadata(metadata['vars'], metadata['globals'])

    # save as 6 hourly files
    # note the date pattern used in the filename, using python convention for
    # datetime
    insitudata.save(
        '%Y%m%d%H%M%S_my_collection.parquet',
        frequency='6H',
        format='parquet')


Elasticsearch
=============

Dynamic site data saved in parquet files can be loaded into Elasticsearch.
This allows **felyx** to select site data from Elasticsearch rather than
parquet files, while allowing also other usages of these data through queries
to Elasticsearch (such as data display, dashboards, etc...).

Dynamic site data can be loaded into Elasticsearch with
``felyx-import-parquet-insitu`` command.
