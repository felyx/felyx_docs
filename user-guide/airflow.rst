.. _Parquet: https://parquet.apache.org/
.. _Elasticsearch: https://www.elastic.co/elasticsearch/
.. _Grafana: https://grafana.com/
.. _Airflow connection documentation: https://airflow-fork-tedmiston.readthedocs.io/en/latest/howto/manage-connections.html#creating-a-connection-with-the-ui
.. _Airflow SSH provider documentation: https://airflow.apache.org/docs/apache-airflow-providers-ssh/stable/index.html
.. _Jobard: https://jobard.gitlab-pages.ifremer.fr/documentation/
.. _felyx_report: https://gitlab.ifremer.fr/felyx/felyx_report

========================================
Automating felyx processing with Airflow
========================================

Purpose
=======

The **felyx** processing can be automated with Airflow.
This can be done by creating an **Airflow** DAG using two types of operators:

 * The **SSH operator** for running commands on a remote host.
 * The **Jobard operator** for executing **felyx** commands by job arrays on a remote cluster.

For more information on **Jobard**, refer to the `Jobard`_ documentation.

Prerequisites
=============

Two types of **Airflow** operators are required:

  * The **SSH operator** for running commands on remote host. This requires the installation of the ssh provider plugin on the **Airflow** instance which is not installed by default. To do this, refer to the `Airflow SSH provider documentation`_.
  * The **Jobard operator** for running job arrays for **felyx** extraction step. This requires the installation of the **Jobard client** python package on **Airflow** python environment.

Jobard operator installation
============================

Copy the **Jobard operator** plugin in your **Airflow** plugins directory:

.. code-block:: bash

  cp jobard.py path/to/airflow/plugins/operators

Airflow configuration
=====================

With the **Airflow** UI, create the SSH connection on the remote host which will be used by the **SSH operator**.
The name of the connection must be the same as the ``ssh_conn_id`` DAG parameter.
To do this, refer to `Airflow connection documentation`_.

Example felyx workflow
======================

Prerequisites
-------------

This example requires an `Elasticsearch`_ and a `Grafana`_ instance for **felyx** monitoring task.

Installation
------------

Copy the **felyx** example DAG in your **Airflow** dags directory:

.. code-block:: bash

  cp dag_felyx.py path/to/airflow/dags

You can see it in the **Airflow** UI DAG list :

  .. image:: /_static/dag_list.png

The Felyx DAG
-------------

This DAG executes a **felyx** workflow example on a piece of data. It is
composed by the following steps :

  * Converting the *in situ* **NetCDF** file collections into `Parquet`_ files.
  * Collect the *EO* files of the source datasets.
  * Extracting with **felyx** command the matchups between *in situ* data and
    *EO* data.
  * Assembling the *MMDB* products with **felyx** processor from the matchups.
  * Reporting into `Elasticsearch`_ and `Grafana`_ the results of the
    extraction and assemble processing.

You can see this workflow in the *Airflow* DAG graph view :

  .. image:: /_static/dag_felyx_graph.png

Convert in situ data
####################

A group of conversion tasks is created from the DAG parameter named `insitu_collections`.
This parameter is a dictionary providing by collection the name of the converter to use and the path of the *in situ* **NetCDF** files to convert `Parquet`_ files.
For each collection to convert, a task is created and executes a **felyx** conversion command.
The **felyx** conversion command is set by the `insitu_converters` DAG parameter.
This parameter is a dictionary providing by converter the path to the conversion command binary and some extra converter parameters if required.

The converted files are produced in a directory set in the `SiteCollections` section of the **felyx** configuration file.
In this example, we have 3 site collections to convert:

  * cmems_drifter
  * cmems_argo
  * cmems_moored


Collect EO files to process
###########################

Before extracting matchups from EO data, we need to collect EO source files to process.
This is done by two chained tasks:

  * The first task creates the EO files lists directory from a root directory
    defined by the `eo_lists_dir` DAG parameter and the dag run id and is stored
    in the `felyx_eo_files_path` Airflow variable.
  * The second task executes a **felyx** command which creates EO files lists
    from EO datasets read in **felyx** configuration file and writes them into
    EO files lists directory defined by the previous task.

This step is necessary to determine input EO file lists to provide to **felyx** extraction.

Extract matchups
################

A group of extraction tasks is created from the `EODatasets` section defined in the **felyx** configuration file.
For each EO dataset, a **felyx** extraction task is created.
Each task is a **Jobard operator** which submits a job order executing **felyx** extraction commands on a remote cluster.
The input EO files to process are provided by the EO file lists.
The output manifest directory is defined by the `manifest_dir` DAG parameter.
Several DAG parameters also allow to configure the Jobard command.

In this example, we have 4 EO datasets to process:

 * S3A_SL_1_RBT_A__OPE_NRT
 * S3A_SL_1_RBT_I__OPE_NRT
 * S3A_SL_2_WCT__OPE_NRT
 * S3A_SL_2_WST__OPE_NRT

Assemble matchups
#################

A group of assemble tasks is created from the `mmdb_ids` DAG parameter.
For each MMDB to generate, a **felyx** assemble task is created.
Each task is a **Jobard operator** which submits a job order executing **felyx** an assemble command on a remote cluster.
The input and the command option is defined by the DAG parameters.
The output MMDBs directory is defined by the `mmdb_dir` DAG parameter.
Some DAG parameters also allow to configure the Jobard command.

In this example, we have 4 MMDBs to generate:

 * S3A_SL__OPE_NRT__cmems_drifter
 * S3A_SL__OPE_NRT__cmems_argo
 * S3A_SL__OPE_NRT__cmems_moored
 * S3A_SL__OPE_NRT__trusted

Report
######

The last task is a reporting task of **felyx** processing steps.
This task executes a `felyx_report`_ command to collect metrics on the extraction and assemble processings and generate `Grafana`_ dashboards.
Some DAG parameters allow to configure this command.

DAG parameters
##############

The DAG inputs and configuration are set in DAG parameters.
You can find a description of each of them in the **DAG Docs** section above the **Airflow** graph view:

  .. image:: /_static/dag_params.png
