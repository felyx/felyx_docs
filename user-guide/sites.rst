.. _sites_and_collections:

==========================
Sites and site collections
==========================

**Sites** are geographical areas where data subsets (or **child products**) will
be extracted from the full-size Earth Observation input files. **Sites** can be
either **static** or **dynamic** (location moving with time). They can be
grouped into **site collections**.

When using **felyx** to build match-up databases, the sites correspond to the in
situ observation platforms. For each of these sites, location(s) must be
provided, as well as observation times if these platforms are moving (ships,
buoys, etc...).

Sites
=====

Static sites
++++++++++++

**Static sites** correspond to a fix geographical polygon (figure 1). The file
subsets intersecting this polygon are extracted to produce child products.

.. figure:: /_static/extractions_dmi004.png
   :width: 400 px

   Example of child product extraction (in blue) from several satellite
   swaths at different acquisition times over a static site (limits in red).

Dynamic sites
+++++++++++++

**Dynamic sites** correspond to a moving target along a spatial and temporal
trajectory, defined as a list of positions and times (figure 2). The file
subsets centered on the trajectory locations within a given distance radius and
time window around these location's times are extracted to produce the child
products.

Dynamic sites allow a lagrangian extraction of child products following a moving
feature which can be a measuring device (drifting buoy, ship, ...) or a natural
feature (hurricane, eddy, ...).

.. figure:: /_static/colocation_isar.png
   :width: 400 px

   Example of child product extraction along the cruise track of a ferry line.
   Child products are extracted at the closest track location to the data time.


Collections
===========

Grouping
++++++++

**Site collections** are groups of existing sites. Collections are used to group 
sites that share a common thematic or source : collections could encompass a set
of platforms of the same type or observation program or data provider (*Argo
floats*, *Drifting buoys from Copernicus Marine Service*,...) or a category of
events (*Tropical hurricanes*, *Eddies*,...).

.. warning::
   It is not possible to mix static and dynamic sites in the same collection
   but sites can be shared by different collections (to define for instance
   subsets of larger site collections). 

Here is an example of static site collection (GHRSST HR-DDS sites):

.. image:: /_static/static_collection.png
   :width: 400 px


And here an example of dynamic collection (drifting buoys):

.. image:: /_static/dynamic_collection.png
   :width: 400 px


.. note::
   The back-end system ensures that a given site is not processed twice in case
   it belongs to several collections.


How to define sites and collections?
====================================

How to configure collection of sites in **felyx** is described in details in
the following sections:
