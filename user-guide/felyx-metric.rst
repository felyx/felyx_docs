==================
Extracting metrics
==================


The child products are extracted from each source EO data file with the
``felyx-extraction`` command and saved in netCDF (1) or referenced into a
manifest file per input EO data file (2), which can be used as an input for
the metrics production .

The metrics can be produced with the ``felyx-metric`` command, taking either
as input a child product file (1), or a list of child product referenced into
a file such as the manifest file produced at the previous step (2), or even a
set of child product selection criteria (the command will automatically search
the relevant child product files in the felyx archive and process them).

The metrics can be directly indexed in the search engine (1) or saved instead in
files, in json format, one for each metric (2). Alternatively, both can be done (3)
in which case there is no need to index later the produced json files.

.. NOTE::
   We recommend to save the metrics in json format for testing purpose
   but also later in production : they can then be used to cope with indexing
   error, missing metrics in the search engine or to reconstruct completely the
   search index in case of a severe crash without having to reprocess the data.
   **Be aware this will create thousands up to millions of small files : your
   local file system must be able to cope with a very large number of inodes**.


``felyx-metric`` command
========================
To extract metrics from a child products, ``felyx-metric`` command is used.

This section leads you through the different processing arguments for this
command:

.. command-output:: felyx-metric --help

Metrics operators
=================

A metric operator returns a single value computed from the content of a child
product. This value can be indifferently a number or a string. This means
that this value can represent a quantitative value or a qualitative value
(a property).

**felyx** comes with a selection of predefined operators. The list of available
operators can be extended through the built-in **plugin extension** mechanism
(refer to the developer guide).


Statistical operators
---------------------

+-------------------------------------+------------------------------------------------------+
| Name                                | Description                                          |
+=====================================+======================================================+
| standard_deviation                  | standard deviation of a field                        |
+-------------------------------------+------------------------------------------------------+
| median_percentage_difference        | median percentage difference of two fields           |   
+-------------------------------------+------------------------------------------------------+
| standard_error                      | standard error of a field                            |
+-------------------------------------+------------------------------------------------------+
| min                                 | minimum value of a field                             |
+-------------------------------------+------------------------------------------------------+
| max                                 | maximum value of a field                             |
+-------------------------------------+------------------------------------------------------+
| mean_percentage_difference          |                                                      |
+-------------------------------------+------------------------------------------------------+
| root_mean_square_error              | root mean square error of a field                    |
+-------------------------------------+------------------------------------------------------+
| median                              | median value of a field                              |
+-------------------------------------+------------------------------------------------------+
| coefficient_of_determination        |                                                      |
+-------------------------------------+------------------------------------------------------+
| coefficient_of_correlation          | correlation coefficient between two fields           |
+-------------------------------------+------------------------------------------------------+
| percentile                          |                                                      |
+-------------------------------------+------------------------------------------------------+
| mean_absolute_percentage_difference |                                                      |
+-------------------------------------+------------------------------------------------------+
| kurtosis                            |                                                      |
+-------------------------------------+------------------------------------------------------+
| skew                                |                                                      |
+-------------------------------------+------------------------------------------------------+
| mean                                |                                                      |
+-------------------------------------+------------------------------------------------------+

Content extraction operators
----------------------------

+-------------------------------------+------------------------------------------------------+
| Name                                | Description                                          |
+=====================================+======================================================+
| centre_value                        | return the value at the center of the miniprod (for  |
|                                     | dynamic sites, it is the location of intersected     |
|                                     | trajectory point)                                    |
+-------------------------------------+------------------------------------------------------+
| number_of_values                    | number of valid values                               |
+-------------------------------------+------------------------------------------------------+
| sum_of_values                       |                                                      |
+-------------------------------------+------------------------------------------------------+
| sum_of_square_values                |                                                      |
+-------------------------------------+------------------------------------------------------+



Common operator arguments
=========================

Common arguments offered by operator plugins to condition the processing of the
metrics include:

limit_to_site
  Metrics processing can be limited to the site mask content by setting this
  value to True (by default).

must_have
  Conditions can be applied to data before calculation (field vs threshold)


What happen to the metrics?
===========================

The metrics processed by the operators are then indexed into a **search
engine**. This is the service from which then can be queried from, using
the felyx **web services** and **APIs (application programming interface)**.

They can also be visualized through the default **report generation interface**
shipped with **felyx**.

These services and APIs are described in a later section of this document.


Configuring the metrics
=======================
The operators to be applied to the extracted child products are defined in the
same configuration file defining the extraction, in the ``metric`` subsection.


.. code-block:: yaml

    EODatasets:
      ESACCI-SEASTATE-L2P-SWH-Sentinel-3_A_PLRM:
        feature_class: Trajectory
        filenaming: tests/data/eo/cciseastate-s3a-sral/ESACCI-SEASTATE-L2P-SWH-Sentinel-3_A-%Y%m%d*.nc
        description: CCI Sea State Altimetry Along-Track data from Sentinel-3A PLRM
        level: L2P
        name: ESACCI-SEASTATE-L2P-SWH-Sentinel-3_A_PLRM v2.0.6
        supplier: ESA CCI Sea State

    SiteCollections:
      cmems_wave:
        description: CMEMS In Situ TAC selection of wave buoy data
        level: custom
        name: CMEMS In Situ TAC Wave
        static: false
        datasource_access: parquet
        parquet_config:
          filepath_pattern: tests/data/insitu/parquet/cmems_wave/daily/%Y%m%d_cmems_wave.parquet
          frequency: 1D

    MMDatasets:
      ESACCI-SEASTATE-L2P-SWH-Sentinel-3_A_PLRM__cmems_wave:
        site_collection_id: cmems_wave
        eo_datasets:
          ESACCI-SEASTATE-L2P-SWH-Sentinel-3_A_PLRM:
            subset_size: 21
            matching_criteria:
              time_window: 360
              search_radius: 100.
            metrics:
              swh_max:
                dataset: ESACCI-SEASTATE-L2P-SWH-Sentinel-3_A_PLRM
                label: max_swh
                processing_chain_description: '[{"max": {"limit_to_site": false, "field": "swh"}}]'
              mean_air_temperature:
                dataset: ESACCI-SEASTATE-L2P-SWH'Sentinel-3_A_PLRM
                label: mean_air_temperature
                processing_chain_description: '[{"mean": {"limit_to_site": false, "field": "surface_air_temperature"}}]'


In above example, two metrics are computed for each child product extracted
from `ESACCI-SEASTATE-L2P-SWH-Sentinel-3_A_PLRM` dataset files:

- `swh_max` defined as the maximum value of `swh` variable in
  the extracted child product
- `mean_air_temperature` defined as the mean value of `surface_air_temperature`
  variable in the extracted child product


.. command-output:: felyx-metric --mdataset ESACCI-SEASTATE-L2P-SWH-Sentinel-3_A_PLRM__cmems_wave -c tests/resources/test_cciseastate/felyx_data_metrics.yaml --inputs tests/results/child-products/cmems_wave/cw6200092/ESACCI-SEASTATE-L2P-SWH-Sentinel-3_A_PLRM/2018/190/20180709220040_cw6200092_ESACCI-SEASTATE-L2P-SWH-Sentinel-3_A_PLRM.nc  --metric-dir tests/results/metrics/
