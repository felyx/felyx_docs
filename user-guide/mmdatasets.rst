.. _felyx-assemble:

=================================
Assembling Multi-Matchup Datasets
=================================

Assembling **Multi-Match-Ups Datasets** consists in joining together in some
files the extracted child products (**match-ups**) matching the same extraction
site's time and location, together with the data associated with these sites at
the same time and location. This type of product can also be refered to as
**Match-Up Database (MDB)**, **Colocation Database**,... it forms the basis of
many works in the field of Earth Observation data analysis, calibration and
validation.

This processing step follows the extraction of the individual match-ups
over each dataset with ``felyx-extraction`` command. It is performed with
``felyx-assemble`` command.

This section leads you through the different processing arguments for
``felyx-assemble`` command.

.. command-output:: felyx-assemble --help


The simplest call assembles child products extracted from local earth
observation (EO) data with  ``felyx-extraction`` command, using the settings
defined in a configuration file (see :ref:`processing-configuration` section).


.. command-output::  felyx-assemble --from-manifests --manifest-dir ./tests/results/ --matchup-dataset ESACCI-SEASTATE-L2P-SWH-Sentinel-3_A_PLRM__cmems_wave -c tests/resources/test_cciseastate/felyx_data.yaml --start 2018-07-09 --end 2018-07-10 --extract-from-source


In above example, we build the multi-matchup dataset described in the
``ESACCI-SEASTATE-L2P-SWH-Sentinel-3_A_PLRM__cmems_wave`` section of the
configuration file ``felyx_data.yaml`` from the child products extracted over
the period from 9 July 2018 to 10 July 2018 (00:00 UTC). The child products
to assemble are collected from the index found in the manifest files
(``--from-manifests``) created by ``felyx-extraction``. The matchups assembled
into the output files are extracted from the source EO files
(``--extract-from-source``), since we did not pre-extract the child products,
referencing them only in the manifests).

It is assumed the manifest file root directory is in the local folder where
the command is run, otherwise ``--manifest-dir`` must specify this path.

By default the output multi-matchup dataset files are created in the current
directory. Use ``--output-dir`` to set another location.

If ``--from-manifests`` is not set, the list of child products to collect
and assemble is taken from *Elasticsearch* search engine, which must be
defined in the system configuration file (refer to
:ref:`system-configuration` section). It means they must have been registered
there when running ``felyx-extraction``.

If ``--extract-from-source`` is not set, the assembling is done by collecting
and stacking pre-extracted child products (if ``--create-child-products`` was
used when running ``felyx-extraction``). Unless the root directy of the created
child products is the current directory, it must be set in
``--child-product-dir`` argument.



Configuration
=============

Multi-match-up datasets can be configured by extending an extraction block in
``MMDatasets`` with the ``output`` section, that defines how to join the
different extracted child products and dynamic site data into periodic output
files.

The following detailed example extends the previous example in :ref:`felyx
extraction section <matchup_extraction>` with an extended ``MMDatasets`` block:


.. code-block:: yaml

    # MMDatasets: dictionary of the extractions that can be called in
    # felyx-extraction.
    # The keys of this dictionary are the identifiers of each extraction, which
    # should be a single string with no spaces or special characters.
    MMDatasets:
      SLSTRA-MAR-L2P-v1.0_test4dyn:
        # site_collection: the site collection for which EO data are extracted in
        # this extraction. Must be one of the keys defined in `SiteCollections`
        # dictionary.
        site_collection_id: test4syn
        # eo_datasets: the dictionary of EO datasets which can be extracted for
        # above `site_collection`, where keys are the identifier of the datasets,
        # and values the extraction parameters for the pair (EO dataset, site
        #  collection)
        eo_datasets:
          # identifier of the EO dataset matched with the site collection. Must be
          # one of the keys defined in `EODatasets` dictionary.
          SLSTRA-MAR-L2P-v1.0:
            # subset_size: the size, in number of consecutive measurements, of the
            # child product to be extracted, centered on the matched site time and
            # location. Must be an odd number. Valid for dynamic site collections
            # only.
            subset_size: 21
            # matching_criteria: the search criteria for EO observation / dynamic
            # site match-ups
            matching_criteria:
              # time_window: the maximum time difference between the EO
              # observation and the nearest site data, in minutes.
              time_window: 120
              # search_radius: the maximum spatial distance between the EO
              # observation and the nearest site data, in km.
              search_radius: 5.

        # output: this block describes how to assemble the match-ups into multi
        # match-up datasets. It is used only by felyx-assemble command.
        output:
          # the temporal extent of each created match-up file (using pandas
          # notation for date range
          frequency: 6H

          # history length of the dynamic site data to include with the matched
          # measurement, as a duration in minutes before and after the matching time
          history: 360

          # default time units default time units in CF convention style (default is
          # 'seconds since 1950-01-01')
          default_time_units: seconds since 1980-01-01

          # The output products that will be written to disk, where keys are the
          # identifier of each product and the values their definition.
          # In most case, there would be only one output product with all the
          # selected dataset variables and attributes. However, one can define
          # multiple products, each one having a particular selection of EO
          # datasets, variables and attributes.
          products:
            SLSTRA-MAR-L2P-v1.0_test4dyn:
              # file pattern for the output product
              filenaming: '%Y/%Y%m%d%H%M%S_SLSTRA-MAR-L2P-v1.0__test4syn.nc'
              # tailor the content of the assembled files for the output product
              content:
                SLSTRA-MAR-L2P-v1.0:
                  # [Optional] list of variables to include in the assembled files
                  # (all of them by default). Python regexp can be used to select
                  # several variables at once.
                  variables: [.*]

                  # [Optional] variables NOT TO include in extracted child products
                  # (none of them by default)
                  #except_variables:

                  # [Optional] list of global attributes to include in the assembled
                  # files (all of them by default)
                  #attributes:

                  # [Optional] list of global attributes NOT TO include in the assembled
                  # files (none of them by default)
                  except_attributes: [.*]

                  # [Optional] list of global attributes from the child products
                  # to stack as new variables into the assembled files.
                  attributes_as_variables:
                    - date_created

                  # prefix by which to rename all variables and global attributes
                  # coming from this dataset (by default the dataset id is used)
                  prefix: s3a


The source data matching and extraction configuration to search and extract
the individual matchups is described in the :ref:`match-up extraction section
<matchup_extraction>`. The configuration part for the assembling of these
individual match-ups into multi-match-up files is defined in the ``output``
block. It is described in further details in the following subsections,
explaining and expanding the above example.

.. toctree::
   :maxdepth: 1

   configuring subproducts <configuration/subproducts>
   sibling datasets <configuration/siblings>


Updating multi-match-up files
=============================
If the input EO data or dynamic site's data have been reprocessed or changed
in some way, one may want to regenerate the MMDB files without running the
whole process from the beginning.

Updated dynamic site's data
---------------------------
If the dynamic site's data were updated, just replace the parquet files (or,
in the  configuration file - ``filepath_pattern``, point to another location
where the updated parquet files are stored) en re-run ``felyx-assemble``
command.

.. Note::

  if additional sites were added to the parquet files, they are ignored as
  this would require to re-run ``felyx-extraction`` as well.


Updated EO source data
----------------------
A frequent use case if for instance when the EO data were reprocessed with a
more a up-to-date algorithm.


Caveats
=======

The match-up assembling may fail in one of the following cases:

* some variables are not provided in every source file: the stacking of the
  corresponding match-ups will fail if they contain a different number of
  variables. A solution is to exclude these variables from the output content
  (``MMDatasets / <MMDB identifier> / output / products / <subproduct
  identifier> / content / <dataset identifier> / except_variables``)
