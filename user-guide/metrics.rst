=======
Metrics
=======

**Metrics** are operators applied to the content of the extracted child
products that returns a single value characterizing the content of these
child products. This value can be indifferently a number or a string. This means
that this value can represent a quantitative value or a qualitative value
(a property).

These metrics can then be registered into Elasticsearch or saved on disk into
json files, to be collected and analysed later to display diagnostics, etc...
based on the extracted match-ups.

**felyx** comes with a selection of predefined operators. The list of available
operators can be extended through the built-in **plugin extension** mechanism
(refer to the developer guide).
