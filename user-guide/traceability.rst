============
Traceability
============

In addition to the data arrays, in situ and EO data files are usually
enriched  by global metadata attributes, which specify further the
acquisition or processing context. These pieces of information can be
important to later analyse discrepancies or errors, and stratify the match-up
datasets.

**felyx** allows to convey this metadata information from both the source
EO files and data associated with the dynamic sites into the final
multi-match-up files in different ways.

EO data traceability
====================
When extracting a child product from a source EO files, felyx adds a few
traceability global attributes:

* the name of the source file it was extracted from, in ``__source``
* the slices of the extracted subset, in ``__source_slices``

Besides, any of the source file's global attributes will be copied by
default: they may contain additional traceability information such as the
production version, processing baseline identifier, the creation date, etc...

To include the values of these attributes into the assembled match-up files,
they need to be converted to variables. Simply fill in
``attributes_as_variables`` list in the configuration of the Multi-Match-Up
dataset, as in the example below:

.. code-block:: yaml

      products:
        SLSTRA-MAR-L2P-v1.0_test4dyn:
          # file pattern for the output product
          filenaming: '%Y/%Y%m%d%H%M%S_SLSTRA-MAR-L2P-v1.0__test4syn.nc'
          # tailor the content of the assembled files for the output product
          content:
            SLSTRA-MAR-L2P-v1.0:
              # [Optional] list of variables to include in the assembled files
              # (all of them by default). Python regexp can be used to select
              # several variables at once.
              variables: [.*]

              # [Optional] variables NOT TO include in extracted child products
              # (none of them by default)
              #except_variables:

              # [Optional] list of global attributes to include in the assembled
              # files (all of them by default)
              #attributes:

              # [Optional] list of global attributes NOT TO include in the assembled
              # files (none of them by default)
              except_attributes: [.*]

              # [Optional] list of global attributes from the child products
              # to stack as new variables into the assembled files.
              attributes_as_variables:
                - date_created

              # [Optional] complementary hard-coded (constant) global attributes
              # to include in all output files
              custom_attributes:
                mmdb_version: v1.0

              # prefix by which to rename all variables and global attributes
              # coming from this dataset (by default the dataset id is used)
              prefix: s3a


Global attributes which have a constant value for all extracted child product
do not need to be converted in variables, which would be a waste of space.

dynamic site's data traceability
================================

To ensure the traceability of the dynamic data (usually in situ measurements),
it is best to include these information as extra columns in the data parquet
files, or global attributes in the parquet metadata (for values constant over
the whole parquet dataset). Such additional information may be:

* the name of the source file for the data
* a version number corresponding to the source data file or dataset
* a creation date
* ...

multi-match-up dataset traceability
===================================

The MMDB files can also include some user defined traceability attributes.
Simply add them in the ``custom_attributes`` section of the MMDB definion block.
For instance adding a version number in ``mmdb_version`` attribute in the
above example.

