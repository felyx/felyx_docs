.. _`online documentation`: http://cerbere.readthedocs.org


====================
Child product format
====================

Child products are saved in netCDF4 format.

Content
=======

The child products include all the variables and attributes extracted from
their source file by default, unless some selection/exclusion settings are
specified in the  processing configuration file.

In addition, the extracted child products are documented with a few *felyx*
specific attributes and variables, providing the extraction context and
metadata.

The list of attributes is described in the following table:

.. csv-table:: list of felyx attributes added to the extracted child products
   :file: child_attributes.csv
   :delim: ;
   :header-rows: 1


**felyx** adds also the following mask variable, indicating which measurements
in the extracted child product are including in the colocated site boundaries
(or colocation radius).

.. code-block:: bash

	byte miniprod_content_mask(time) ;
		miniprod_content_mask:_FillValue = -128b ;
		miniprod_content_mask:long_name = "miniprod region of interest mask" ;
		miniprod_content_mask:flag_meanings = "outside_miniprod inside_miniprod" ;
		miniprod_content_mask:flag_values = 0b, 1b ;
		miniprod_content_mask:coordinates = "lat lon" ;


.. note::

    child products are in netCDF format, **whatever the format of the
    source data**. This independency to the source data format is achieved
    thanks to the **cerbere** python package developed by Ifremer that expose
    the content of any data format through the same interface (close to
    self-descriptive formats such as NetCDF). This package handles already a
    large range of formats and products, and can be extended in a flexible
    way. More information on the **cerbere** package and how to extend it is
    provided in the `online documentation`_ of **cerbere**.



Format example
==============

The format of the child products ensures that:

  * **latitude**, **longitude** and **time** information are always expressed
    in the same way (same field names and units).
  * similar data acquisition patterns will always be structured the same way,
    having the same **spatial and temporal dimensions**. The names and list of
    these dimensions for each type of pattern(**swath**, **grid**,
    **trajectory**) is also described in the `online documentation`_ of
    **cerbere**.

Here is an example of the format for a child product extracted over a static
site from a **swath** file (Sentinel-3A L2P Sea Surface Temperature)::

    netcdf \20180226001729_dummy1_SLSTRA-MAR-L2P-v1.0 {
    dimensions:
        row = 228 ;
        cell = 245 ;
        channel = 3 ;
    variables:
        float lat(row, cell) ;
            lat:_FillValue = 1.e+20f ;
            lat:least_significant_digit = 3LL ;
            lat:comment = "Geographical coordinates" ;
            lat:long_name = "Latitude" ;
            lat:standard_name = "latitude" ;
            lat:units = "degrees_north" ;
            lat:valid_max = 90.f ;
            lat:valid_min = -90.f ;
            lat:axis = "Y" ;
            lat:valid_range = -90.f, 90.f ;
        short l2p_flags(row, cell) ;
            l2p_flags:comment = "Flags should be used to properly interpret the data" ;
            l2p_flags:flag_masks = 1s, 2s, 4s, 8s, 16s, 32s, 64s, 128s, 256s, 512s, 1024s, 2048s, 4096s, 8192s, 16384s ;
            l2p_flags:flag_meanings = "microwave land ice lake river tidal cosmetic_fill day sun_glint cloud pointing exception overflow stratospheric dual_nadir_diff_sst_type" ;
            l2p_flags:long_name = "L2P and user-defined SST quality flags" ;
            l2p_flags:coordinates = "lon lat" ;
        float nedt(channel, row, cell) ;
            nedt:_FillValue = 1.e+20f ;
            nedt:comment = "Top of atmosphere noise equivalent brightness temperature (1st channel = S7; 2nd = S8; 3rd = S9)" ;
            nedt:long_name = "Top of atmosphere noise equivalent brightness temperature (experimental field)" ;
            nedt:units = "kelvin" ;
            nedt:valid_max = 32767.f ;
            nedt:valid_min = -32767.f ;
            nedt:coordinates = "lon lat" ;
            nedt:add_offset = 0. ;
            nedt:scale_factor = 0.001 ;
        float nadir_sst_theoretical_uncertainty(row, cell) ;
            nadir_sst_theoretical_uncertainty:_FillValue = 1.e+20f ;
            nadir_sst_theoretical_uncertainty:comment = "Nadir SST theoretical uncertainty, supplied within the dual swath region, when dual_nadir_sst_difference available" ;
            nadir_sst_theoretical_uncertainty:long_name = "Nadir SST total uncertainty (experimental field)" ;
            nadir_sst_theoretical_uncertainty:units = "kelvin" ;
            nadir_sst_theoretical_uncertainty:valid_max = 32767.f ;
            nadir_sst_theoretical_uncertainty:valid_min = -32767.f ;
            nadir_sst_theoretical_uncertainty:coordinates = "lon lat" ;
            nadir_sst_theoretical_uncertainty:add_offset = 0. ;
            nadir_sst_theoretical_uncertainty:scale_factor = 0.001 ;
        float satellite_zenith_angle(row, cell) ;
            satellite_zenith_angle:_FillValue = 1.e+20f ;
            satellite_zenith_angle:comment = "Satellite zenith angle at time of observation" ;
            satellite_zenith_angle:long_name = "Satellite zenith_angle (experimental field)" ;
            satellite_zenith_angle:standard_name = "zenith_angle" ;
            satellite_zenith_angle:units = "angular_degrees" ;
            satellite_zenith_angle:valid_max = 90.f ;
            satellite_zenith_angle:valid_min = -90.f ;
            satellite_zenith_angle:coordinates = "lon lat" ;
            satellite_zenith_angle:add_offset = 0. ;
            satellite_zenith_angle:scale_factor = 1. ;
        float adi_dtime_from_sst(row, cell) ;
            adi_dtime_from_sst:_FillValue = 1.e+20f ;
            adi_dtime_from_sst:comment = "Difference in hours between ADI and SST" ;
            adi_dtime_from_sst:long_name = "Time difference of ADI data from SST measurement" ;
            adi_dtime_from_sst:units = "hour" ;
            adi_dtime_from_sst:valid_max = 127.f ;
            adi_dtime_from_sst:valid_min = -127.f ;
            adi_dtime_from_sst:coordinates = "lon lat" ;
            adi_dtime_from_sst:add_offset = 0. ;
            adi_dtime_from_sst:scale_factor = 0.1 ;
        float lon(row, cell) ;
            lon:_FillValue = 1.e+20f ;
            lon:least_significant_digit = 3LL ;
            lon:comment = "Geographical coordinates" ;
            lon:long_name = "Longitude" ;
            lon:standard_name = "longitude" ;
            lon:units = "degrees_east" ;
            lon:valid_max = 180.f ;
            lon:valid_min = -180.f ;
            lon:axis = "X" ;
            lon:valid_range = -180.f, 180.f ;
        float sea_ice_fraction(row, cell) ;
            sea_ice_fraction:_FillValue = 1.e+20f ;
            sea_ice_fraction:comment = "Fractional sea ice cover" ;
            sea_ice_fraction:long_name = "Fractional sea ice contamination in a pixel" ;
            sea_ice_fraction:source = "ECMWF" ;
            sea_ice_fraction:standard_name = "sea_ice_area_fraction" ;
            sea_ice_fraction:units = "1" ;
            sea_ice_fraction:valid_max = 100.f ;
            sea_ice_fraction:valid_min = -100.f ;
            sea_ice_fraction:coordinates = "lon lat" ;
            sea_ice_fraction:add_offset = 0.5 ;
            sea_ice_fraction:scale_factor = 0.005 ;
        float dual_nadir_sst_difference(row, cell) ;
            dual_nadir_sst_difference:_FillValue = 1.e+20f ;
            dual_nadir_sst_difference:comment = "Dual minus nadir SST difference stored as GHRSST experimental field" ;
            dual_nadir_sst_difference:long_name = "Dual minus nadir SST difference (experimental field)" ;
            dual_nadir_sst_difference:units = "kelvin" ;
            dual_nadir_sst_difference:valid_max = 32767.f ;
            dual_nadir_sst_difference:valid_min = -32767.f ;
            dual_nadir_sst_difference:coordinates = "lon lat" ;
            dual_nadir_sst_difference:add_offset = 0. ;
            dual_nadir_sst_difference:scale_factor = 0.001 ;
        byte sst_algorithm_type(row, cell) ;
            sst_algorithm_type:comment = "L2P sea surface temperature retrieval algorithm" ;
            sst_algorithm_type:flag_meanings = "no_retrieval N2_retrieval N3R_retrieval N3_retrieval D2_retrieval D3_retrieval" ;
            sst_algorithm_type:flag_values = 0b, 1b, 2b, 3b, 4b, 5b ;
            sst_algorithm_type:long_name = "SST algorithm types" ;
            sst_algorithm_type:coordinates = "lon lat" ;
        float sses_standard_deviation(row, cell) ;
            sses_standard_deviation:_FillValue = 1.e+20f ;
            sses_standard_deviation:comment = "Standard deviation estimate derived using the techniques described at https://www.ghrsst.org/ghrsst/tags-and-wgs/stval-wg/sses-description-of-schemes" ;
            sses_standard_deviation:long_name = "Single Sensor Error Statistic standard deviation estimate" ;
            sses_standard_deviation:units = "kelvin" ;
            sses_standard_deviation:valid_max = 127.f ;
            sses_standard_deviation:valid_min = -127.f ;
            sses_standard_deviation:coordinates = "lon lat" ;
            sses_standard_deviation:add_offset = 1.27 ;
            sses_standard_deviation:scale_factor = 0.01 ;
        float dt_analysis(row, cell) ;
            dt_analysis:_FillValue = 1.e+20f ;
            dt_analysis:comment = "The difference between the SST observation and the last SST analysis" ;
            dt_analysis:long_name = "SST deviation from last analysis field" ;
            dt_analysis:reference = "OSTIA L4 SST analysis" ;
            dt_analysis:units = "kelvin" ;
            dt_analysis:valid_max = 127.f ;
            dt_analysis:valid_min = -127.f ;
            dt_analysis:coordinates = "lon lat" ;
            dt_analysis:add_offset = 0. ;
            dt_analysis:scale_factor = 0.1 ;
        float wind_speed_dtime_from_sst(row, cell) ;
            wind_speed_dtime_from_sst:_FillValue = 1.e+20f ;
            wind_speed_dtime_from_sst:comment = "Difference in hours between wind speed and SST" ;
            wind_speed_dtime_from_sst:long_name = "Time difference of wind speed from SST measurement" ;
            wind_speed_dtime_from_sst:units = "hour" ;
            wind_speed_dtime_from_sst:valid_max = 127.f ;
            wind_speed_dtime_from_sst:valid_min = -127.f ;
            wind_speed_dtime_from_sst:coordinates = "lon lat" ;
            wind_speed_dtime_from_sst:add_offset = 0. ;
            wind_speed_dtime_from_sst:scale_factor = 0.1 ;
        float brightness_temperature(channel, row, cell) ;
            brightness_temperature:_FillValue = 1.e+20f ;
            brightness_temperature:comment = "Top of the atmosphere brightness temperature (1st channel = S7; 2nd = S8; 3rd = S9)" ;
            brightness_temperature:long_name = "Top of atmosphere brightness temperature (experimental field)" ;
            brightness_temperature:standard_name = "toa_brightness_temperature" ;
            brightness_temperature:units = "kelvin" ;
            brightness_temperature:valid_max = 32767.f ;
            brightness_temperature:valid_min = -32767.f ;
            brightness_temperature:coordinates = "lon lat" ;
            brightness_temperature:add_offset = 290. ;
            brightness_temperature:scale_factor = 0.01 ;
        byte quality_level(row, cell) ;
            quality_level:comment = "Quality levels used for all GHRSST SST data" ;
            quality_level:flag_meanings = "no_data cloud worst_quality low_quality acceptable_quality best_quality" ;
            quality_level:flag_values = 0b, 1b, 2b, 3b, 4b, 5b ;
            quality_level:long_name = "SST measurement quality indicator" ;
            quality_level:coordinates = "lon lat" ;
        double time(row, cell) ;
            time:_FillValue = 1.e+20 ;
            time:long_name = "measurement time" ;
            time:standard_name = "time" ;
            time:axis = "T" ;
            time:units = "seconds since 1981-01-01T00:00:00+00:00" ;
            time:calendar = "proleptic_gregorian" ;
        float sst_theoretical_uncertainty(row, cell) ;
            sst_theoretical_uncertainty:_FillValue = 1.e+20f ;
            sst_theoretical_uncertainty:comment = "Theoretical uncertainty of SST" ;
            sst_theoretical_uncertainty:long_name = "SST total uncertainty (experimental field)" ;
            sst_theoretical_uncertainty:standard_name = "sea_surface_skin_temperature_standard_error" ;
            sst_theoretical_uncertainty:units = "kelvin" ;
            sst_theoretical_uncertainty:valid_max = 32767.f ;
            sst_theoretical_uncertainty:valid_min = -32767.f ;
            sst_theoretical_uncertainty:coordinates = "lon lat" ;
            sst_theoretical_uncertainty:add_offset = 0. ;
            sst_theoretical_uncertainty:scale_factor = 0.001 ;
        float sses_bias(row, cell) ;
            sses_bias:_FillValue = 1.e+20f ;
            sses_bias:comment = "Bias estimate derived using the techniques described at https://www.ghrsst.org/ghrsst/tags-and-wgs/stval-wg/sses-description-of-schemes" ;
            sses_bias:long_name = "Single Sensor Error Statistic bias estimate" ;
            sses_bias:units = "kelvin" ;
            sses_bias:valid_max = 127.f ;
            sses_bias:valid_min = -127.f ;
            sses_bias:coordinates = "lon lat" ;
            sses_bias:add_offset = 0.f ;
            sses_bias:scale_factor = 0.01f ;
        float probability_cloud_single_in(row, cell) ;
            probability_cloud_single_in:_FillValue = 1.e+20f ;
            probability_cloud_single_in:comment = "Probability of cloud in pixel as estimated by Bayesian Cloud detection on a single view" ;
            probability_cloud_single_in:long_name = "Probability of cloud in pixel (single view) computed on the 1 km nadir view grid" ;
            probability_cloud_single_in:valid_max = 100.f ;
            probability_cloud_single_in:valid_min = -100.f ;
            probability_cloud_single_in:coordinates = "lat lon time" ;
            probability_cloud_single_in:add_offset = 0.5 ;
            probability_cloud_single_in:scale_factor = 0.005 ;
        float probability_cloud_single_io(row, cell) ;
            probability_cloud_single_io:_FillValue = 1.e+20f ;
            probability_cloud_single_io:comment = "Probability of cloud in pixel as estimated by Bayesian Cloud detection on a single view" ;
            probability_cloud_single_io:long_name = "Probability of cloud in pixel (single view) computed on the 1 km oblique view grid" ;
            probability_cloud_single_io:valid_max = 100.f ;
            probability_cloud_single_io:valid_min = -100.f ;
            probability_cloud_single_io:coordinates = "lat lon time" ;
            probability_cloud_single_io:add_offset = 0.5 ;
            probability_cloud_single_io:scale_factor = 0.005 ;
        float aerosol_dynamic_indicator(row, cell) ;
            aerosol_dynamic_indicator:_FillValue = 1.e+20f ;
            aerosol_dynamic_indicator:comment = "Based on AATSR Saharan Dust Index" ;
            aerosol_dynamic_indicator:long_name = "Aerosol contamination indicator" ;
            aerosol_dynamic_indicator:source = "Saharan Dust Index" ;
            aerosol_dynamic_indicator:units = "count" ;
            aerosol_dynamic_indicator:valid_max = 127.f ;
            aerosol_dynamic_indicator:valid_min = -127.f ;
            aerosol_dynamic_indicator:coordinates = "lon lat" ;
            aerosol_dynamic_indicator:add_offset = 0. ;
            aerosol_dynamic_indicator:scale_factor = 0.05 ;
        float sea_ice_fraction_dtime_from_sst(row, cell) ;
            sea_ice_fraction_dtime_from_sst:_FillValue = 1.e+20f ;
            sea_ice_fraction_dtime_from_sst:comment = "Difference in hours between sea ice fraction and SST" ;
            sea_ice_fraction_dtime_from_sst:long_name = "Time difference between sea ice fraction data from SST measurement" ;
            sea_ice_fraction_dtime_from_sst:units = "hour" ;
            sea_ice_fraction_dtime_from_sst:valid_max = 127.f ;
            sea_ice_fraction_dtime_from_sst:valid_min = -127.f ;
            sea_ice_fraction_dtime_from_sst:coordinates = "lon lat" ;
            sea_ice_fraction_dtime_from_sst:add_offset = 0. ;
            sea_ice_fraction_dtime_from_sst:scale_factor = 0.1 ;
        float sea_surface_temperature(row, cell) ;
            sea_surface_temperature:_FillValue = 1.e+20f ;
            sea_surface_temperature:comment = "Marine skin surface temperature" ;
            sea_surface_temperature:depth = "10 micrometres" ;
            sea_surface_temperature:long_name = "sea surface skin temperature" ;
            sea_surface_temperature:standard_name = "sea_surface_skin_temperature" ;
            sea_surface_temperature:units = "kelvin" ;
            sea_surface_temperature:valid_max = 4500.f ;
            sea_surface_temperature:valid_min = -300.f ;
            sea_surface_temperature:coordinates = "lon lat" ;
            sea_surface_temperature:add_offset = 273.15 ;
            sea_surface_temperature:scale_factor = 0.01 ;
        float wind_speed(row, cell) ;
            wind_speed:_FillValue = 1.e+20f ;
            wind_speed:comment = "10 m wind speed from ECMWF" ;
            wind_speed:height = "10 m" ;
            wind_speed:long_name = "10 m wind speed" ;
            wind_speed:source = "ECMWF" ;
            wind_speed:standard_name = "wind_speed" ;
            wind_speed:units = "m s-1" ;
            wind_speed:valid_max = 127.f ;
            wind_speed:valid_min = -127.f ;
            wind_speed:coordinates = "lon lat" ;
            wind_speed:add_offset = 25.4 ;
            wind_speed:scale_factor = 0.2 ;
        byte miniprod_content_mask(row, cell) ;
            miniprod_content_mask:_FillValue = -128b ;
            miniprod_content_mask:long_name = "miniprod region of interest mask" ;
            miniprod_content_mask:flag_meanings = "outside_miniprod inside_miniprod" ;
            miniprod_content_mask:flag_values = 0b, 1b ;
            miniprod_content_mask:coordinates = "lat lon time" ;

    // global attributes:
            :Conventions = "CF-1.6, Unidata Observation Dataset v1.0" ;
            :Metadata_Conventions = "Unidata Dataset Discovery v1.0" ;
            :acknowledgment = "European Commission Copernicus Programme" ;
            :cdm_data_type = "swath" ;
            :comment = "GHRSST SST L2P" ;
            :creator_email = "ops@eumetsat.int" ;
            :creator_name = "EUMETSAT" ;
            :creator_url = "http://navigator.eumetsat.int/" ;
            :date_created = "2022-08-12T14:17:21Z" ;
            :easternmost_longitude = 177.459854125977 ;
            :file_quality_level = 3s ;
            :gds_version_id = "2.0r5" ;
            :geospatial_lat_resolution = 0.009f ;
            :geospatial_lat_units = "degrees_north" ;
            :geospatial_lon_resolution = 0.009f ;
            :geospatial_lon_units = "degrees_east" ;
            :history = "Sentinel-3 Optical Instrument Processing Facility  2018-09-29T06:53:21Z: PUGCoreProcessor joborder.4287024.xml" ;
            :id = "SLSTRA-MAR-L2P-v1.0" ;
            :institution = "MAR" ;
            :keywords = "Oceans > Ocean Temperature > Sea Surface Temperature" ;
            :keywords_vocabulary = "NASA Global Change Master Directory (GCMD) Science Keywords" ;
            :license = "Copernicus Programme free, full and open data policy" ;
            :metadata_link = "N/A" ;
            :naming_authority = "org.ghrsst" ;
            :netcdf_version_id = "4.2 of Jul  5 2012 17:07:43 $" ;
            :northernmost_latitude = 88.7633666992188 ;
            :platform = "Sentinel3A" ;
            :processing_level = "L2P" ;
            :product_version = "1.0" ;
            :project = "Group for High Resolution Sea Surface Temperature" ;
            :publisher_email = "ghrsst-po@nceo.ac.uk" ;
            :publisher_name = "The GHRSST Project Office" ;
            :publisher_url = "http://www.ghrsst.org" ;
            :references = "S3IPF PDS 005 - i2r7 - Product Data Format Specification - SLSTR, S3IPF PDS 002 - i1r6 - Product Data Format Specification - Product Structures, S3IPF DPM 007 - i1r8 - Detailed Processing Model - SLSTR Level 2" ;
            :sensor = "SLSTR" ;
            :source = "IPF-SL-2 06.14" ;
            :southernmost_latitude = -85.899543762207 ;
            :spatial_resolution = "1 km at nadir" ;
            :standard_name_vocabulary = "NetCDF Climate and Forecast (CF) Metadata Convention" ;
            :start_time = "20180226T001644Z" ;
            :stop_time = "20180226T015743Z" ;
            :summary = "Sentinel-3A SLSTR skin sea surface temperature" ;
            :time_coverage_end = "2018-02-26T01:57:43Z" ;
            :time_coverage_start = "2018-02-26T00:16:44Z" ;
            :title = "Sentinel-3A SLSTR L2P SST dataset" ;
            :uuid = "TBC" ;
            :westernmost_longitude = -175.249542236328 ;
            :__id = "Reprocessed WST - SLSTRA-MAR-L2P-v1.0_dummy1" ;
            :__title = "child product derived from 20180226001644-MR1-L2P_GHRSST-SSTskin-SLSTRA-20180929065321-v02.0-fv01.0.nc over felyx site dummy1" ;
            :__summary = "child product created by felyx on 20220812T141721 from /home/jfpiolle/git/felyx_processor/tests/data/eo/slstr-a/20180226001644-MR1-L2P_GHRSST-SSTskin-SLSTRA-20180929065321-v02.0-fv01.0.nc provided by EUMETSAT. Contains data over dummy1 (a polygon with bounds from -73.0 to -72.0 degrees North and from 50.0 to 57.0 degrees East)." ;
            :__history = "Created on 20220812T141721 with /home/jfpiolle/git/felyx_processor/tests/data/eo/slstr-a/20180226001644-MR1-L2P_GHRSST-SSTskin-SLSTRA-20180929065321-v02.0-fv01.0.nc" ;
            :__date_created = "2022-08-12T14:17:21.337996" ;
            :__date_modified = "2022-08-12T14:17:21.337996" ;
            :__keywords = "" ;
            :__acknowledgement = "" ;
            :__license = "" ;
            :__format_version = "Felyx 1.0" ;
            :__creator_name = "jfpiolle" ;
            :__creator_email = "" ;
            :__creator_url = "" ;
            :__references = "" ;
            :__time_coverage_resolution = "" ;
            :__source = "20180226001644-MR1-L2P_GHRSST-SSTskin-SLSTRA-20180929065321-v02.0-fv01.0.nc" ;
            :__source_slices = "{\'row\': slice(0, 228, None), \'cell\': slice(0, 245, None)}" ;
            :__site_id = "dummy1" ;
            :__site_name = "dummy1" ;
            :__site_geometry = "POLYGON ((50 -73, 50 -72, 57 -72, 57 -73, 50 -73))" ;
            :__dataset_id = "SLSTRA-MAR-L2P-v1.0" ;
            :__dataset_name = "Reprocessed WST - SLSTRA-MAR-L2P-v1.0" ;
            :__site_collection_id = "test4static" ;
            :__site_collection_name = "test_static" ;
            :__site_collection_summary = "Static collection" ;
            :__geospatial_lon_min = 50. ;
            :__geospatial_lon_max = 57. ;
            :__geospatial_lat_min = -73. ;
            :__geospatial_lat_max = -72. ;
            :__percentage_coverage_of_site_by_miniprod = 58.8 ;
            :__time_coverage_start = "2018-02-26T00:17:29Z" ;
            :__time_coverage_end = "2018-02-26T00:18:33Z" ;
            :__source_time_coverage_end = "2018-02-26T01:57:43Z" ;
            :__source_time_coverage_start = "2018-02-26T00:16:44Z" ;
            :institution_abbreviation = "ifremer/cersat" ;
            :processing_software = "Cersat/Cerbere 1.0" ;
            :geospatial_vertical_units = "meters above mean sea level" ;
            :geospatial_vertical_positive = "up" ;
            :date_modified = "2022-08-12T14:17:21Z" ;
    }
