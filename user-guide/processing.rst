.. _jobard : https://jobard.gitlab-pages.ifremer.fr/documentation/

.. _processing:


===================
Processing in felyx
===================

**felyx** is meant to operate on different platforms and to take advantage of
them, providing in particular the capability to distribute the processing
load, using the available or felyx provided task scheduling.

**felyx** can therefore operate on a standalone PC, on a set of virtual
machines in a cloud environment or on a HPC infrastructure.

Standalone processing
=====================

**felyx** can run in standalone over a single laptop or workstation. In this
configuration, the different commands should be run sequentially. The overall
processing time will be longer but it can be acceptable for a limited or
occasional usage. In this configuration, run the ``felyx-extraction`` or
``felyx-assemble`` commands as shown throughout the different examples in
this documentation.

Distributed processing
======================

In the context where hundreds or thousands of EO data files must be
processed, it may be necessary to distribute the processing over multiple
machines in a cloud or High Performance Computing (HPC) infrastructure.

Many of such infrastructures provide tools to distribute jobs over multiple
processing nodes. It is perfectly fine to run **felyx** commands through these
tools. Particular attention shall be paid to the allocation of the memory and
CPU time resources which often must be explicitly set (or the jobs would be
killed if they use more than the default resources); these settings are
strongly dependant on the type and number of the processed input files or the
number of matchups potentially extracted, they are therefore application
dependant and shall be set by the user accordingly.


Distributed processing with jobard
==================================

**felyx** operates well together with jobard_, a framework to distribute the
processing load in job array over multiple nodes, whether it is a cloud
computing platform or an HPC infrastructure. jobard_ is independent of
**felyx** and can be used for other similar embarassingly parallel usages where
multiple independent jobs run simultaneously over a set of available resources.

Overview
--------

For jobard_ installation and usage, please refer to the jobard_  online
documentation.

The ``felyx-mmdb`` command is integrated with jobard

Testing distributed processing
------------------------------

Once **jobard** job scheduler is set-up, you can verify the overall behaviour
by running the following small test that builds a complete MMDB.

To run this test, ``felyx-processor`` source repository must be downloaded
locally:

.. code-block:: bash

  git clone git+https://gitlab.ifremer.fr/felyx/felyx_processor.git

Activate the conda environment where felyx was installed and then go into
``felyx-processor`` directory and run the following command:

.. code-block:: bash

  # activate conda env <myenv> (must contain felyx-processor and
  # jobard-client packages
  conda activate myenv

  cd felyx_processor
  mkdir tests/tmp/

  # replace in the following command <jobard api> with the URL of the
  # installed jobard API server
  felyx-mmdb tests/resources/test_slstr_a_cmems/s3a_mmdb.yaml \
    --mmdb-id  S3A_SL__OPE_NRT__cmems_drifter \
    --manifest-dir tests/tmp/ --skip-sibling-extraction \
    --start 2021-09-15 --end 2021-09-16 \
    --mmdb-dir tests/tmp/ \
    --logfile tests/tmp/e2e_mmdb.log \
    -u http://visi-common-bigdata-test.ifremer.fr \
    --felyx-extraction-bin /home1/datahome/cerint/.conda/envs/felyx_processor_master_26072022_2/bin/felyx-extraction \
    --felyx-assemble-bin /home1/datahome/cerint/.conda/envs/felyx_processor_master_26072022_2/bin/felyx-assemble


Note that if jobard is run over PBS or HTCondor, the full path to
``felyx-extraction`` and ``felyx-assemble`` binaries within the conda
environment of the cluster must be provided:

.. code-block:: bash

  felyx-mmdb tests/resources/test_slstr_a_cmems/s3a_mmdb.yaml \
    --mmdb-id  S3A_SL__OPE_NRT__cmems_drifter \
    --manifest-dir tests/tmp/ --skip-sibling-extraction \
    --start 2021-09-15 --end 2021-09-16 \
    --mmdb-dir tests/tmp/ \
    --logfile tests/tmp/e2e_mmdb.log \
    -u <jobard api> \
    --felyx-extraction-bin <full path to felyx-extraction> \
    --felyx-assemble-bin <full path to felyx-assemble>

It may be needed also to provide full path patterns in the configuration file
``tests/resources/test_slstr_a_cmems/s3a_mmdb.yaml`` and in all above path
arguments.
