==============
Child products
==============

**Child products** are subsets from larger input EO files matching the
boundaries of static or dynamic sites. We call **extraction** the process of
subsetting larger original Earth Observation files (swath, grid, image,
along-track,...) over these sites.

The extraction process extracts all the fields from the source file: you will
get **the same list of fields** in the extracted child product. The child
products are reformatted to a **common NetCDF format** using the same naming
conventions for spatial and temporal dimensions and variables.

The extraction works slightly differently over static sites (which have
permanent boundaries) and dynamic sites.

Static miniprods
================

Over a static site, the child product is defined as the section of a file fully
including the boundaries of the site. This is purely an intersection
problem:

.. figure:: /_static/static_miniprod.png


In this example, a swath is subsetted so that it fully includes the site
area (in yellow). If its boundaries overlap the swath edge, then only the swath
section intersecting the site area will be extracted and it will not fully
include the site boundaries in this case.

.. note::

   There is no remapping, filtering or resampling of the extracted data. They are
   stored in their native projection with the exact same content.


.. note::

   In order to make comparisons between child product easier, a mask is added to
   a child product. It specifies which pixels from the extracted subset are
   within the site limits or outside.

.. figure:: /_static/static_miniprod_mask.png
    

Image of the mask corresponding to the same child product as above. The red area
indicates which pixels are within the static site limits.

.. note::

  It is possible to forbid a child product creation when there is not a
  single valid pixel within the site limits.


Dynamic miniprods
=================

Dynamic sites have no predefined shape and locations like the static site. They
consist of a list of times and locations (vertices defining a trajectory).

**felyx** will extract a child product from a source file where it intersects a
location of this trajectory, under the condition that the time difference of
these source data with the time associated with the trajectory location is
lower than a given threshold (or **time window**).

The size of the extracted child product is given in pixels by the user, in the
extraction configuration. For a swath or grid source file, it is a box
of N x N pixels where N is the chosen size, centered on the pixel matching
the trajectory vertex. For along-track data, it is a segment of N pixels
centered on the trajectory vertex.


  .. image:: /_static/dynamic_miniprod.png

  .. image:: /_static/dynamic_miniprod_mask.png


.. note::

  It is possible to forbid a child product creation when there is not a single
  valid pixel within the colocation radius (the limits of the site).

