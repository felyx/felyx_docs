.. _felyx-mmdb:
.. _felyx_distributed: https://gitlab.ifremer.fr/felyx/felyx_distributed

===================================
Complete Matchup Dataset Production
===================================

The complementary package `felyx_distributed`_ provides a ``felyx-mmdb``
command which combines ``felyx-extraction`` and ``felyx-assemble``
into one single command to generate multi-matchup datasets covering a given
period of time, in a sequential way or distributing the processing load with
jobard .

This section leads you through the different processing arguments for
``felyx-mmdb`` command.

.. command-output:: felyx-mmdb --help

``felyx-mmdb`` is able to fetch the input Earth Observation data files to be
processed between ``--start`` and ``--end`` date from your local datastore,
using the ``filenaming`` pattern defined in the processing configuration file.
This is a major difference with ``felyx-extraction`` which was taking a list
of EO files to process instead.