.. _matchup_extraction:

=========================
Extracting child products
=========================
To extract **child products** (subsets) from an Earth Observation file,
``felyx-extraction`` command is used.

This section leads you through the different processing arguments for this
command:

.. command-output:: felyx-extraction --help


Basic usage
===========

The simplest call performs child product extraction from local earth
observation (EO) data files using the extraction settings defined for the
dataset it belongs to in a configuration file (see
:ref:`processing-configuration` on how to write a configuration file):

.. code-block:: bash

  felyx-extraction --configuration <path to configuration file> \
  --dataset-id <dataset_id> --inputs <path to EO data file(s)>

where ``dataset-id`` is the dataset identifier, as defined in the configuration
file (``--configuration``), corresponding to the processed input EO file given
after ``--inputs``.

For instance:

.. command-output:: felyx-extraction -c tests/resources/test_cciseastate/felyx_data.yaml --inputs tests/data/eo/cciseastate-s3a-sral/ESACCI-SEASTATE-L2P-SWH-Sentinel-3_A-20180709T212105-fv01.nc  --dataset-id ESACCI-SEASTATE-L2P-SWH-Sentinel-3_A_PLRM

This creates a manifest file, that summarizes the list of found subsets
matching the extraction criteria, by default in the repository where the
command was executed.

Alternatively the (list of) file(s) to process can be written in a text file
(one full file path per line) and passed to the commend through
``--from-list`` argument:

.. command-output:: find tests/data/eo/cciseastate-s3a-sral/ -name "*.nc"  >inputs.list
   :shell:

.. command-output:: felyx-extraction --from-list ./inputs.list -c tests/resources/test_cciseastate/felyx_data.yaml --dataset-id  ESACCI-SEASTATE-L2P-SWH-Sentinel-3_A_PLRM
   :ellipsis: 10


Manifest files
==============
By default the extracted child products are not written to disk. Instead they
are referenced into a manifest file in json format. This manifest file
contains the list of extracted child products for a single input EO data
file, with their properties. The root directory where to write the manifest
files can be set with ``manifest-dir``, otherwise the current directory is used.

The manifest file for a given input EO data file is written in a subdirectory
defined as::

  <root manifest directory>/<dataset_id>/<YYYY>/<JJJ>/<basename of the input
  EO data file>.manifest

where YYYY and JJJ correspond to the year and day in year of the start
acquisition date of the EO data file.

Writing the child products on disk
==================================
One can force saving the extracted child products to disk by using the
``--create-miniprod``. A NetCDF file is created for each extracted
child product.

The root directory where to write the child products can be set with
``--child-product-dir`` argument. The saved child products will be stored under
the following folder organization:

.. code-block:: bash

    <root child product directory>/<dynamic site collection><dynamic site><dataset_id>/<YYYY>/<JJJ>/

.. command-output:: felyx-extraction --create-miniprod --child-product-dir ./tests/results/child-products/ -c tests/resources/test_cciseastate/felyx_data.yaml --inputs tests/data/eo/cciseastate-s3a-sral/ESACCI-SEASTATE-L2P-SWH-Sentinel-3_A-20180709T212105-fv01.nc  --dataset-id ESACCI-SEASTATE-L2P-SWH-Sentinel-3_A_PLRM

The extracted miniprod are created and saved under the folder hierarchy
defined above:

.. command-output:: ls tests/results/child-products/*/*
   :shell:
   :ellipsis: 10


Limiting the extraction
-----------------------
It is possible to limit the extraction of child products to a selection of site
collections only, using ``--allowed-collections`` argument. Child products
will only be extracted over the sites of the specified collection(s), among
those defined in the configuration file. The extraction can be further
restricted to a specific list of sites only, using ``--allowed-sites``:

.. command-output:: felyx-extraction --allowed-sites cw6200082 -c tests/resources/test_cciseastate/felyx_data.yaml --inputs tests/data/eo/cciseastate-s3a-sral/ESACCI-SEASTATE-L2P-SWH-Sentinel-3_A-20180709T212105-fv01.nc  --dataset-id ESACCI-SEASTATE-L2P-SWH-Sentinel-3_A_PLRM


The extraction can also be limited to a given geographical area. Input EO
data files that don't intersect the provided area will not be processed further.
The extraction box is defined with ``--area`` argument followed by the
minimum latitude, minimum longitude, maximum latitude and maximum longitude
of the area bounding box.

.. command-output:: felyx-extraction --area 0 -180 70 0 -c tests/resources/test_cciseastate/felyx_data.yaml --inputs tests/data/eo/cciseastate-s3a-sral/ESACCI-SEASTATE-L2P-SWH-Sentinel-3_A-20180709T212105-fv01.nc  --dataset-id ESACCI-SEASTATE-L2P-SWH-Sentinel-3_A_PLRM

To decide whether an input EO data file intersects the area or interest or not,
``felyx-extraction`` uses the input EO data file's latitude/longitude
coordinates.

However if the data files contains global CF attributes defining the file
footprint's bounding box (``southernmost_latitude`` etc), they can be used
instead by specifying ``--area-attributes=cf`` in the command line.

Alternatively, if the EO data file provides a global attribute providing the
file footprint as a WKT string, this attribute can be used instead by
specifying ``--area-attribute=<name of the attribute>``.

