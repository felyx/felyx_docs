
.. _felyx_report: https://gitlab.ifremer.fr/felyx/felyx_report
.. _Elasticsearch: https://www.elastic.co/elasticsearch/
.. _Grafana: https://grafana.com/

.. _felyx-report-details:

=======================
Monitoring and alerting
=======================

For a production environment, with daily extraction and matchup assembling
processing by felyx, a **felyx** extension allows to generate a monitoring
dashboard that displays some indicators on the production status and
completeness. This service is supported by `felyx_report`_ package.

This section describes how to install, configure and use this extension
for the production monitoring of multi-matchup databases (MMDB).

Installation
============

Prerequisites
-------------

`felyx_report`_ requires `Elasticsearch`_ (version >= 7) and `Grafana`_
(version 8.4.2) to be installed in your environment.

`Elasticsearch`_  is used for storing the indicators produced for the
monitoring of **felyx** processing.

`Grafana`_ is used to display the indicators in a graphical web interface and
to trigger alerts if necessary.


Manual installation
-------------------

Install the report generator in a conda environment:

.. code-block:: bash

   # create conda env
   conda create -n felyx-report python=3
   conda activate felyx-report

   # install poetry
   pip install poetry poetry-dynamic-versioning

   # get package code
   git clone https://gitlab.ifremer.fr/felyx/felyx_report.git

   # install
   cd felyx_report
   poetry install -vv



Usage
=====

Overview
--------
Production indicators are calculated and displayed in a web dashboard (with
`Grafana`_) for each MMDB defined within a felyx MMDB configuration file (in
YAML) and for a given time range specified in the ``felyx-report`` command
arguments:

.. code-block:: bash

   felyx-report -h


For each MMDB and for each day of the time range specified in the
``felyx-report`` command arguments, indicators are first calculated and stored
into an `Elasticsearch`_ index. Each index cumulates the whole report data of a
given MMDB.

After computing these indicators, the ``felyx-report`` processing creates
a `Grafana`_ dashboard displaying these indicators, which can be opened
within a web browser. Alerts can also be set when a given indicator is not in
the expected range, and notifications can be sent one or several administrators.

The following plots are displayed for each MMDB:

1. A summary of the processed input data: for each EO dataset, the number of
processed EO files vs the number of produced manifests (which should be equal):

.. image:: ../_static/mmdb_report1.png


2. A summary of the produced metrics (if any were configured in the MMDB
configuration file): for each EO dataset and each type of metrics, the number of
created metrics:

  .. image:: ../_static/mmdb_report2.png

3. A summary of the produced matchups with:

*  the number of in situ measurements used in input (from the parquet in
   situ files)

  .. image:: ../_static/mmdb_report3.png


*  for each EO dataset and MMDB output product, the number of created MMDB
   output files
*  for each EO dataset and MMDB output product: the number of created matchups
   from manifest vs the number of assembled matchups

  .. image:: ../_static/mmdb_report4.png



4. The list of alerts raised:

  .. image:: ../_static/mmdb_report5.png


The ``felyx-report`` command needs to be run periodically (for instance
through a cron) to refresh the indicators over the period since its last
execution.


The informations required for the collection and processing of the production
indicators are given in the MMDB configuration file or ``felyx-report``
command arguments. They include:

- the used in situ data files (in parquet) which paths are defined in
  *SiteCollections* section of the MMDB configuration file.
- the source Earth Observation (EO) data files processed with `felyx-processor`
  which paths are defined in *EODatasets* section of the MMDB configuration
  file.
- the Manifest files produced by the ``felyx-processor`` extraction process for
  each input EO which path is given in the ``felyx-report`` command arguments.
- the Metric files (in json) produced by the extraction process which path is
  given in the ``felyx-report`` command arguments. This input is optional.
- the output MMDB files produced by the ``felyx-assemble`` matchup assembling
  process which path is given in the ``felyx-report`` command arguments.



Configuration
=============

System configuration file
-------------------------

The **felyx** reporting extension requires Elasticsearch and grafana to be
available in the environment. The access configuration to these services is
defined in a felyx system configuration file (in json format) which is the
same as the one optionally used by ``felyx-processor`` commands. Additional
sections must be provided within this system configuration file in particular
for grafana.

Refer to the :ref:`system-configuration` section for a full description of this
system configuration file.

The system configuration file can then be passed on to the
``felyx-report`` command (``-c`` argument) or referenced to by the
``FELYX_SYS_CFG`` environment variable


MMDB configuration file
-----------------------

Additional optional configuration items must be added to a MMDB configuration
file if we want to produce indicators and set alerts for the corresponding
MMDBs. This section describes the addition to the configuration file
described in the :ref:`processing-configuration`.

Basically, it will define for the different production indicators if we want
them to be displayed or not in the dashboard, and if alerts should be raised
when they deviate from the expected behavior.

This sections details the additional configuration items to be added in a
MMDB configuration file to set the dashboard and alert properties, per type
of indicator.

Indicator on input data processing
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
This indicator monitors the number of input EO files processed successfully.
A successfully processed input EO file will produce a corresponding manifest
file, and the number of input and manifest files should be equal. To
calculate this indicator, ``felyx-report`` collects the number of EO files in
the local archive within a time frame and similarly evaluates the number of
produced manifest files for the same dataset.

The settings for this indicator are defined in the dataset definition of the
``EODatasets`` section of the MMDB configuration file, which describes the
processed datasets, by adding a ``monitoring`` configuration item as below:

.. code-block:: yaml

    EODatasets:
      ESACCI-SEASTATE-L2P-SWH-Jason-3:
        dataset_class: NCDataset
        feature_class: Trajectory
        filenaming: tests/data/eo/jason-3/%Y/%j/ESACCI-SEASTATE-L2P-SWH-Jason-3-%Y%m%dT*-fv01.nc
        description: CCI Sea State Altimetry Along-Track data from Jason-3 v3.0
        level: L2P
        name: ESACCI-SEASTATE-L2P-SWH-Jason-3 v3.0
        supplier: ESA CCI Sea State
        # additional (optional) setting for felyx-report
        monitoring:
          alert_check_nb_of_processed_eo_files: True

if ``alert_check_nb_of_processed_eo_files`` is set to ``True``, an alert will
be generated for each day where the number of input EO files and the number
of created manifests is not equal. If ``False`` or not set, this alert
rule is ignored.


Indicator on metrics processing
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
This indicator monitors that the number of metrics processed is consistent with
the number of created matchups is in the nominal range (verifying
``felyx-extraction`` completed successfully).

.. code-block:: yaml

  metrics:
      ...
      monitoring:
              alert_check_nb_metrics : True

if ``alert_check_nb_metrics`` is set, the number of created metrics in the
generated metrics files is compared to the number of extracted matchups
(as listed in the manifest files created by ``felyx-extraction``). They
should be equal and an alert will be triggered if not. If it is not set, this
alert rule is ignored.

Indicator on dynamic sites
^^^^^^^^^^^^^^^^^^^^^^^^^^
This indicator monitors the number of dynamic sites locations (usually in situ
measurements) used for the matchup extraction. It helps to make sure that these
data are correctly and continuously fed to felyx. It is calculated from the
content of the parquet files containing the dynamic sites locations and
measurements.

The settings for this indicator are defined in the definition of the
``SiteCollections`` section of the MMDB configuration file, which describes the
sources for the dynamic sites, by adding a ``monitoring`` configuration item as
below:

.. code-block:: yaml

    SiteCollections:
      cmems_wave:
        description: CMEMS In Situ TAC selection of wave buoy data
        name: CMEMS In Situ TAC Wave
        datasource_access: parquet
        parquet_config:
          filepath_pattern: tests/data/insitu/parquet/cmems_wave/%Y%m%d_cmems_wave.parquet
          frequency: 1D
        # additional (optional) setting for felyx-report
        monitoring:
          alert_threshold_nb_of_in_situ_measurements: 19500

if ``alert_threshold_nb_of_in_situ_measurements`` is set to a given threshold,
an alert will be generated for each day where the number of received dynamic
site locations is lower than this threshold. If it is not set, this alert
rule is ignored.


Indicators on matchup extraction
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

These indicators monitor that the number of created matchups is in the nominal
range (verifying ``felyx-extraction`` completed successfully) and that the
number of created MMDB output files is as expected (verifying ``felyx-assemble``
completed successfully).

The settings for this indicator are defined in the definition of the
``products`` section of the MMDB configuration file, which describes content
of the MMDB output files, by adding a ``monitoring`` configuration item as
below:


.. code-block:: yaml

      products:
        CMEMS-L2P-SWH-Jason-3__cmems_wave:
          # output file pattern for the subproduct
          filenaming: '%Y/%Y%m%d_CMEMS-L2P-SWH-Jason-3__cmems_wave.nc'
          # tailor the content of the assembled files
          content:
            ESACCI-SEASTATE-L2P-SWH-Jason-3:
              # [Optional] list of variables to include in the assembled files (all
              # of them by default)
              variables: [.*]

              # [Optional] variables NOT TO include in extracted child products
              # (none of them by default)
              #except_variables:

              # [Optional] list of global attributes to include in the assembled
              # files (all of them by default)
              #attributes:

              # [Optional] list of global attributes NOT TO include in the assembled
              # files (none of them by default)
              except_attributes:
                - time_coverage_.*
                - cycle_number
                - relative_pass_number
                - equator_.*
                - track_id
                - date_.*
                - geospatial.*
                - history
                - input

              attributes_as_variables:
                - cycle_number
                - relative_pass_number

              # prefix by which to rename all variables and global attributes
              # coming from this dataset (by default the dataset id is used)
              prefix: cci
              # additional (optional) setting for felyx-report
              monitoring:
                alert_threshold_nb_of_created_mmdb_files: 1
                dashboard_create_nb_of_created_matchups: True
                alert_check_nb_of_created_matchups: True


if ``alert_check_nb_of_created_matchups`` is set, the number of extracted
matchups (as listed in the manifest files created by ``felyx-extraction``) is
compared to the number of assembled matchups in the MMDB output files. They
should be equal and an alert will be triggered if not. If it is not set, this
alert rule is ignored.

.. note::

  It is possible that for some datasets, the manifest files were not
  generated (in the case of the configuration of sibling products), and the
  displayed indicator with then be an empty plot. To avoid this, set
  ``dashboard_create_nb_of_created_matchups`` to ``False``: the corresponding
  plot will not be displayed in the dashboard.

For a continuous and regular inflow of dynamic site data (in situ measurements)
, it is expected that a constant number of MMDB output files should be
generated. if ``alert_threshold_nb_of_created_mmdb_files``, an alert is
triggered if the daily number of output files is above the provided threshold.
If it is not set, this alert rule is ignored.



Alert notifications
^^^^^^^^^^^^^^^^^^^
Alerts can be notified periodically by email to the felyx administrators.

These notifications can be configured in the MMDB configuration file in a new
``contact_points`` section to be added in the file.

.. code-block:: yaml

  contact_points:
    - name: 'name1'
      addresses:
        - 'your.address@domain.com'
        - 'your.other.address@domain.com'
      disable_resolve_message: False
      repeat_interval:
        duration: 12
        unit: 'h'
  ...

For each contact to notify, a list of email addresses can be specified in the
``addresses`` list.

The ``disable_resolve_message`` option allows to disable the resolve message
that is sent when the alerting state returns to false.

The ``repeat_interval`` entry specifies the waiting time before resending an
alert.


Issues
======

Somtimes when accessing the dashboard in your web browser, the dashboard may
appear without any indicator data loaded despite usually all the steps in
``felyx-report`` were completed successfully:

  * the Elasticsearch index was created and fed in with the indicator data
  * the Grafana datasource connecting to Elasticsearch was created
  * the Grafana dashboard was created

This is an issue with Grafana, met by many users:

  * https://community.grafana.com/t/datasource-test-using-api/66770
  * https://github.com/grafana/grafana/issues/11950

The solution is to go in Grafana GUI, select the datasource created for the
dashboard and click on *Save & test*.
