========
Commands
========

.. toctree::
   :maxdepth: 1

   felyx-extraction : extracting data subsets <commands/felyx-extraction>
   felyx-metrics : computing metrics over miniprods <commands/felyx-metric>
   felyx-assemble : assembling match-up datasets <commands/felyx-assemble>

   felyx-report : generating felyx production monitoring <commands/felyx-report>

..

   *felyx-ingest-insitu* : storing in situ data into a storage back-end <felyx-ingest-insitu>
