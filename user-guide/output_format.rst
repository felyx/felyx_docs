.. _output-format:

=============
Output format
=============

**felyx** creates two sorts of output data files:

* **child products**: extraction of single subsets from a source Earth
  Observation data file over a given static or dynamic site
* **multi-matchup** product files, aggregating a series of child products
  over a period of time within a single file.

In this section we describe the output format for both types of products:

.. toctree::
   :maxdepth: 2

   Child product format <child_product_output_format>
   Multi matchup database format <mmdb_output_format>


