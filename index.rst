.. felyx documentation master file, created by
   sphinx-quickstart on Mon Sep  9 10:33:48 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

felyx : data subsetting and multi-matchup datasets
**************************************************

**felyx** is a free software solution, written in python, whose aim is to
provide Earth Observation (EO) data producers and users with an open-source,
flexible and reusable tool to allow scientific analysis and performance
monitoring of scientific EO data by extracting data subsets over specific
areas or producing matchups with reference in situ measurements.

Credits
=======
.. image:: _static/logo/copernicus.jpg
  :width: 300

**felyx** is developed by Ifremer_ and supported by Copernicus_
through Eumetsat_.

.. _Ifremer: https://www.ifremer.fr
.. _Copernicus: https://www.copernicus.eu

**felyx** was initially founded and supported by the `European Space Agency
(ESA)`_, Eumetsat_.

.. _European Space Agency (ESA): https://esa.int
.. _Eumetsat: https://eumetsat.int


.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: For users

   Getting started <getting-started-guide/index>
   User guide <user-guide/index>

.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: For developers/contributors

   Developer guide <developer-guide/index>
   GitLab repository <https://gitlab.ifremer.fr/felyx>

.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: Indices and tables


License¶
========
**felyx** is available under the open source `GPLv3 license`__.

__ https://www.gnu.org/licenses/gpl-3.0.en.html