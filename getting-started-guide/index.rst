###############
Getting Started
###############

The getting started guide aims to get you using **felyx** productively as
quickly as possible. It is designed as an entry point for new users, and it
provides an introduction to felyx's main concepts and usage.

.. toctree::
   :maxdepth: 2
   :hidden:

   why-felyx
   installation
   quick-overview
