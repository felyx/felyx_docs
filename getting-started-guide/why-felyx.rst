Why felyx ?
===========

Concept
-------
**felyx** primarily works as a data extraction tool to subset large observation
datasets over static (predefined) geographical areas or time dependant (moving)
target locations. The smaller subsets matching these targets are referred to as
**child products**. The static or time dependant targets are referred to as
**sites**.

.. image:: ../_static/static_miniprod.png
   :width: 900 px

The extracted child products can be assembled and combined with the in situ
measurements attached to these extraction sites within output files, referred
to as **matchup datasets**.

Qualitative or quantitative **metrics** can also be computed for each
extracted child products that help later advanced selection or segmentation of
the extracted subsets for analysis or automatic alerting. An predefined set of
statistical operators (mean, median, rms, ...) is provided, to be applied to
the variable of your choice within the child products, but it can be extended
with custom operators defined by the **felyx** user.

These extracted child products and metrics can be additionally registered in a
fast search engine (ElasticSearch) from which they can be later queried by
users or interactive applications but this is not required.

**felyx** framework is deployable on a laptop for local execution, or to a
cloud or HPC infrastructure for large scale distributed processing over
massive collections of data.

**felyx** is  domain agnostic and can be used with geographically and
temporally located observations of any domain such as ocean, atmospheric
or land.

**felyx** is based on standard and reusable technologies and components making
it portable and cross platform.


Usage
-----

Possible usages of **felyx** include:

* monitoring and assessing the quality of Earth observations (e.g. satellite
  products and their time series) through statistical analysis and/or
  comparison with other data sources, in NRT or over longer periods
* assessing and inter-comparing geophysical inversion algorithms or
  different datasets
* alerting and reporting on performance degradation or specific conditions
* performing geophysical analysis (variability, trends,....)
* observing a given phenomenon, collecting and cumulating various parameters
  over a defined area
* crossing different sources of data for synergy applications

In this context, **felyx** serves different kind of users:

* instrument engineers
* calibration engineers
* quality control engineers
* validation scientists
* algorithm developers
* ocean/atmosphere scientific community.

