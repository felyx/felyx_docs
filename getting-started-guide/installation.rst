.. _installing:
.. _environment.yml: https://gitlab.ifremer.fr/felyx/felyx_processor/-/blob/master/environment.yml
.. _cerbere: https://cerbere.readthedocs.io/en/latest/
.. _jobard: https://jobard.gitlab-pages.ifremer.fr/documentation/
.. _felyx_distributed: https://gitlab.ifremer.fr/felyx/felyx_distributed

Installation
============

The basic installation requires ``felyx-processor`` package which contains
command line tools to extract data with **felyx** and produce multi-match-up
datasets.

Requirements:

  * python>=3.10
  * xarray >= 2024.10.0
  * pyinterp

You should first install the ``pyinterp`` dependency. Refer to the
corresponding installation procedure at:
https://pangeo-pyinterp.readthedocs.io/en/latest/

.. warning::
   **felyx** is not compatible with ``xarray`` versions ``2024.1`` to ``2024.9``
   because of a bug in ``xarray`` on time encoding [1]_.


With conda (Recommended)
------------------------

The easiest and fastest way to start using felyx.

First, you have to create and activate a conda env :

.. code-block:: bash

  conda create -n felyx python=3.11
  conda activate felyx

then install ``felyx-processor`` package:

.. code-block:: bash

  conda install -c conda-forge felyx-processor


With Poetry
-----------
When installing with Poetry, we still recommend to install within a conda
environment, so first create your environment:

.. code-block:: bash

  conda create -n felyx_processor
  conda activate felyx_processor

  git clone git+https://gitlab.ifremer.fr/felyx/felyx_processor.git
  cd felyx_processor
  poetry install -vv


From Pypi (Not recommended)
---------------------------
When installing with pip, we still recommend to install within a conda
environment, so first create your environment:

.. code-block:: bash

  conda create -n felyx-processor -y --file https://gitlab.ifremer.fr/felyx/felyx_processor/-/raw/master/assets/conda/conda-run-linux-64.lock
  conda activate felyx-processor

Then install `felyx_processor` :

.. code-block:: bash

  pip install felyx-processor


Extensions
==========

**felyx** extensions
--------------------
Several extras can be installed, with pip or conda :

  * The ``felyxcontribs_metrics_base`` extra is necessary if you want to
    generate metrics.
  * The ``jobard-client`` extra contains the API to schedule, distribute and
    monitor the processing of a large amount of files simultaneously, using
    `jobard`_ framework. Refer to section
  * `felyx_distributed`_ is a wrapper over ``jobard-client`` allowing for
    distriburted end to end production of a full match-up database. Refer to
    :ref:`felyx-mmdb` section for more details.


**cerbere** extensions
----------------------
**felyx** manages reading any source of Earth Observation data through
`cerbere`_ python package, that will interface with specific data sources
through extensions that can be found in
`cerbere repository <https://gitlab.ifremer.fr/cerbere/>`_.

For instance, processing Copernicus Sentinel-3 data in felyx will require to
install the `cerberecontrib-s3 <https://gitlab.ifremer
.fr/cerbere/cerberecontrib-s3>`_  extensions. Similarly, processing any
data complying to GHRSST format will require `cerberecontrib-ghrsst
<https://gitlab.ifremer.fr/cerbere/cerberecontrib-ghrsst>`_
extension.



.. [1] https://github.com/pydata/xarray/issues/9488
