.. _felyx_processor: https://gitlab.ifremer.fr/felyx/felyx_processor
.. _ESA CCI Sea State project: https://climate.esa.int/en/projects/sea-state/
.. _Copernicus Marine Service: https://marine.copernicus.eu
.. _felyx_extraction:
.. _jobard: https://jobard.gitlab-pages.ifremer.fr/documentation/

Quick overview
==============

Overview
--------

The basic concept of **felyx** is to subset data from large original Earth
Observation (EO) datasets over sites of interest, that can later analysed by
users.

The first step is to extract **child products** (subsets) from the source EO
files over a user provided selection of static or moving geographical sites
of interest. These child products can be extracted physically (as actual
netcdf files) or as references to the source EO files (filename and slice
indices).

**Metrics** can also be computed from the content of these extracted child
products, to derive properties describing the content or context of the
extracted child products.

Indexing of these child products and attached metrics into a search engine
(currently implemented with `elasticsearch <http://www.elasticsearch.org/>`_)
, for later search or query, is also possible but not required for production
of child products and matchup databases..

Last, the child products and metrics can be grouped (or **assembled**) into
**multi-matchup dataset** files, or stored separately.


Each step can be performed independently with a given command line tool that
can be sequenced in different ways as explained in this example, which will
leads you through the different steps for generating a multi-matchup dataset
from altimetry data against moored buoys. An end to end command performing
all the steps is also available.

  .. image:: /_static/production.png
     :width: 900 px

In the following tutorial we assume you have installed `felyx_processor`_ as
described in the previous chapter.


Input data
----------
In this example we use the data provided in the unit tests directory of
`felyx_processor`_ package. You should then first download the
`felyx_processor`_ source repository from gitlab:

.. code-block:: bash

    $ git clone git+https://gitlab.ifremer.fr/felyx/felyx_processor
    $ cd felyx_processor


The altimetry data, in NetCDF4 format, and taken from
`ESA CCI Sea State project`_, are located here:

.. command-output:: ls tests/data/eo/cciseastate-s3a-sral/ -1
    :ellipsis: 5

These data will be matched with the wave observations from moored buoys provided
by `Copernicus Marine Service`_, that have been converted to **felyx**
parquet format for processing:

.. command-output:: ls tests/data/insitu/parquet/cmems_wave/daily/ -1


Configuration
-------------
The **felyx** processing configuration for this example is described into a
YAML file. This configuration defines how the altimeter files, belonging to the
dataset ``ESACCI-SEASTATE-L2P-SWH-Sentinel-3_A_PLRM`` are to be matched up
with the CMEMS wave measurements (within a maximum time difference of 6 hours
set by ``time_window`` and a maximum distance of 100 km set by
``search_radius``).

For more details on the format and content of the configuration file, refer
to the :ref:`processing-configuration` chapter.


.. command-output:: cat tests/resources/test_cciseastate/felyx_data.yaml



Extracting child products from the input files
----------------------------------------------

Child products can be extracted from the input altimeter files by running the
`felyx_extraction`_ command line tool on each of these files:

.. command-output:: felyx-extraction -c tests/resources/test_cciseastate/felyx_data.yaml --inputs tests/data/eo/cciseastate-s3a-sral/ESACCI-SEASTATE-L2P-SWH-Sentinel-3_A-20180709T212105-fv01.nc  --dataset-id ESACCI-SEASTATE-L2P-SWH-Sentinel-3_A_PLRM --manifest-dir tests/results/


In this example, child products are identified for each close-by wave
measurement from CMEMS moored buoys dataset, within the defined maximum
radius and time window, for each input altimeter file (``--inputs`` argument)
belonging to the dataset ``ESACCI-SEASTATE-L2P-SWH-Sentinel-3_A_PLRM``
(``--dataset`` argument), as per defined in the configuration file (``-c``
argument). Note that physical extraction into netCDF files is possible (using
``--create-miniprod`` argument).

The above command creates one output file in the folder specified with
``--manifest-dir`` argument : the manifest which contains the list of found
matchups (one per line in the file). In particular, reference to the source
file (``source``) and the subsetting slices within this file (``slices``)
allow later on the physical extraction of these subsets.

.. command-output:: cat tests/results/ESACCI-SEASTATE-L2P-SWH-Sentinel-3_A_PLRM/2018/190/ESACCI-SEASTATE-L2P-SWH-Sentinel-3_A-20180709T212105-fv01.nc.manifest

.. note::

  Here `felyx_extraction`_ is run sequentially over each input file. This is
  not very efficient in production mode or for large datasets. Each input
  file should be processed independently in parallel over multiple nodes.
  This can be achieved in different ways, depending on the tools available in
  your infrastructure for such tasks. We suggest also to use the `jobard`_
  framework which can operate over a cloud or an HPC infrastructure. For more
  details, see the :ref:`processing` section.


Assembling the child products
-----------------------------
In the context of dynamic sites, all the child products have the same size
and can be grouped and stacked into periodic output files (N-hourly, N-daily,
etc...), also called **multi-matchup datasets**. This is the **assembling**
step which can be performed with ``felyx-assemble`` command:

.. command-output:: felyx-assemble -c tests/resources/test_cciseastate/felyx_data.yaml --from-manifests --manifest-dir ./tests/results/ --extract-from-source --start 2018-07-09 --end 2018-07-09 -o tests/results/ --matchup-dataset ESACCI-SEASTATE-L2P-SWH-Sentinel-3_A_PLRM__cmems_wave

The configuration of these assembled multi-matchup datasets was defined in the
same configuration as previously (passed with ``-c``) under the ``MMDatasets``
configuration item with the identifier
*ESACCI-SEASTATE-L2P-SWH-Sentinel-3_A_PLRM__cmems_wave*. This is the
identifier passed to ``felyx-argument`` with ``--matchup-dataset`` argument.

The creation of multi-matchup datasets is asked for the period from
2018-07-09 (``--start``) and 2018-07-09 (``--end``). The output multi-matchup
datasets files are defined in the configuration file as monthly so one file
only will be created for this time interval. The child products within this
interval are collected from the previously created manifest files
(``--from-manifests`` and ``--manifest-dir``), the corresponding data actually
extracted from the source files  (``--extract-from-source``).

There are many processing alternatives and different combinations of
arguments. Refer to :ref:`felyx-assemble` section for more details.

We can then have a look at the assembled file in netCDF format which contains
12 matchups (as we only processed one file above, there should be many more
in a complete processing situation).

The matchups are assembled along the ``obs`` axis. Each extracted satellite
data is a segment of 21 measurements (along ``cciseastate_time`` axis) as
specified in the configuration file. The history of wave buoy in situ
measurements within +/- 6 hours around the satellite to matchup colocation
time was also configured, and provided for each matchup along the
``site_obs`` axis.

.. command-output:: ncdump -h tests/results/2018/20180701_ESACCI-SEASTATE-L2P-SWH-Sentinel-3_A_PLRM__cmems_wave.nc

By default, **felyx** extracts all the variables from the source data
(satellite and insitu) but it is fully configurable (refer to
:ref:`processing-configuration`). It also add some
variables of itw own for traceability to the source. Check the
:ref:`output-format` section for more details.



  



