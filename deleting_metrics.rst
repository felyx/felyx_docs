Deleting metrics
================

Metrics and miniprod metadata can be removed from the indexing system (Elastic
Search) using the `felyx-wipe-miniprods` commandline tool.

.. Warning::
   This removes the complete set of metadata of a miniprod, including the miniprod description. The
   corresponding netCDF miniprod file is not deleted : deleted miniprods in the indexing system can
   not be queried anymore although the netCDF file may still exist. 

Usage
-----

::

  usage: felyx-wipe-miniprods [-h] [--dataset DATASET] [--collection COLLECTION]
                            [--site SITE] [--start START] [--stop STOP]
                            [--dry-run]

  optional arguments:
  -h, --help            show this help message and exit
  --dataset DATASET     dataset ID
  --collection COLLECTION
                        collection name
  --site SITE           site code
  --start START         start date (YYYY-mm-ddTHH:MM:SS format
  --stop STOP           stop date (YYYY-mm-ddTHH:MM:SS format
  --dry-run


More details
------------

The following options can be used:

  * `--site` to delete the metrics for a given site. Can be used in combination
    with `--dataset` option. Use the site short identifier.
  * `--dataset` to delete the metrics for a given dataset. Can be used in
    combination with `--site` option. se the dataset identifier.
  * `--dry-run` allows to check the number of metrics and miniprods that will be removed by the command
    but does not actually remove them.
  * `--start` and `--end` to remove metrics between start and end times, 
    expressed as *YYYY-mm-ddTHH:MM:SS*


.. Note::
   `FELYX_CFG` environment variable must be set pointing to your global configuration file.
   For instance::

      setenv FELYX_CFG /home/cercache/project/felyx/conf/felyx_cfg.json

For instance, removing all metrics and miniprods metadata for the site
**TEKK000** and dataset **arc-upa-l2p-atsr2-v2.1**::

  felyx-wipe-miniprods --site TEKK000 --dataset arc-upa-l2p-atsr2-v2.1

Removing all metrics of dataset **argo-iquam** for collection 
**amsr2-jaxa-l2p-v01.0** (for instance if we want to reprocess the complete
dataset for this collection)::

  felyx-wipe-miniprods  --dataset amsr2-jaxa-l2p-v01.0 --collection argo-iquam 



