===================
Returning miniprods
===================

Selecting miniprods and returning a zipped tar file.

  .. sourcecode:: python

   {
    "extraction": {
        "packaging": {
            "input": {
                "selection": {
                    "dataset_list": ["ostia-ukmo-l4-glob-v2.0"],
                    "start_time": "2010-01-01T00:00:00",
                    "stop_time": "2010-01-06T00:00:00",
                    "site_list": ["ghr126"],
		    "data_type": "miniprods"
                    }
                },
            "operation_name": "targzip"
        }
    }
   }

