=================
Dynamic site data
=================

The times, locations and data associated to dynamic sites can be stored
and managed in different ways within felyx system:

* in a back-end storage engine, such as Elasticsearch
* in data files stored in *Apache parquet* format


The **felyx in situ server** is an independant system that stores and gives access to in
situ data values. 

The data of an insitu server can be grouped into **datasets** (for instance corresponding to 
different observation networks and/or different types of measurements). All platforms in a
given dataset shall share a common set of fields (measured parameters).

This section describes how to manage the data contained in this server.

Data storage in felyx in situ server
====================================

The in situ server requires data to be archived in a particular way. The
format of this archive is as follows:

  1. A directory structure designed to ensure that a reasonable number
     of files are stored in each directory. Each separate dataset is stored in
     an individual directory tree. This is automatically generated.
  2. A set of sqlite files, one per site for each dataset, these are
     automatically generated.
  3. A JSON metadata file for each dataset describing the field level and
     global level metadata for that dataset. This is only required for
     the generation of felyx compatible netCDF files.

.. important::

  In order for a felyx instance to use this in situ server, the site names
  (or station IDs) must match those of the felyx instance (used as dynamic sites).


Format of the in situ data
==========================

To ingest data into the in situ server, the ingestion program reads a CSV file (which you must
create) and inserts the contents of that file into the archive.

The format of the CSV file is based on the felyx trajectory file format. There
are a few points to remember:

1. Only the field 'Time' is required. All entries into the time field MUST be
   in the ISO format YYYY-MM-DDTHH-MM-SS. This is represented by the C/POSIX
   format string "Y%-%m-%dT%H:%M:%S". Any time not in this format will cause
   ALL data in the CSV file to be rejected.

2. All times MUST be provided in UTC.

3. The system will automatically handle additional (or missing) fields in
   subsequent data imports; however, partially empty fields will cause ALL
   data in the CSV file to be rejected.

4. To assist non-English users, the delimiter is the semi-colon (;) character.
   This delimiter may be padded with whitespace, to air human readability.

5. One file may contain information from many sites (but will be considered
   to consist of data from only one dataset).

6. ONLY numeric data may be stored. None numeric data will cause the process
   to fail, and NO data will be removed or inserted.

7. Missing values should be denoted by the word 'null'. Implicitly missing data
   will cause the process to fail cleanly (see 3).

8. The row "position ;" is ignored, and is included only for compatibility with
   the felyx trajectory format.

The generic format is::

    target ; <site_name_1>
    fields ; <field_name_1> ; ... ;  <field_name_N> ; Time
    position ;
    <field_value_1> ; ... ;  <field_value_N> ; <measurement time as YYYY-MM-DDTHH:MM:SS>
    <field_value_1> ; ... ;  <field_value_N> ; <measurement time as YYYY-MM-DDTHH:MM:SS>
    target ; <site_name_2>
    fields ; <field_name_1> ; ... ;  <field_name_N> ; Time
    position ;
    <field_value_1> ; ... ;  <field_value_N> ; <measurement time as YYYY-MM-DDTHH:MM:SS>
    <field_value_1> ; ... ;  <field_value_N> ; <measurement time as YYYY-MM-DDTHH:MM:SS>

With a simple example being::

    target ; LMEL
    fields ; Wind_Speed ; Dew_Point_Temperature ; Extra_Quality_Flag ; Wind_Direction ; Sea_Water_Pressure ; Air_Pressure ; Platform_Type ; Air_Temperature ; Longitude ; Cloud_Coverage ; Time ; Latitude ; Quality_Indicator ; Quality_Flag ; Sea_Surface_Temperature
    position ;
    9.0 ; 279.649993896 ; 0 ; 180.0 ; null ; 99700.0 ; 1 ; 284.949981689 ; 5.69999980927 ; 0 ; 2012-10-01T00:00:00 ; 62.7999992371 ; 0 ; 0 ; 285.25
    8.0 ; 279.649993896 ; 0 ; 170.0 ; null ; 99660.0 ; 1 ; 285.149993896 ; 5.80000019073 ; 0 ; 2012-10-01T01:00:00 ; 62.7999992371 ; 0 ; 0 ; 285.25
    target ; SHIP
    fields ; Wind_Speed ; Dew_Point_Temperature ; Extra_Quality_Flag ; Wind_Direction ; Sea_Water_Pressure ; Air_Pressure ; Platform_Type ; Air_Temperature ; Longitude ; Cloud_Coverage ; Time ; Latitude ; Quality_Indicator ; Quality_Flag ; Sea_Surface_Temperature
    position ;
    6.19999980927 ; 269.75 ; 0 ; 30.0 ; null ; 101860.0 ; 0 ; 274.149993896 ; 224.5 ; 7 ; 2012-10-01T00:00:00 ; 71.0 ; null ; null ; 278.049987793
    6.69999980927 ; 269.350006104 ; 0 ; 50.0 ; null ; 101910.0 ; 0 ; 274.149993896 ; 224.5 ; 0 ; 2012-10-01T01:00:00 ; 71.0 ; null ; null ; 278.049987793

.. important::

  The order of the fields must correspond the order of the values, and it must
  contain a field called 'Time'; however the actual order if consistent is not
  relevant.

Metadata file
=============

You should also create a 'metadata' file, to contain information about the
contents of a dataset. This is used to generate miniprods from the in situ
archive. The information should be stored as a JSON file, the following may
be appropriate for this dataset:

  .. code-block:: json

    {
        "variable": {
            "Air_Pressure": {
                "units": "kilopascal",
                "long_name": "in situ air pressure at sea level",
                "standard_name": "air_pressure_at_sea_level",
                "comment": "fake data",
                "source": "imaginary data"
            },
            "Longitude": {
                "units": "degrees east",
                "long_name": "longitude",
                "standard_name": "longitude",
                "comment": "precision 0.01 degree"
            },
            "Time": {
                "units": "seconds since 1981-01-01 00:00:00",
                "long_name": "reference time of file"
            },
            "Latitude": {
                "units": "degrees north",
                "long_name": "latitude",
                "standard_name": "latitude",
                "comment": "precision 0.01 degree"
            },
            "SST": {
                "units": "kelvin",
                "long_name": "in situ sea surface temperature",
                "standard_name": "sea_surface_temperature",
                "comment": "fake data",
                "source": "imaginary data"
            }
        },
        "global": {
            "comment": "totally imaginary data",
            "license": "freely and openly available, with no restrictions; ",
            "title": "Fake Data for Demonstration",
            "product_version": "1.0",
            "summary": "Fake Data for Demonstration",
            "source": "the mind",
            "contact": "info@pelamis.co.uk",
            "Conventions": "CF-1.4",
            "institution": "Pelamis Scientific Software Ltd"
        }
    }

This file should be placed in the location provided by the python code (with
the dataset name corrected):

  .. code-block:: python

    import os
    from felyx_in_situ import IN_SITU_ARCHIVE, METADATA_FILE_NAME

    # Same as previous example
    DATASET_NAME = 'my_in_situ_v1'

    path = os.path.join(IN_SITU_ARCHIVE, DATASET_NAME, METADATA_FILE_NAME)

Which is the directory in the archive for that dataset. See the terminal help
for that command.


Ingesting data into the in situ server
======================================

Load your back-end virtual environment (or front-end if you have installed everything on a
single server).

Set the ``IN_SITU_SERVER_CFG`` environment variable to point to the in situ server configuration file:

.. code-block:: python

   export IN_SITU_SERVER_CFG={path to your configuration}/in_situ_server_cfg.json


Run the ingestion command:

.. code-block:: python

   felyx-in-situ-ingester -c <path to your in situ csv file> <dataset identifier>



